/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * summary_tree_model_column_record.hh
 * Copyright (C) 2019 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SUMMARY_TREE_MODEL_COLUMN_RECORD_H_
#define _SUMMARY_TREE_MODEL_COLUMN_RECORD_H_

class SummaryTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
	SummaryTreeModelColumnRecord()
		{ add(pickup); add(customer); add(garment); add(description); add(price); add(completed); add(empty); add(time_acp); add(index); }
	Gtk::TreeModelColumn<Glib::ustring> pickup;
	Gtk::TreeModelColumn<Glib::ustring> customer;
	Gtk::TreeModelColumn<Glib::ustring> garment;
	Gtk::TreeModelColumn<Glib::ustring> description;
	Gtk::TreeModelColumn<float> price;
	Gtk::TreeModelColumn<bool> completed;
	Gtk::TreeModelColumn<Glib::ustring> empty; // empty column used to improve the visual look of the columns
	Gtk::TreeModelColumn<time_t> time_acp; // hidden column to store the acceptance date
	Gtk::TreeModelColumn<int> index; // hidden column to store the index of the garment
};

#endif // _SUMMARY_TREE_MODEL_COLUMN_RECORD_H_

