/*
 * combobox_tree_model_column_record.hh
 * Copyright (C) 2015 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COMBOBOX_TREE_MODEL_COLUMN_RECORD_HH_
#define _COMBOBOX_TREE_MODEL_COLUMN_RECORD_HH_

#include <gtkmm/treemodelcolumn.h>

class ComboboxTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
	ComboboxTreeModelColumnRecord() { add(column); }
	Gtk::TreeModelColumn<Glib::ustring> column;
};

#endif // _COMBOBOX_TREE_MODEL_COLUMN_RECORD_HH_

