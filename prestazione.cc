/*
 * prestazione.cc
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
LiberAcus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LiberAcus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "prestazione.hh"
#include <iostream>
#include <iomanip>
#include <vector>

Glib::ustring Prestazione::sqlite_table = "TABLE PRESTAZIONI (" \
  "CAPO TEXT NOT NULL, "					\
  "DESCRIZIONE TEXT NOT NULL, "					\
  "PREZZO REAL NOT NULL, "					\
  "PRIMARY KEY (CAPO, DESCRIZIONE));";

int Prestazione::retrieve_prestazione(void *data, int argc, char **argv, char **azColName) {
  Prestazione prst;
  std::vector<Prestazione>* vec { static_cast<std::vector<Prestazione>*>(data) };
  for(int i=0; i<argc; i++) {
    if (Glib::ustring(azColName[i]) == Glib::ustring("CAPO") && argv[i]) {
      prst.set_capo(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("DESCRIZIONE") && argv[i]) {
      prst.set_descrizione(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("PREZZO") && argv[i]) {
      prst.set_prezzo(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("COMPLETATA") && argv[i]) {
      prst.set_completata(argv[i]);
    }
  }
  vec->push_back(prst);
  return 0;
}

Prestazione& Prestazione::operator=(const Prestazione &prst) {
  if (this != &prst) {
    capo = prst.capo;
    descrizione = prst.descrizione;
    prezzo = prst.prezzo;
    completata = prst.completata;
  }
  return *this;
}

bool Prestazione::operator<(const Prestazione &prst) const {
  if (capo.raw() < prst.capo.raw()) {
    return true;
  } else if (capo.raw() == prst.capo.raw() && descrizione.raw() < prst.descrizione.raw()) {
    return true;
  } else {
    return false;
  }
}

bool Prestazione::operator==(const Prestazione &prst) const {
  if (capo.raw() == prst.capo.raw() && descrizione.raw() == prst.descrizione.raw()) {
    return true;
  }
  return false;
}

void Prestazione::set_completata(const Glib::ustring &str) {
  if (str == Glib::ustring("true") || str == Glib::ustring("1")) {
    completata = true;
  } else {
    if (str == Glib::ustring("false") || str == Glib::ustring("0")) {
      completata = false;
    } else {
      std::cerr << "ERRORE! set_completata():: la stringa non contiene nessun valore valido" << std::endl;
    }
  }
}
