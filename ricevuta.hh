/*
 * ricevuta.hh
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 LiberAcus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LiberAcus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RICEVUTA_HH_
#define _RICEVUTA_HH_

#include "capo.hh"

#define LINE_COLUMNS	32

enum class MEMB_FIELD { num, date_acc, nome, acconto, date_rit, urg, cons };

class Ricevuta {
public:
  static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
  static int retrieve_ricevuta(void *data, int argc, char **argv, char **azColName);
  static void set_numero_ricevute_emesse(unsigned int val);
  static inline void set_sovraprezzo_urgenza(float val) { sovraprezzo_urgenza = val; }
  static inline float get_sovraprezzo_urgenza(void) { return sovraprezzo_urgenza; }
  static unsigned int get_last_numero_ricevuta(void);
  static unsigned int generate_next_numero_ricevuta(void);
  static Glib::ustring get_formatted_line(const Glib::ustring &str1, const Glib::ustring &str2, Glib::ustring::size_type line);
  inline Ricevuta() : numero (0), nome_cliente(""), acconto (0.0), urgente(false), consegnato(false) {}
  inline ~Ricevuta() { }
  Ricevuta& operator=(const Ricevuta &rcvt);
  bool operator<(const Ricevuta &rcvt) const;
  bool operator==(const Ricevuta &rcvt) const;
  inline void set_data_accettazione(const CustomDate& cd) { data_accettazione = cd; }
  inline void set_date_acceptance(const char* ptr) { time_t uxtime = atoll(ptr); data_accettazione = CustomDate{&uxtime}; }
  inline void set_data_accettazione(const Glib::ustring& str) { time_t uxtime = atoll(str.c_str()); data_accettazione = CustomDate{&uxtime}; }
  inline void set_data_accettazione(const Glib::ustring& str, DATE_FORMAT format) { data_accettazione = CustomDate::parse_string(str.c_str(), format); }
  inline void set_data_accettazione(const char* ptr, DATE_FORMAT format) { set_data_accettazione(Glib::ustring(ptr), format); }
  inline CustomDate get_data_accettazione(void) const { return data_accettazione; }
  inline time_t get_date_acc_unix(void) const { return data_accettazione.get_time_t(); }
  inline Glib::ustring get_data_acc_string(DATE_FORMAT format) const { return Glib::ustring(data_accettazione.get_formatted_string(format)); }
  inline void set_numero(unsigned int val) { numero = val; }
  inline void set_numero(const Glib::ustring &str) { numero = atoi(str.c_str()); }
  inline void set_numero(const char* ptr) { numero = atoi(ptr); }
  inline unsigned int get_numero(void) const { return numero; }
  inline Glib::ustring get_numero_string(void) const { return Glib::ustring::format(numero); }
  inline void set_nome_cliente(const Glib::ustring& str) { nome_cliente.assign(str); }
  inline void set_nome_cliente(const char* ptr) { nome_cliente.assign(Glib::ustring(ptr)); }
  inline Glib::ustring get_nome_cliente(void) const { return nome_cliente; }
  void set_capi(const std::vector<Capo> &list_cp);
  inline const std::vector<Capo>* get_const_capi(void) const { return &capi; }
  inline void clear_capi(void) { capi.clear(); }
  inline void aggiungi_capo(const Capo& cp) { capi.push_back(cp); }
  Capo* get_capo(unsigned int index);
  bool set_prestazione_completata(const unsigned int nr, const Glib::ustring& descr, bool flag);
  float get_totale_lordo(void) const;
  float get_totale_netto(void) const;
  inline float get_totale_da_pagare(void) const { return get_totale_netto() - acconto; }
  inline void set_acconto(float val) { acconto = val; }
  inline void set_acconto(const Glib::ustring &str) { acconto = atof(str.c_str()); }
  inline float get_acconto(void) const { return acconto; }
  inline void set_data_ritiro(const CustomDate& tempo) { data_ritiro = tempo; }
  inline void set_data_ritiro(const char* ptr) { time_t uxtime = atoll(ptr); data_ritiro = CustomDate{&uxtime}; }
  inline void set_data_ritiro(const Glib::ustring& str) { time_t uxtime = atoll(str.c_str()); data_ritiro = CustomDate{&uxtime}; }
  inline void set_data_ritiro(const Glib::ustring& str, DATE_FORMAT format) { data_ritiro = CustomDate::parse_string(str.c_str(), format); }
  inline void set_data_ritiro(const char* ptr, DATE_FORMAT format) { set_data_ritiro(Glib::ustring(ptr), format); }
  inline CustomDate get_data_ritiro(void) const { return data_ritiro; }
  inline time_t get_date_rit_unix(void) const { return data_ritiro.get_time_t(); }
  inline Glib::ustring get_data_rit_string(DATE_FORMAT format) const { return Glib::ustring(data_ritiro.get_formatted_string(format)); }
  inline void set_urgente(bool flag) { urgente = flag; }
  void set_urgente(const Glib::ustring &str);
  inline void set_urgente(const char* ptr) { set_urgente(Glib::ustring(ptr)); }
  inline bool is_urgente(void) const  { return urgente; }
  inline bool is_pagato(void) const { return get_totale_netto() == acconto; }
  inline void set_consegnato(bool flag) { consegnato = flag; }
  void set_consegnato(const Glib::ustring &str);
  inline void set_consegnato(const char* ptr) { set_consegnato(Glib::ustring(ptr)); }
  void set_lavoro_completato(void);
  bool is_lavoro_completato(void) const;
  inline bool is_consegnato(void) const { return consegnato; }
  Glib::ustring get_ricevuta_string(bool stampa_numero) const;
  Glib::ustring get_etichetta_string(std::vector<Capo>::const_iterator iter, bool stampa_numero) const;
protected:
  std::vector<Capo>::iterator get_capo(const unsigned int nr, bool* result);
private:
  static Glib::ustring sqlite_table;
  static unsigned int ricevute_emesse;
  static float sovraprezzo_urgenza;
  CustomDate data_accettazione;
  unsigned int numero;
  Glib::ustring nome_cliente;
  std::vector<Capo> capi;
  float acconto;
  CustomDate data_ritiro;
  bool urgente;
  bool consegnato;
};

#endif // _RICEVUTA_HH_

