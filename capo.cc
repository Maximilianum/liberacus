/*
 * capo.cc
 * Copyright (C) 2017 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "capo.hh"
#include <vector>

Glib::ustring Capo::sqlite_table = "TABLE ORDINI (" \
  "ID INTEGER PRIMARY KEY AUTOINCREMENT," \
  "DATA_ACC INTEGER NOT NULL, " \
  "CAPO TEXT NOT NULL, " \
  "INDICE INTEGER NOT NULL, " \
  "DESCRIZIONE TEXT NOT NULL, " \
  "PREZZO REAL NOT NULL, " \
  "COMPLETATA INT, " \
  "FOREIGN KEY(DATA_ACC) REFERENCES RICEVUTE(DATA_ACC));";

int Capo::retrieve_capo(void *data, int argc, char **argv, char **azColName) {
  std::pair<task_info, Prestazione> ti;
  std::vector<std::pair<task_info, Prestazione>>* vec { static_cast<std::vector<std::pair<task_info, Prestazione>>*>(data) };
  for(int i=0; i<argc; i++) {
    if (Glib::ustring(azColName[i]) == Glib::ustring("NOME") && argv[i]) {
      ti.first.customer = Glib::ustring(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("DATA_ACC") && argv[i]) {
      ti.first.date = CustomDate(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("INDICE") && argv[i]) {
      ti.first.index = atoi(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("DATA_RIT") && argv[i]) {
      ti.first.pickup = CustomDate(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("CAPO") && argv[i]) {
      ti.second.set_capo(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("DESCRIZIONE") && argv[i]) {
      ti.second.set_descrizione(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("PREZZO") && argv[i]) {
      ti.second.set_prezzo(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("COMPLETATA") && argv[i]) {
      ti.second.set_completata(argv[i]);
    }
  }
  vec->push_back(ti);
  return 0;
}

Capo& Capo::operator=(const Capo &cp) {
  if (this != &cp) {
    numero = cp.numero;
    capo = cp.capo;
    set_prestazioni(cp.prestazioni);
  }
  return *this;
}

bool Capo::operator<(const Capo &cp) const {
  if (capo.raw() < cp.capo.raw()) {
    return true;
  } else if (capo.raw() == cp.capo.raw() && numero < cp.numero) {
    return true;
  } else {
    return false;
  }
}

bool Capo::operator==(const Capo &cp) const {
  if (capo.raw() == cp.capo.raw() && numero == cp.numero) {
    return true;
  } else {
    return false;
  }
}

void Capo::set_prestazioni(const std::vector<Prestazione> &list_prst) {
  prestazioni.clear();
  prestazioni.insert(prestazioni.begin(), list_prst.begin(), list_prst.end());
}

bool Capo::aggiungi_prestazione(const Prestazione &prst) {
  if (prst.get_capo().raw() == capo.raw()) {
    prestazioni.push_back(prst);
    return true;
  }
  return false;
}

bool Capo::set_prestazione_completata(const Glib::ustring& descr, bool flag) {
  bool result;
  std::vector<Prestazione>::iterator iter_prst = get_prestazione(descr, &result);
  if (result) {
    iter_prst->set_completata(flag);
  }
  return result;
}

void Capo::completa_prestazioni(void) {
  std::vector<Prestazione>::iterator iter_prst { prestazioni.begin() };
  std::vector<Prestazione>::iterator end_prst { prestazioni.end() };
  while (iter_prst != end_prst) {
    iter_prst->set_completata(true);
    iter_prst++;
  }
}

/**
 * protected functions
 */

std::vector<Prestazione>::iterator Capo::get_prestazione(const Glib::ustring& descr, bool* result) {
  std::vector<Prestazione>::iterator iter_prs = prestazioni.begin();
  std::vector<Prestazione>::iterator end_prs = prestazioni.end();
  while (iter_prs != end_prs) {
    if (iter_prs->get_descrizione() == descr) {
      *result = true;
      return iter_prs;
    }
    iter_prs++;
  }
  *result = false;
  return end_prs;
}
