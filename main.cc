/*
 * main.cc
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * LiberAcus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LiberAcus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "controller.hh"


#ifdef ENABLE_NLS
#include <libintl.h>
#endif


/* utent interface file for installed program */
//#define UI_FILE "/usr/share/liberacus/ui/liberacus.ui"

/* utent interface file for developing and testing */
//#define UI_FILE "liberacus.ui"

//Gtk::Window* main_win;
//Controller* main_ctrl;

// check if data need to be saved
/*bool on_quitting(void) {
	if (main_ctrl->is_log_active()) {
		main_ctrl->write_log("--> Going to quit application <--");
		main_ctrl->close_log();
	}
	return true;
	}*/

int main(int argc, char* argv[]) {
	Glib::ustring appname { APP_FILE_NAME };
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(Glib::ustring("org.maniscalco.") + appname, Gio::APPLICATION_FLAGS_NONE);
	Glib::ustring main_dir_path (Glib::get_home_dir() + Glib::ustring("/.local/share/") + appname);
	Glib::RefPtr<Gio::File> main_dir = Gio::File::create_for_path(main_dir_path);
	// try to create the main directory in .local/share/
	try {
		main_dir->make_directory();
	} catch (const Gio::Error& error) {
		if (error.code() != Gio::Error::EXISTS) {
			std::cerr << error.what() << std::endl;
		}
	}
	Glib::ustring ui_filename { Glib::ustring::compose("/usr/share/%1/ui/%1.ui", appname) };
	for (int i = 1; i < argc; i++) {
		Glib::ustring option { argv[i] };
		if (option.compare(Glib::ustring("--devel")) == 0 || option.compare(Glib::ustring("-d")) == 0) {
			ui_filename.assign(Glib::ustring::compose("%1.ui", appname));
		}
	}
	//Load the Glade file and instiate its widgets:
	Glib::RefPtr<Gtk::Builder> builder;
	try {
		builder = Gtk::Builder::create_from_file(ui_filename);
	} catch (const Glib::FileError & ex) {
		std::cerr << ex.what() << std::endl;
		return 1;
	}
	Controller* ctrl = new Controller(builder, app, main_dir_path);
	if (ctrl) {
		app->run(*(ctrl->get_main_window()));
	}
	delete ctrl;
	return 0;
}
