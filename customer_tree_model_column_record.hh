/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * customer_tree_model_column_record.hh
 * Copyright (C) 2015 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CUSTOMER_TREE_MODEL_COLUMN_RECORD_H_
#define _CUSTOMER_TREE_MODEL_COLUMN_RECORD_H_

class CustomerTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
	CustomerTreeModelColumnRecord()
		{ add(name); add(telephone); }
	Gtk::TreeModelColumn<Glib::ustring> name;
	Gtk::TreeModelColumn<Glib::ustring> telephone;
};

#endif // _CUSTOMER_TREE_MODEL_COLUMN_RECORD_H_

