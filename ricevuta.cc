/*
 * ricevuta.cc
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 LiberAcus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LiberAcus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ricevuta.hh"
#include <iomanip>
#include <vector>

Glib::ustring Ricevuta::sqlite_table = "TABLE RICEVUTE (" \
  "DATA_ACC INTEGER PRIMARY KEY NOT NULL, " \
  "NUMERO INTEGER NOT NULL, " \
  "NOME TEXT NOT NULL, " \
  "ACCONTO REAL, " \
  "DATA_RIT INTEGER NOT NULL, " \
  "URGENTE INT, " \
  "CONSEGNATO INT);";

unsigned int Ricevuta::ricevute_emesse = 0;
float Ricevuta::sovraprezzo_urgenza = 0.0;

/*
 * static class function
 */

int Ricevuta::retrieve_ricevuta(void *data, int argc, char **argv, char **azColName) {
  Ricevuta rcvt;
  Ricevuta* rcvt_ptr = &rcvt;
  Capo cp;
  Capo* cp_ptr = &cp;
  Prestazione prst;
  std::vector<Ricevuta>* vec { static_cast<std::vector<Ricevuta>*>(data) };
  for(int i=0; i<argc; i++) {
    if (Glib::ustring(azColName[i]) == Glib::ustring("DATA_ACC") && argv[i]) {
      rcvt.set_date_acceptance(argv[i]);
      /* check if this DATA_ACC is the same of the previous row
       * if it's same, it won't add a new Ricevuta, but will update the previous one
       */
      if (vec->size() > 0 && rcvt.get_data_accettazione() == vec->rbegin()->get_data_accettazione()) {
	rcvt_ptr = &*(vec->rbegin());
      }
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("NUMERO") && argv[i]) {
      rcvt_ptr->set_numero(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("NOME") && argv[i]) {
      rcvt_ptr->set_nome_cliente(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("ACCONTO") && argv[i]) {
      rcvt_ptr->set_acconto(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("DATA_RIT") && argv[i]) {
      rcvt_ptr->set_data_ritiro(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("URGENTE") && argv[i]) {
      rcvt_ptr->set_urgente(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("CONSEGNATO") && argv[i]) {
      rcvt_ptr->set_consegnato(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("INDICE") && argv[i]) {
      cp_ptr = rcvt_ptr->get_capo(atoi(argv[i]));
      if (cp_ptr == nullptr) {
	cp_ptr = &cp;
      }
      cp_ptr->set_numero(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("CAPO") && argv[i]) {
      cp_ptr->set_capo(argv[i]);
      prst.set_capo(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("DESCRIZIONE") && argv[i]) {
      prst.set_descrizione(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("PREZZO") && argv[i]) {
      prst.set_prezzo(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("COMPLETATA") && argv[i]) {
      prst.set_completata(argv[i]);
      cp_ptr->aggiungi_prestazione(prst);
    }
  }
  if (cp_ptr == &cp) {
    rcvt_ptr->aggiungi_capo(*cp_ptr);
  }
  if (rcvt_ptr == &rcvt) {
    vec->push_back(rcvt);
  }
  return 0;
}

void Ricevuta::set_numero_ricevute_emesse ( unsigned int val ) {
  ricevute_emesse = val;
}

unsigned int Ricevuta::get_last_numero_ricevuta ( void ) {
  return ricevute_emesse;
}

unsigned int Ricevuta::generate_next_numero_ricevuta ( void ) {
  ricevute_emesse++;
  return ricevute_emesse;
}

Glib::ustring Ricevuta::get_formatted_line ( const Glib::ustring &str1, const Glib::ustring &str2, Glib::ustring::size_type line ) {
  Glib::ustring formatted;
  if ( ( str1.size() + str2.size() ) < line ) {
    Glib::ustring::size_type diff = line - str1.size() - str2.size();		
    Glib::ustring spazio ( diff, ' ' );
    formatted += str1 + spazio + str2 + "\n";
  } else {
    formatted += str1 + str2 + "\n";
  }
  return formatted;
}

/*
 * member functions
 */

Ricevuta& Ricevuta::operator=( const Ricevuta &rcvt ) {
  if ( this != &rcvt ) {
    data_accettazione = rcvt.data_accettazione;
    numero = rcvt.numero;
    nome_cliente = rcvt.nome_cliente;
    set_capi(rcvt.capi);
    acconto = rcvt.acconto;
    data_ritiro = rcvt.data_ritiro;
    urgente = rcvt.urgente;
    consegnato = rcvt.consegnato;
  }
  return *this;
}

bool Ricevuta::operator<( const Ricevuta &rcvt ) const {
  if (data_accettazione < rcvt.data_accettazione) {
    return true;
  } else if (data_accettazione == rcvt.data_accettazione && numero < rcvt.numero) {
    return true;
  }
  return false;
}

bool Ricevuta::operator==( const Ricevuta &rcvt ) const {
  if (data_accettazione == rcvt.data_accettazione && numero == rcvt.numero ) {
    return true;
  }
  return false;
}

void Ricevuta::set_capi(const std::vector<Capo> &list_cp) {
  capi.clear();
  capi.insert(capi.begin(), list_cp.begin(), list_cp.end());
}

Capo* Ricevuta::get_capo(unsigned int index) {
  bool result;
  std::vector<Capo>::iterator iter = get_capo(index, &result);
  return result ? &*iter : nullptr;
}

std::vector<Capo>::iterator Ricevuta::get_capo(const unsigned int nr, bool* result) {
  std::vector<Capo>::iterator iter_capi = capi.begin();
  std::vector<Capo>::iterator end_capi = capi.end();
  while (iter_capi != end_capi) {
    if (iter_capi->get_numero() == nr) {
      *result = true;
      return iter_capi;
    }
    iter_capi++;
  }
  *result = false;
  return end_capi;
}

bool Ricevuta::set_prestazione_completata(const unsigned int nr, const Glib::ustring& descr, bool flag) {
  bool result;
  std::vector<Capo>::iterator iter_cp = get_capo(nr, &result);
  if (result) {
    result = iter_cp->set_prestazione_completata(descr, flag);
  }
  return result;
}

float Ricevuta::get_totale_lordo(void) const {
  float totale = 0.0;
  std::vector<Capo>::const_iterator iter_capi = capi.begin();
  std::vector<Capo>::const_iterator end_capi = capi.end();
  while (iter_capi != end_capi) {
    const std::vector<Prestazione>* prestazioni = iter_capi->get_const_prestazioni ();
    std::vector<Prestazione>::const_iterator iter_prst = prestazioni->begin();
    std::vector<Prestazione>::const_iterator end_prst = prestazioni->end();
    while (iter_prst != end_prst) {
      totale += iter_prst->get_prezzo();
      iter_prst++;
    }
    iter_capi++;
  }
  return totale;
}

float Ricevuta::get_totale_netto(void) const {
  float totale = get_totale_lordo();
  if (urgente) {
    totale += (totale / 100.0)*Ricevuta::get_sovraprezzo_urgenza();
  }
  return totale;
}

void Ricevuta::set_urgente(const Glib::ustring &str) {
  if ( str == Glib::ustring("true") || str == Glib::ustring("1")) {
    urgente = true;
  } else {
    if (str == Glib::ustring("false") || str == Glib::ustring("0")) {
      urgente = false;
    } else {
      std::cerr << "ERRORE! set_urgente():: la stringa non contiene nessun valore valido" << std::endl;
    }
  }
}

void Ricevuta::set_consegnato(const Glib::ustring &str) {
  if ( str == Glib::ustring("true") || str == Glib::ustring("1")) {
    consegnato = true;
  } else {
    if ( str == Glib::ustring("false") || str == Glib::ustring("0")) {
      consegnato = false;
    } else {
      std::cerr << "ERRORE! set_consegnato():: la stringa non contiene nessun valore valido" << std::endl;
    }
  }
}

void Ricevuta::set_lavoro_completato(void) {
  std::vector<Capo>::iterator iter_capi = capi.begin();
  std::vector<Capo>::iterator end_capi = capi.end();
  while (iter_capi != end_capi) {
    iter_capi->completa_prestazioni();
    iter_capi++;
  }
}

bool Ricevuta::is_lavoro_completato(void) const {
  std::vector<Capo>::const_iterator iter_capi = capi.begin();
  std::vector<Capo>::const_iterator end_capi = capi.end();
  while (iter_capi != end_capi) {
    const std::vector<Prestazione>* prestazioni = iter_capi->get_const_prestazioni();
    std::vector<Prestazione>::const_iterator iter_prst = prestazioni->begin();
    std::vector<Prestazione>::const_iterator end_prst = prestazioni->end();
    while (iter_prst != end_prst) {
      if (!iter_prst->get_completata()) {
	return false;
      }
      iter_prst++;
    }
    iter_capi++;
  }
  return true;
}

Glib::ustring Ricevuta::get_ricevuta_string(bool stampa_numero) const {
  int num_prest = 0;
  std::vector<Capo>::const_iterator iter = capi.begin();
  std::vector<Capo>::const_iterator end = capi.end();
  Glib::ustring str, str_start, str_end;
  if (stampa_numero) {
    str += "<tt><b>" + get_numero_string() + "</b> di " + data_accettazione.get_formatted_string(DATE_FORMAT::print) + "\n";
  } else {
    str += "<tt>" + data_accettazione.get_formatted_string(DATE_FORMAT::print) + "\n";
  }
  str += "<b>" + nome_cliente + "</b>\n\n";
  while ( iter != end ) {
    const std::vector<Prestazione>* prestazioni = iter->get_const_prestazioni();
    std::vector<Prestazione>::const_iterator iter_prst = prestazioni->begin();
    std::vector<Prestazione>::const_iterator end_prst = prestazioni->end();
    Glib::ustring capo = iter_prst->get_capo ();
    if (capo.size() > LINE_COLUMNS - 1) {
      capo.assign(capo.substr(0,LINE_COLUMNS - 2) + ".");
    }
    str += capo + "\n";
    while (iter_prst != end_prst) {			
      Glib::ustring descr = iter_prst->get_descrizione ();
      if (descr.size() > 20) {
	descr.assign(descr.substr(0,20));
      }
      str_end = "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), iter_prst->get_prezzo());
      str_start = descr;
      str += get_formatted_line(str_start, str_end, LINE_COLUMNS);
      num_prest++;
      iter_prst++;
    }
    iter++;
  }
  str += "\n";
  str_start = Glib::ustring::format(num_prest) + " capi, totale:";
  float totale = get_totale_lordo();
  str_end = "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), totale);
  str += get_formatted_line (str_start, str_end, LINE_COLUMNS);
  if (urgente) {
    str_start = "Consegna urgente (+" + Glib::ustring::format(Ricevuta::get_sovraprezzo_urgenza()) + "%)";
    str_end = "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), (totale / 100.0)*Ricevuta::get_sovraprezzo_urgenza());
    str += get_formatted_line (str_start, str_end, LINE_COLUMNS);
    totale += (totale / 100.0)*Ricevuta::get_sovraprezzo_urgenza();
  }
  str_start = "Totale da pagare:";
  str_end = "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), totale - acconto);
  str += get_formatted_line (str_start, str_end, LINE_COLUMNS);
  str += "\n<b>Ritiro capi: " + data_ritiro.get_formatted_string(DATE_FORMAT::print) + "</b>\n\n</tt>";
  return str;
}

Glib::ustring Ricevuta::get_etichetta_string(std::vector<Capo>::const_iterator iter, bool stampa_numero) const {
  Glib::ustring str;
  str += "<tt>\n";
  if (stampa_numero) {
    str += get_formatted_line(get_numero_string(), nome_cliente, LINE_COLUMNS);
  } else {
    str += nome_cliente + "\n";
  }
  Glib::ustring capo = iter->get_capo();
  if ( capo.size() > LINE_COLUMNS - 1) {
    capo.assign(capo.substr(0, LINE_COLUMNS - 2) + ".");
  }
  str += capo + "\n";
  const std::vector<Prestazione>* prestazioni = iter->get_const_prestazioni();
  std::vector<Prestazione>::const_iterator iter_p = prestazioni->begin();
  std::vector<Prestazione>::const_iterator end_p = prestazioni->end();
  while (iter_p != end_p) {
    Glib::ustring descrizione = iter_p->get_descrizione ();
    if (descrizione.size() > 20) {
      descrizione.assign(descrizione.substr(0, 20));
    }
    Glib::ustring prezzo_str = "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), iter_p->get_prezzo());
    str += get_formatted_line(descrizione, prezzo_str, LINE_COLUMNS);
    iter_p++;
  }
  if (urgente) {
    str += "<b>URGENTE</b>\n";
  }
  str += "Ritiro: " + data_ritiro.get_formatted_string(DATE_FORMAT::print) + "\n</tt>";
  return str;
}
