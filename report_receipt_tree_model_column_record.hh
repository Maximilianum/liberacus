/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * report_receipt_tree_model_column_record.hh
 * Copyright (C) 2015 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _REPORT_RECEIPT_TREE_MODEL_COLUMN_RECORD_H_
#define _REPORT_RECEIPT_TREE_MODEL_COLUMN_RECORD_H_

#include "custom_date.hh"

class ReportReceiptTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
	ReportReceiptTreeModelColumnRecord()
		{ add(number); add(customer); add(date_acceptance); add(total); add(time_acceptance); }
	Gtk::TreeModelColumn<Glib::ustring> number;
	Gtk::TreeModelColumn<Glib::ustring> customer;
	Gtk::TreeModelColumn<Glib::ustring> date_acceptance;
	Gtk::TreeModelColumn<Glib::ustring> total;
	Gtk::TreeModelColumn<CustomDate> time_acceptance;
};

#endif // _REPORT_RECEIPT_TREE_MODEL_COLUMN_RECORD_H_

