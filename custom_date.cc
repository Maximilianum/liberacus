/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * custom_date.cc
 * Copyright (C) 2018 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "custom_date.hh"

std::ostream& operator<<(std::ostream& os, const CustomDate& cd) {
	time_t unixtime = cd.get_time_t();
	struct tm* std_date = localtime(&unixtime);
	return os << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std_date->tm_sec << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << " unix time: " << unixtime;
}

CustomDate& CustomDate::operator=(const CustomDate &cd) {
	if (this != &cd) {
		unixtime = cd.unixtime;
	}
	return *this;
}
	
void CustomDate::get_time_period_boundary(CustomDate* start, CustomDate* end, TIME_PERIOD period) {
	time_t now = time(NULL);
	CustomDate cnow {&now};
	int today = cnow.get_weekday();
	cnow.set_hour(0);
	cnow.set_minute(0);
	cnow.set_second(0);
	*start = cnow;
	cnow.set_hour(23);
	cnow.set_minute(59);
	cnow.set_second(59);
	switch (period) {
		case TIME_PERIOD::daily:
			*end = cnow;
			break;
		case TIME_PERIOD::weekly:
			start->add_days(1 - today);
			*end = cnow;
			end->add_days(6 - today);
			break;
		case TIME_PERIOD::monthly:
			start->add_days(1 - cnow.get_day());
			*end = cnow;
			end->set_day(end->get_days_of_month());
			break;
		case TIME_PERIOD::annually:
			start->set_day(1);
			start->set_month(1);
			*end = cnow;
			end->set_day(31);
			end->set_month(12);
			break;
	}
}

CustomDate CustomDate::parse_string(const char* cs, DATE_FORMAT format) {
	std::string str { cs };
	time_t unixtime = time(nullptr);
	struct tm* std_date = localtime(&unixtime);
	switch (format) {
		case DATE_FORMAT::xml:
			// "%1:%2:%3 %4/%5/%6", hour, minute, second, day, month, year;
			std_date->tm_hour = atoi(str.substr(0, 2).c_str());
			std_date->tm_min = atoi(str.substr(3, 2).c_str());
			std_date->tm_sec =  atoi(str.substr(6, 2).c_str());
			std_date->tm_mday = atoi(str.substr(9, 2).c_str());
			std_date->tm_mon = atoi(str.substr(12, 2).c_str()) - 1;
			std_date->tm_year = atoi(str.substr(15, 4).c_str()) - 1900;
			break;
		case DATE_FORMAT::sql:
			// "%1-%2-%3 %4:%5:%6", year, month, day, hour, minute, second;
			std_date->tm_hour = atoi(str.substr(11, 2).c_str());
			std_date->tm_min = atoi(str.substr(14, 2).c_str());
			std_date->tm_sec = atoi(str.substr(17, 2).c_str());
			std_date->tm_mday = atoi(str.substr(8, 2).c_str());
			std_date->tm_mon = atoi(str.substr(5, 2).c_str()) - 1;
			std_date->tm_year = atoi(str.substr(0, 4).c_str()) - 1900;
			break;
		case DATE_FORMAT::human:
			// "%1 %2/%3/%4  ore %5:%6:%7", wday, day, month, year, hour, minute, second;
			std_date->tm_hour = atoi(str.substr(20, 2).c_str());
			std_date->tm_min = atoi(str.substr(23, 2).c_str());
			std_date->tm_sec = atoi(str.substr(26, 2).c_str());
			std_date->tm_mday = atoi(str.substr(4, 2).c_str());
			std_date->tm_mon = atoi(str.substr(7, 2).c_str()) - 1;
			std_date->tm_year = atoi(str.substr(10, 4).c_str()) - 1900;
			break;
		case DATE_FORMAT::label:
			// "%1 %2/%3/%4", wday, day, month, year;
			std_date->tm_mday = atoi(str.substr(4, 2).c_str());
			std_date->tm_mon = atoi(str.substr(7, 2).c_str()) - 1;
			std_date->tm_year = atoi(str.substr(10, 4).c_str()) - 1900;
			std_date->tm_hour = 0;
			std_date->tm_min = 0;
			std_date->tm_sec = 0;
			break;
		case DATE_FORMAT::print:
			// "%1 %2/%3/%4 %5:%6", wday, day, month, year, hour, minute;
			std_date->tm_hour = atoi(str.substr(11, 2).c_str());
			std_date->tm_min = atoi(str.substr(14, 2).c_str());
			std_date->tm_mday = atoi(str.substr(4, 2).c_str());
			std_date->tm_mon = atoi(str.substr(7, 2).c_str()) - 1;
			std_date->tm_year = atoi(str.substr(10, 4).c_str()) - 1900;
			std_date->tm_sec = 0;
			break;
		case DATE_FORMAT::date:
			// "%1/%2/%3", day, month, year;
			std_date->tm_mday = atoi(str.substr(0, 2).c_str());
			std_date->tm_mon = atoi(str.substr(3, 2).c_str()) - 1;
			std_date->tm_year = atoi(str.substr(6, 4).c_str()) - 1900;
			std_date->tm_hour = 0;
			std_date->tm_min = 0;
			std_date->tm_sec = 0;
			break;
	}
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
	return CustomDate { &unixtime };
}

CustomDate CustomDate::start_of_day(const CustomDate& cd) {
	time_t unixtime = cd.get_time_t();
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_hour = 0;
	std_date->tm_min = 0;
	std_date->tm_sec = 0;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
	return CustomDate { &unixtime };
}

CustomDate CustomDate::end_of_day(const CustomDate& cd) {
	time_t unixtime = cd.get_time_t();
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_hour = 23;
	std_date->tm_min = 59;
	std_date->tm_sec = 59;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
	return CustomDate { &unixtime };
}

bool CustomDate::is_leap_year(int year) {
	if (year % 400 == 0) {
		return true;
	} else if (year % 100 == 0) {
		return false;
	} else if (year % 4 == 0) {
		return true;
	}
	return false;
}

CustomDate::CustomDate(const int& dy, const int& mo, const int& yr, const int& hr, const int& mi, const int& se) {
	time_t now = time(nullptr);
	struct tm* std_date = localtime(&now);
	std_date->tm_mday = dy;
	std_date->tm_mon = mo - 1;
	std_date->tm_year = yr - 1900;
	std_date->tm_hour = hr;
	std_date->tm_min = mi;
	std_date->tm_sec = se;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
}

void CustomDate::set_year(short int val) {
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_year = val;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
}

int CustomDate::get_year(void) const {
	struct tm* std_date = localtime(&unixtime);
	return std_date->tm_year;
}

void CustomDate::set_month(short int val) {
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_mon = val;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
}

int CustomDate::get_month(void) const {
	struct tm* std_date = localtime(&unixtime);
	return std_date->tm_mon;
}

int CustomDate::get_days_of_month(void) const {
	struct tm* std_date = localtime(&unixtime);
	if (std_date->tm_mon == 1) {
		if (is_leap_year(std_date->tm_year + 1900)) {
			return 29;
		} else {
			return 28;
		}
	} else {
		return MONTH_DAYS[std_date->tm_mon];
	}
}

void CustomDate::set_day(int val) {
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_mday = val;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
}

int CustomDate::get_day(void) const {
	struct tm* std_date = localtime(&unixtime);
	return std_date->tm_mday;
}

void CustomDate::set_hour(short int val) {
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_hour = val;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
}

int CustomDate::get_hour(void) const {
	struct tm* std_date = localtime(&unixtime);
	return std_date->tm_hour;
}

void CustomDate::set_minute(short int val) {
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_min = val;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
}

int CustomDate::get_minute(void) const {
	struct tm* std_date = localtime(&unixtime);
	return std_date->tm_min;
}

void CustomDate::set_second(short int val) {
	struct tm* std_date = localtime(&unixtime);
	std_date->tm_sec = val;
	std_date->tm_isdst = -1;
	unixtime = mktime(std_date);
}

int CustomDate::get_second(void) const {
	struct tm* std_date = localtime(&unixtime);
	return std_date->tm_sec;
}

int CustomDate::get_weekday(void) const {
	struct tm* std_date = localtime(&unixtime);
	return std_date->tm_wday;
}

bool CustomDate::is_holiday(void) const {
	struct tm* std_date = localtime(&unixtime);
	// check if it's Sunday
	if (std_date->tm_wday == 0) {
		return true;
	}
	// check fixed holidays
	for (int i = 0; i < HOLIDAYS_SIZE; i++) {
		if (std_date->tm_mon == HOLIDAYS[i].first && std_date->tm_mday == HOLIDAYS[i].second) {
			return true;
		}
	}
	// check Easter day and Easter Monday
	std::pair<short, short> easter = get_easter_day(std_date->tm_year + 1900);
	if (std_date->tm_mon == easter.first && (std_date->tm_mday == easter.second || std_date->tm_mday == easter.second + 1)) {
		return true;
	}
	return false;
}

std::string CustomDate::get_formatted_string(DATE_FORMAT format) const {
	std::ostringstream str;
	std::string giorni[7] = { "Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab" };
	struct tm* std_date = localtime(&unixtime);
	switch (format) {
		case DATE_FORMAT::xml:  // "%1:%2:%3 %4/%5/%6"
			str << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std_date->tm_sec << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900;
			break;
		case DATE_FORMAT::sql:  // "%1-%2-%3 %4:%5:%6"
			str << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << "-" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "-" << std::setfill('0') << std::setw(2) << std_date->tm_mday << " " << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std::fixed << std::setprecision(3) << std_date->tm_sec;
			break;
		case DATE_FORMAT::human:	// "%1 %2/%3/%4  ore %5:%6:%7"
			str << giorni[std_date->tm_wday] << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << "  ore " << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std_date->tm_sec;
			break;
		case DATE_FORMAT::label:	// "%1 %2/%3/%4"
			str << giorni[std_date->tm_wday] << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900;
			break;
		case DATE_FORMAT::print:	// "%1 %2/%3/%4 %5:%6"
			str << giorni[std_date->tm_wday] << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday  << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << " " << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min;
			break;
		case DATE_FORMAT::date:	// "gg/mm/aaaa"
			str << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900;
			break;
	}
	return str.str();
}

bool CustomDate::is_same_day(const CustomDate& cd) const {
	struct tm* std_date = localtime(&unixtime);
	int myday = std_date->tm_mday;
	int mymon = std_date->tm_mon;
	int myyear = std_date->tm_year;
	time_t other = cd.get_time_t();
	std_date = localtime(&other);
	return myday == std_date->tm_mday && mymon == std_date->tm_mon && myyear == std_date->tm_year;
}

std::pair<short, short> CustomDate::get_easter_day(short int yr) const {
	int a = yr % 19;
	int b = yr / 100;
	int c = yr % 100;
	int d = b / 4;
	int e = b % 4;
	int g = (8 * b + 13) / 25;
	int h = (19 * a + b - d - g + 15) % 30;
	int m = (a + 11 * h) / 319;
	int j = c / 4;
	int k = c % 4;
	int l = (2 * e + 2 * j - k - h + m + 32) % 7;
	int n = (h - m + l + 90) / 25;
	int p = (h - m + l + n + 19) % 32;
	std::pair<short, short> easter = {n, p};
	return easter;
}
