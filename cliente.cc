/*
 * cliente.cc
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cliente.hh"
#include <vector>

Glib::ustring Cliente::sqlite_table = "TABLE RUBRICA (" \
  "NOME TEXT PRIMARY KEY NOT NULL, " \
  "TELEFONO TEXT);";

int Cliente::retrieve_cliente(void *data, int argc, char **argv, char **azColName) {
  Cliente clnt;
  std::vector<Cliente>* vec { static_cast<std::vector<Cliente>*>(data) };
  for(int i=0; i<argc; i++) {
    if (Glib::ustring(azColName[i]) == Glib::ustring("NOME") && argv[i]) {
      clnt.set_nome(argv[i]);
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("TELEFONO") && argv[i]) {
      clnt.set_telefono(argv[i]);
    }
  }
  vec->push_back(clnt);
  return 0;
}

Cliente& Cliente::operator=(const Cliente &clt) {
  if (this != &clt) {
    nome = clt.nome;
    telefono = clt.telefono;
  }
  return *this;
}

bool Cliente::operator<(const Cliente &clt) const {
  if (nome.raw() < clt.nome.raw()) {
    return true;
  }
  return false;
}

bool Cliente::operator==(const Cliente &clt) const {
  if (nome.raw() == clt.nome.raw()) {
    return true;
  }
  return false;
}
