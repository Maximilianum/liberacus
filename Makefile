#
# Makefile
#


# compilatore da usare
CPP = g++

TARGET ?= debug
ifeq ($(TARGET),debug)
 CPPFLAGS = -std=c++14 -O0 -g `pkg-config --cflags gtkmm-3.0`
endif
ifeq ($(TARGET),release)
 CPPFLAGS = -std=c++14 -O3 `pkg-config --cflags gtkmm-3.0`
endif

# nome dell'eseguibile
EXE = liberacus

# lista delle librerie da utilizzare separate dagli spazi
# ogni libreria dovrà avere il prefisso -l
LIBS = `pkg-config --libs gtkmm-3.0` -lsqlite3

# automated creation of object files list from all files with .cc suffix
OBJS := $(patsubst %.cc,%.o,$(wildcard *.cc))

$(EXE): $(OBJS) Makefile
	$(CPP) $(CPPFLAGS) -o $@ $(OBJS) $(LIBS)

# automated dependencies
#$(OBJS): $(HDRS) Makefile

main.o : main.cc controller.hh

controller.o : controller.cc controller.hh ctrl_app_engine.hh ctrl_engine.hh combobox_tree_model_column_record.hh cliente.hh prestazione.hh ricevuta.hh summary_tree_model_column_record.hh customer_tree_model_column_record.hh task_tree_model_column_record.hh receipt_tree_model_column_record.hh receipt_task_tree_model_column_record.hh report_receipt_tree_model_column_record.hh preferenze.hh custom_date.hh database.hh

ctrl_engine.o : ctrl_engine.cc ctrl_engine.hh combobox_tree_model_column_record.hh

ctrl_appa_engine.o : ctrl_app_engine.cc ctrl_app_engine.hh ctrl_engine.hh combobox_tree_model_column_record.hh

prestazione.o : prestazione.cc prestazione.hh

capo.o : capo.cc capo.hh prestazione.hh

cliente.o : cliente.cc cliente.hh

custom_date.o : custom_date.cc custom_date.hh

db_engine.o : db_engine.cc db_engine.hh

database.o : database.cc database.hh db_engine.hh cliente.hh prestazione.hh ricevuta.hh preferenze.hh

ricevuta.o : ricevuta.cc ricevuta.hh capo.hh prestazione.hh custom_date.hh

preferenze.o : preferenze.cc preferenze.hh custom_date.hh

form_print_operation.o : form_print_operation.cc form_print_operation.hh

# pulizia
.PHONY: clean
clean:
	rm -f core $(EXE) *.o

install:
	mkdir -p $(DESTDIR)/usr/bin
	strip --strip-unneeded --remove-section=.comment --remove-section=.note $(EXE)
	install -m 0755 $(EXE) $(DESTDIR)/usr/bin/$(EXE)
	mkdir -p $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 $(EXE).ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 $(EXE).png $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 cliente-new.png $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 prestazione-new.png $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 ricevuta-new.png $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 ricevuta-paga.png $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 LiberAcus.png $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 $(EXE).desktop $(DESTDIR)/usr/share/applications

uninstall:
	rm -f $(DESTDIR)/usr/bin/$(EXE)
	rm -f -R $(DESTDIR)/usr/share/$(EXE)
	rm -f $(DESTDIR)/usr/share/applications/$(EXE).desktop
