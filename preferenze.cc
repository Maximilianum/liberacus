/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 6; tab-width: 6 -*-  */
/*
 * preferenze.cc
 * Copyright (C) 2014 - 2020 Massimiliano <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "preferenze.hh"
#include "database.hh"

Glib::ustring Preferenze::sqlite_settings_table { Glib::ustring("TABLE SETTINGS (" \
                                                 "ENTRY TEXT PRIMARY KEY NOT NULL, " \
                                                 "VALUE TEXT NOT NULL); ") };

void Preferenze::set_password(const Glib::ustring &str) {
	std::vector<Glib::ustring> values { database->get_setting(SETT_PASSWORD) };
	try {
		if (values.size() > 0) {
			database->update(SETT_PASSWORD, str);
		} else {
			database->insert(SETT_PASSWORD, str);
		}
	} catch (const DataBase::DBError& dbe) {
		std::cerr << "Preferenze::set_password() " << dbe.get_message() << std::endl;
	}
}

Glib::ustring Preferenze::get_password(void) const {
	Glib::ustring value { database->get_setting(SETT_PASSWORD) };
	return value;
}

void Preferenze::set_enable_password(const Glib::ustring& str) {
	Glib::ustring psw;
	if (str == Glib::ustring("si")) {
		psw = "true";
	} else if (str == Glib::ustring("no")) {
		psw = "false";
	} else if (str == Glib::ustring("true") || str == Glib::ustring("false")) {
		psw = str;
	}
	Glib::ustring value { database->get_setting(SETT_ENABLE_PSW) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_ENABLE_PSW, psw);
	} else {
		database->insert(SETT_ENABLE_PSW, psw);
	}
}

bool Preferenze::get_enable_password(void) const {
	Glib::ustring value { database->get_setting(SETT_ENABLE_PSW) };
	return value.compare(Glib::ustring("true")) == 0;
}

void Preferenze::set_receipt_header(const Glib::ustring &str) {
	Glib::ustring value { database->get_setting(SETT_REC_HEADER) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_REC_HEADER, str);
	} else {
		database->insert(SETT_REC_HEADER, str);
	}
}

Glib::ustring Preferenze::get_receipt_header(void) const {
	Glib::ustring value { database->get_setting(SETT_REC_HEADER) };
	return value;
}

void Preferenze::set_receipt_greeting(const Glib::ustring &str) {
	Glib::ustring value { database->get_setting(SETT_REC_GREETING) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_REC_GREETING, str);
	} else {
		database->insert(SETT_REC_GREETING, str);
	}
}

Glib::ustring Preferenze::get_receipt_greeting(void) const {
	Glib::ustring value { database->get_setting(SETT_REC_GREETING) };
	return value;
}

void Preferenze::set_receipt_autodelete(short val) {
	Glib::ustring value { database->get_setting(SETT_REC_AUT_DELETE) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_REC_AUT_DELETE, Glib::ustring::format(val));
	} else {
		database->insert(SETT_REC_AUT_DELETE, Glib::ustring::format(val));
	}
}

void Preferenze::set_receipt_autodelete(const Glib::ustring& str) {
	short val;
	if (str == Glib::ustring("mai") || str == Glib::ustring("never")) {
		val = 0;
	} else if (str == Glib::ustring("giornaliera") || str == Glib::ustring("daily")) {
		val = 1;
	} else if (str == Glib::ustring("settimanale") || str == Glib::ustring("weekly")) {
		val = 2;
	} else if (str == Glib::ustring("mensile") || str == Glib::ustring("monthly")) {
		val = 3;
	} else if (str == Glib::ustring("annuale") || str == Glib::ustring("annually")) {
		val = 4;
	}
	set_receipt_autodelete(val);
}

short Preferenze::get_receipt_autodelete(void) const {
	Glib::ustring value { database->get_setting(SETT_REC_AUT_DELETE) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("0");
	}
	return atoi(value.c_str());
}

void Preferenze::set_reset_number(TIME_PERIOD period) {
	short val;
	switch (period) {
		case TIME_PERIOD::daily:
			val = 0;
			break;
		case TIME_PERIOD::weekly:
			val = 1;
			break;
		case TIME_PERIOD::monthly:
			val = 2;
			break;
		case TIME_PERIOD::annually:
			val = 3;
			break;
	}
	set_reset_number(val);
}

void Preferenze::set_reset_number(const Glib::ustring& str) {
	short val;
	if (str == Glib::ustring("giornaliero")) {
		val = 0;
	} else if (str == Glib::ustring("settimanale")) {
		val = 1;
	} else if (str == Glib::ustring("mensile")) {
		val = 2;
	} else if (str == Glib::ustring("annuale")) {
		val = 3;
	}
	set_reset_number(val);
}

void Preferenze::set_reset_number(unsigned short val) {
	Glib::ustring value { database->get_setting(SETT_RESET_NUMBER) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_RESET_NUMBER, Glib::ustring::format(val));
	} else {
		database->insert(SETT_RESET_NUMBER, Glib::ustring::format(val));
	}
}

TIME_PERIOD Preferenze::get_reset_number_period(void) const {
	TIME_PERIOD period;
	short val = get_reset_number();
	switch (val) {
		case 0:
			period = TIME_PERIOD::daily;
			break;
		case 1:
			period = TIME_PERIOD::weekly;
			break;
		case 2:
			period = TIME_PERIOD::monthly;
			break;
		case 3:
			period = TIME_PERIOD::annually;
			break;
	}
	return period;
}

short Preferenze::get_reset_number(void) const {
	Glib::ustring value { database->get_setting(SETT_RESET_NUMBER) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("0");
	}
	return atoi(value.c_str());
}

void Preferenze::set_print_number(const Glib::ustring& str) {
	Glib::ustring psw;
	if (str == Glib::ustring("si")) {
		psw = "true";
	} else if (str == Glib::ustring("no")) {
		psw = "false";
	} else if (str == Glib::ustring("true") || str == Glib::ustring("false")) {
		psw = str;
	}
	Glib::ustring value { database->get_setting(SETT_PRINT_NUMBER) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_PRINT_NUMBER, psw);
	} else {
		database->insert(SETT_PRINT_NUMBER, psw);
	}
}

bool Preferenze::get_print_number(void) const {
	Glib::ustring value { database->get_setting(SETT_PRINT_NUMBER) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("true");
	}
	return value.compare(Glib::ustring("true")) == 0;
}

void Preferenze::set_view_number(const Glib::ustring& str) {
	Glib::ustring psw;
	if (str == Glib::ustring("si")) {
		psw = "true";
	} else if (str == Glib::ustring("no")) {
		psw = "false";
	} else if (str == Glib::ustring("true") || str == Glib::ustring("false")) {
		psw = str;
	}
	Glib::ustring value { database->get_setting(SETT_VIEW_NUMBER) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_VIEW_NUMBER, psw);
	} else {
		database->insert(SETT_VIEW_NUMBER, psw);
	}
}

bool Preferenze::get_view_number(void) const {
	Glib::ustring value { database->get_setting(SETT_VIEW_NUMBER) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("true");
	}
	return value.compare(Glib::ustring("true")) == 0;
}

void Preferenze::set_max_per_day(const Glib::ustring& str) {
	Glib::ustring value { database->get_setting(SETT_MAX_PER_DAY) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_MAX_PER_DAY, str);
	} else {
		database->insert(SETT_MAX_PER_DAY, str);
	}
}

float Preferenze::get_max_per_day(void) const {
	Glib::ustring value { database->get_setting(SETT_MAX_PER_DAY) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("0.00");
	}
	return atof(value.c_str());
}

void Preferenze::set_enable_max_per_day(const Glib::ustring& str) {
	Glib::ustring psw;
	if (str == Glib::ustring("si")) {
		psw = "true";
	} else if (str == Glib::ustring("no")) {
		psw = "false";
	} else if (str == Glib::ustring("true") || str == Glib::ustring("false")) {
		psw = str;
	}
	Glib::ustring value { database->get_setting(SETT_ENABLE_MAX_PD) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_ENABLE_MAX_PD, psw);
	} else {
		database->insert(SETT_ENABLE_MAX_PD, psw);
	}
}

bool Preferenze::get_enable_max_per_day(void) const {
	Glib::ustring value { database->get_setting(SETT_ENABLE_MAX_PD) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("false");
	}
	return value.compare(Glib::ustring("true")) == 0;
}

void Preferenze::set_enable_debug_log(const Glib::ustring& str) {
	Glib::ustring psw;
	if (str == Glib::ustring("si")) {
		psw = "true";
	} else if (str == Glib::ustring("no")) {
		psw = "false";
	} else if (str == Glib::ustring("true") || str == Glib::ustring("false")) {
		psw = str;
	}
	Glib::ustring value { database->get_setting(SETT_DEBUG_LOG) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_DEBUG_LOG, psw);
	} else {
		database->insert(SETT_DEBUG_LOG, psw);
	}
}

bool Preferenze::get_enable_debug_log(void) const {
	Glib::ustring value { database->get_setting(SETT_DEBUG_LOG) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("false");
	}
	return value.compare(Glib::ustring("true")) == 0;
}

void Preferenze::set_extra_charge(const Glib::ustring& str) {
	Glib::ustring value { database->get_setting(SETT_EXTRA_CHARGE) };
	if (value.compare(Glib::ustring("")) != 0) {
		database->update(SETT_EXTRA_CHARGE, str);
	} else {
		database->insert(SETT_EXTRA_CHARGE, str);
	}
}

float Preferenze::get_extra_charge(void) const {
	Glib::ustring value { database->get_setting(SETT_EXTRA_CHARGE) };
	if (value.compare(Glib::ustring("")) == 0) {
		value.assign("0.00");
	}
	return atof(value.c_str());
}
