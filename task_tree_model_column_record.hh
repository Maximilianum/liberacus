/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * task_tree_model_column_record.h
 * Copyright (C) 2015 - 2019 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TASK_TREE_MODEL_COLUMN_RECORD_H_
#define _TASK_TREE_MODEL_COLUMN_RECORD_H_

class TaskTreeModelColumnRecord: public Gtk::TreeModelColumnRecord 
{
public:
	 TaskTreeModelColumnRecord()
	{ add(garment); add(description); add(price); }
	Gtk::TreeModelColumn<Glib::ustring> garment;
	Gtk::TreeModelColumn<Glib::ustring> description;
	Gtk::TreeModelColumn<float> price;

protected:

private:

};

#endif // _TASK_TREE_MODEL_COLUMN_RECORD_H_

