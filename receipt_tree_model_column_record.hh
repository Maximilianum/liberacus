/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * receipt_tree_model_column_record.hh
 * Copyright (C) 2015 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.co>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RECEIPT_TREE_MODEL_COLUMN_RECORD_H_
#define _RECEIPT_TREE_MODEL_COLUMN_RECORD_H_

class ReceiptTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
	ReceiptTreeModelColumnRecord()
		{ add(number); add(customer); add(date_acceptance); add(total); add(deposit); add(date_deliver); add(urgent); add(delivered); add(empty); add(time_acceptance); add(time_deliver); }
	Gtk::TreeModelColumn<Glib::ustring> number;
	Gtk::TreeModelColumn<Glib::ustring> customer;
	Gtk::TreeModelColumn<Glib::ustring> date_acceptance;
	Gtk::TreeModelColumn<float> total;
	Gtk::TreeModelColumn<float> deposit;
	Gtk::TreeModelColumn<Glib::ustring> date_deliver;
	Gtk::TreeModelColumn<bool> urgent;
	Gtk::TreeModelColumn<bool> delivered;
	Gtk::TreeModelColumn<Glib::ustring> empty;
	Gtk::TreeModelColumn<time_t> time_acceptance;
	Gtk::TreeModelColumn<time_t> time_deliver;
};

#endif // _RECEIPT_TREE_MODEL_COLUMN_RECORD_H_

