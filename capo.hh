/*
 * capo.hh
 * Copyright (C) 2014 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CAPO_HH_
#define _CAPO_HH_

#include <vector>
#include "custom_date.hh"
#include "prestazione.hh"

typedef struct {
  Glib::ustring customer;
  CustomDate date;
  int index;
  CustomDate pickup;
} task_info;

class Capo {
public:
  static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
  static int retrieve_capo(void *data, int argc, char **argv, char **azColName);
  inline Capo() : numero (0), capo (Glib::ustring()) {}
  inline Capo(const Glib::ustring &str, unsigned int n) : numero (n), capo (str) {}
  Capo& operator=(const Capo &cp);
  bool operator<(const Capo &cp) const;
  bool operator==(const Capo &cp) const;
  inline void set_numero(unsigned int value) { numero = value; }
  inline void set_numero(const Glib::ustring& str) { numero = atoi(str.c_str()); }
  inline void set_numero(const char* ptr) { numero = atoi(ptr); }
  inline unsigned int get_numero(void) const { return numero; }
  inline Glib::ustring get_numero_string(void) const { return Glib::ustring::format(numero); }
  inline void set_capo(const Glib::ustring& str) { capo.assign(str); }
  inline void set_capo(const char* ptr) { capo.assign(Glib::ustring(ptr)); }
  inline Glib::ustring get_capo(void) const { return capo; }
  void set_prestazioni(const std::vector<Prestazione> &list_prst);
  inline const std::vector<Prestazione>* get_const_prestazioni(void) const { return &prestazioni; }
  bool aggiungi_prestazione(const Prestazione &prst);
  bool set_prestazione_completata(const Glib::ustring& descr, bool flag);
  void completa_prestazioni(void);
protected:
  std::vector<Prestazione>::iterator get_prestazione(const Glib::ustring& descr, bool* result);
private:
  static Glib::ustring sqlite_table;
  unsigned int numero;
  Glib::ustring capo;
  std::vector<Prestazione> prestazioni;
};

#endif // _CAPO_HH_

