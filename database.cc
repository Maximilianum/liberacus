/*
 * database.cc
 * Copyright (C) 2017 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.hh"

DataBase::DataBase(const Glib::ustring& db_path) {
  // try to open an existing database
  int result = sqlite3_open_v2(db_path.c_str(), &sqlt_db, SQLITE_OPEN_READWRITE, NULL);
  if (result != SQLITE_OK) {
    // create a new database
    result = sqlite3_open_v2(db_path.c_str(), &sqlt_db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE, NULL);
    if (result == SQLITE_OK) {
      origin = DATABASE_ORIGIN::created;
      // create SQL TABLE RUBRICA
      Glib::ustring sql = Glib::ustring("CREATE ") + Cliente::get_sqlite_table();
      int rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table RUBRICA: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE PRESTAZIONE
      sql = Glib::ustring("CREATE ") + Prestazione::get_sqlite_table();
      rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table PRESTAZIONI: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE RICEVUTA
      sql = Glib::ustring("CREATE ") + Ricevuta::get_sqlite_table();
      rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table RICEVUTE: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create index for column DATA_ACC on table RICEVUTE
      sql = Glib::ustring("CREATE UNIQUE INDEX date_ric_index ON RICEVUTE (DATA_ACC);");
      rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating index on table RICEVUTE: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE ORDINE
      sql = Glib::ustring("CREATE ") + Capo::get_sqlite_table();
      rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table ORDINI: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create index for column DATA_ACC on table ORDINI
      sql = Glib::ustring("CREATE INDEX date_ord_index ON ORDINI (DATA_ACC);");
      rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating index on table ORDINI: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      // create SQL TABLE SETTINGS
      sql = Glib::ustring("CREATE ") + Preferenze::get_sqlite_settings_table();
      rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while creating table SETTINGS: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
      sql = Glib::ustring("PRAGMA user_version = 2;");
      rc = DataBase::exec(sqlt_db, sql, NULL, NULL);
      if (rc != SQLITE_OK) {
	std::cerr << "Error while setting user version: " << sqlite3_errmsg(sqlt_db) << std::endl;
      }
    } else {
      std::cerr << "Can't open database: " << sqlite3_errmsg(sqlt_db) << std::endl;
    }
  } else {
    origin = DATABASE_ORIGIN::opened;
    //std::cout << "database version is " << DataBase::get_version(sqlt_db) << std::endl;
  }
}

/*
 * member functions
 */

// clienti

std::vector<Cliente> DataBase::get_clienti(void) {
  std::vector<Cliente> clienti;
  Glib::ustring sql { Glib::ustring("SELECT * from RUBRICA;") };
  if (DataBase::exec(sqlt_db, sql, Cliente::retrieve_cliente, &clienti) == SQLITE_OK) {
    return clienti;
  } else {
    throw DBError { std::string { "Database::get_clienti() exec failed!" } };
  }
}

std::vector<Glib::ustring> DataBase::get_nomi_clienti(void) {
  std::vector<Glib::ustring> strings;
  Glib::ustring sql { Glib::ustring("SELECT NOME from RUBRICA;") };
  if (DataBase::exec(sqlt_db, sql, retrieve_string, &strings) == SQLITE_OK) {
    return strings;
  } else {
    throw DBError { std::string { "Database::get_nomi_clienti() exec failed!" } };
  }
}

Cliente DataBase::get_cliente(const Glib::ustring& name) {
  std::vector<Cliente> clienti;
  Glib::ustring sql { Glib::ustring::compose("SELECT * from RUBRICA where NOME='%1';", get_sqlite_safe_string(name)) };
  if (DataBase::exec(sqlt_db, sql, Cliente::retrieve_cliente, &clienti) == SQLITE_OK) {
    if (clienti.size() > 0) {
      return clienti.front();
    } else {
      throw DBEmptyResultException { std::string { "Database::get_cliente() empty result!" } };
    }
  } else {
    throw DBError { std::string { "Database::get_cliente() exec failed!" } };
  }
}

void DataBase::insert(const Cliente& cliente) {
  Glib::ustring sql { Glib::ustring::compose("INSERT INTO RUBRICA (NOME,TELEFONO) VALUES ('%1', '%2');", get_sqlite_safe_string(cliente.get_nome()), get_sqlite_safe_string(cliente.get_telefono())) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert() exec failed!" } };
  }
}

void DataBase::update(const Glib::ustring& name, const Cliente& cliente) {
  Glib::ustring sql { Glib::ustring::compose("UPDATE RUBRICA set NOME='%1', TELEFONO='%2' where NOME='%3';", get_sqlite_safe_string(cliente.get_nome()), get_sqlite_safe_string(cliente.get_telefono()), get_sqlite_safe_string(name)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update() exec failed!" } };
  }
}

void DataBase::delete_cliente(const Glib::ustring& name) {
  Glib::ustring sql { Glib::ustring::compose("DELETE from RUBRICA where NOME='%1';", get_sqlite_safe_string(name)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_client() exec failed!" } };
  }
}

bool DataBase::is_name_present(const Glib::ustring& name) {
  std::vector<Cliente> clienti;
  Glib::ustring sql { Glib::ustring::compose("SELECT NOME from RUBRICA where NOME='%1';", get_sqlite_safe_string(name)) };
  if (DataBase::exec(sqlt_db, sql, Cliente::retrieve_cliente, &clienti) == SQLITE_OK) {
    return clienti.size() > 0;
  } else {
    throw DBError { std::string { "Database::is_name_present() exec failed!" } };
  }
}

// prestazioni

std::vector<Prestazione> DataBase::get_prestazioni(void) {
  std::vector<Prestazione> prestazioni;
  Glib::ustring sql { Glib::ustring("SELECT * from PRESTAZIONI;") };
  if (DataBase::exec(sqlt_db, sql, Prestazione::retrieve_prestazione, &prestazioni) == SQLITE_OK) {
    return prestazioni;
  } else {
    throw DBError { std::string { "Database::get_prestazioni() exec failed!" } };
  }
}

std::vector<Glib::ustring> DataBase::get_descrizione_prestazioni_per_capo(const Glib::ustring& capo) {
  std::vector<Glib::ustring> strings;
  Glib::ustring sql { Glib::ustring::compose("SELECT DESCRIZIONE from PRESTAZIONI where CAPO='%1';", get_sqlite_safe_string(capo)) };
  if (DataBase::exec(sqlt_db, sql, retrieve_string, &strings) == SQLITE_OK) {
    return strings;
  } else {
    throw DBError { std::string { "Database::get_descrizione_prestazioni_per_capo() exec failed!" } };
  }
}

std::vector<Glib::ustring> DataBase::get_capo_prestazioni(void) {
  std::vector<Glib::ustring> strings;
  Glib::ustring sql { Glib::ustring("SELECT CAPO from PRESTAZIONI") };
  if (DataBase::exec(sqlt_db, sql, retrieve_string, &strings) == SQLITE_OK) {
    return strings;
  } else {
    throw DBError { std::string { "Database::get_capo_prestazioni() exec failed!" } };
  }
}

void DataBase::insert(const Prestazione& prestazione) {
  Glib::ustring sql { Glib::ustring::compose("INSERT INTO PRESTAZIONI (CAPO,DESCRIZIONE,PREZZO) VALUES ('%1','%2','%3');", get_sqlite_safe_string(prestazione.get_capo()), get_sqlite_safe_string(prestazione.get_descrizione()), prestazione.get_prezzo()) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert() exec failed!" } };
  }
}

void DataBase::update(const Glib::ustring& capo, const Glib::ustring& descr, const Prestazione& prestazione) {
  Glib::ustring sql { Glib::ustring::compose("UPDATE PRESTAZIONI set CAPO='%1', DESCRIZIONE='%2', PREZZO='%3' where CAPO='%4' AND DESCRIZIONE='%5';", get_sqlite_safe_string(prestazione.get_capo()), get_sqlite_safe_string(prestazione.get_descrizione()), prestazione.get_prezzo(), get_sqlite_safe_string(capo), get_sqlite_safe_string(descr)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update() exec failed!" } };
  }
}

void DataBase::delete_prestazione(const Glib::ustring& capo, const Glib::ustring& descr) {
  Glib::ustring sql { Glib::ustring::compose("DELETE from PRESTAZIONI where CAPO='%1' AND DESCRIZIONE='%2';", get_sqlite_safe_string(capo), get_sqlite_safe_string(descr)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_prestazione() exec failed!" } };
  }
}

bool DataBase::is_prestazione_present(const Glib::ustring& capo, const Glib::ustring& descr) {
  std::vector<Prestazione> prestazioni;
  Glib::ustring sql { Glib::ustring::compose("SELECT CAPO DESCRIZIONE from PRESTAZIONI where CAPO='%1' AND DESCRIZIONE='%2';", get_sqlite_safe_string(capo), get_sqlite_safe_string(descr)) };
  if (DataBase::exec(sqlt_db, sql, Prestazione::retrieve_prestazione, &prestazioni) == SQLITE_OK) {
    return prestazioni.size() > 0;
  } else {
    throw DBError { std::string { "Database::is_prestazione_present() exec failed!" } };
  }
}

Glib::ustring DataBase::get_prezzo_prestazione(const Glib::ustring& capo, const Glib::ustring& descr) {
  std::vector<Glib::ustring> strings;
  Glib::ustring sql { Glib::ustring::compose("SELECT PREZZO from PRESTAZIONI where CAPO='%1' AND DESCRIZIONE='%2';", get_sqlite_safe_string(capo), get_sqlite_safe_string(descr)) };
  if (DataBase::exec(sqlt_db, sql, retrieve_string, &strings) == SQLITE_OK) {
    return strings.size() > 0 ? strings.front() : Glib::ustring("");
  } else {
    throw DBError { std::string { "Database::get_prezzo_prestazione() exec failed!" } };
  }
}

// ricevute

std::vector<Ricevuta> DataBase::get_ricevute(const Glib::ustring& where, SELECT details) {
  std::vector<Ricevuta> ricevute { select_ricevute(where, details) };
  return ricevute;
}

std::vector<std::pair<task_info, Prestazione>> DataBase::get_tasks_from(const CustomDate& cd) {
  std::vector<std::pair<task_info, Prestazione>> tasks;
  CustomDate sod { CustomDate::start_of_day(cd) };
  Glib::ustring sql { Glib::ustring::compose("SELECT RICEVUTE.NOME, " \
		                             "RICEVUTE.DATA_ACC, " \
					     "ORDINI.INDICE, " \
	                         	     "RICEVUTE.DATA_RIT, " \
		                             "ORDINI.CAPO, " \
					     "ORDINI.DESCRIZIONE, " \
					     "ORDINI.PREZZO, " \
		                             "ORDINI.COMPLETATA " \
		                             "from RICEVUTE " \
		                             "inner join ORDINI on RICEVUTE.DATA_ACC = ORDINI.DATA_ACC " \
					     "where RICEVUTE.DATA_RIT >= '%1' ORDER BY RICEVUTE.DATA_RIT ASC;", sod.get_time_t()) };
  if (DataBase::exec(sqlt_db, sql, Capo::retrieve_capo, &tasks) == SQLITE_OK) {
    return tasks;
  } else {
    throw DBError { std::string { "Database::get_tasks_from() exec failed!" } };
  }
}

void DataBase::insert(const Ricevuta& rcv) {
  Glib::ustring sql { Glib::ustring::compose("INSERT INTO RICEVUTE (DATA_ACC,NUMERO,NOME,ACCONTO,DATA_RIT,URGENTE,CONSEGNATO) VALUES ('%1', '%2', '%3', '%4', '%5', '%6', '%7');", rcv.get_date_acc_unix(), rcv.get_numero_string(), get_sqlite_safe_string(rcv.get_nome_cliente()), rcv.get_acconto(), rcv.get_date_rit_unix(), rcv.is_urgente(), rcv.is_consegnato()) };
  const std::vector<Capo>* capi { rcv.get_const_capi() };
  std::vector<Capo>::const_iterator iter_c = capi->cbegin();
  unsigned int index = 0;
  while (iter_c != capi->cend())
    {
      const std::vector<Prestazione>* prestazioni { iter_c->get_const_prestazioni() };
      std::vector<Prestazione>::const_iterator iter_p = prestazioni->cbegin();
      while (iter_p != prestazioni->end())
	{
	  sql.append(Glib::ustring::compose("INSERT INTO ORDINI (DATA_ACC,CAPO,INDICE,DESCRIZIONE,PREZZO,COMPLETATA) VALUES ('%1', '%2', '%3', '%4', '%5', '%6');", rcv.get_date_acc_unix(), get_sqlite_safe_string(iter_p->get_capo()), index, get_sqlite_safe_string(iter_p->get_descrizione()), iter_p->get_prezzo(), iter_p->get_completata()));
	  iter_p++;
	}
      index++;
      iter_c++;
    }
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert() exec failed!" } };
  }
}

void DataBase::update(time_t date, const Ricevuta& rcv) {
  Glib::ustring sql { Glib::ustring::compose("UPDATE RICEVUTE set DATA_ACC='%1', NUMERO='%2', NOME='%3', ACCONTO='%4', DATA_RIT='%5', URGENTE='%6', CONSEGNATO='%7' where DATA_ACC='%8';", rcv.get_date_acc_unix(), rcv.get_numero_string(), get_sqlite_safe_string(rcv.get_nome_cliente()), rcv.get_acconto(), rcv.get_date_rit_unix(), rcv.is_urgente(), rcv.is_consegnato(), date) };
  sql.append(Glib::ustring::compose("DELETE from ORDINI where DATA_ACC='%1';", date));
  const std::vector<Capo>* capi { rcv.get_const_capi() };
  std::vector<Capo>::const_iterator iter_c = capi->cbegin();
  unsigned int index = 0;
  while (iter_c != capi->cend())
    {
      const std::vector<Prestazione>* prestazioni { iter_c->get_const_prestazioni() };
      std::vector<Prestazione>::const_iterator iter_p = prestazioni->cbegin();
      while (iter_p != prestazioni->cend())
	{
	  sql.append(Glib::ustring::compose("INSERT INTO ORDINI (DATA_ACC,CAPO,INDICE,DESCRIZIONE,PREZZO,COMPLETATA) VALUES ('%1', '%2', '%3', '%4', '%5', '%6');", rcv.get_date_acc_unix(), get_sqlite_safe_string(iter_p->get_capo()), index, get_sqlite_safe_string(iter_p->get_descrizione()), iter_p->get_prezzo(), iter_p->get_completata()));
	  iter_p++;
	}
      index++;
      iter_c++;
    }
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update(1) exec failed!" } };
  }
}

void DataBase::update(time_t date, const Ricevuta& rcv, MEMB_FIELD field) {
  int result;
  Glib::ustring sql;
  switch (field) {
  case MEMB_FIELD::date_acc:
    sql = Glib::ustring::compose("UPDATE RICEVUTE set DATA_ACC='%1' where DATA_ACC='%2';", rcv.get_date_acc_unix(), date);
    break;
  case MEMB_FIELD::nome:
    sql = Glib::ustring::compose("UPDATE RICEVUTE set NOME='%1' where DATA_ACC='%2';", get_sqlite_safe_string(rcv.get_nome_cliente()), date);
    break;
  case MEMB_FIELD::acconto:
    sql = Glib::ustring::compose("UPDATE RICEVUTE set ACCONTO='%1' where DATA_ACC='%2';", rcv.get_acconto(), date);
    break;
  case MEMB_FIELD::date_rit:
    sql = Glib::ustring::compose("UPDATE RICEVUTE set DATA_RIT='%1' where DATA_ACC='%2';", rcv.get_date_rit_unix(), date);
    break;
  case MEMB_FIELD::urg:
    sql = Glib::ustring::compose("UPDATE RICEVUTE set URGENTE='%1' where DATA_ACC='%2';", rcv.is_urgente(), date);
    break;
  case MEMB_FIELD::cons:
    sql = Glib::ustring::compose("UPDATE RICEVUTE set CONSEGNATO='%1' where DATA_ACC='%2';", rcv.is_consegnato(), date);
    break;
  }
  if (sql.compare(Glib::ustring("")) != 0) {
    throw DBError { std::string { "Database::update(2) MEMB_FIELD::num can't be updated!" } };
  }
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update(2) exec failed!" } };
  }
}

void DataBase::update_nome(const Glib::ustring& old_name, const Glib::ustring& new_name) {
  Glib::ustring sql { Glib::ustring::compose("UPDATE RICEVUTE set NOME='%1' where NOME='%2';", get_sqlite_safe_string(new_name), get_sqlite_safe_string(old_name)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update_nome() exec failed!" } };
  }
}

void DataBase::update_task_completed(time_t date, int index, const Glib::ustring& descr, bool flag) {
  Glib::ustring sql { Glib::ustring::compose("UPDATE ORDINI set COMPLETATA='%1' where DATA_ACC='%2' and INDICE='%3' and DESCRIZIONE='%4';", flag, date, index, get_sqlite_safe_string(descr)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update_task_completed() exec failed!" } };
  }
}

void DataBase::update_receipt_completed(time_t date, bool flag) {
  Glib::ustring sql { Glib::ustring::compose("UPDATE ORDINI set COMPLETATA='%1' where DATA_ACC='%2';", flag, date) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update_receipt_completed() exec failed!" } };
  }
}

void DataBase::delete_ricevuta(time_t date) {
  Glib::ustring sql { Glib::ustring::compose("DELETE from RICEVUTE where DATA_ACC='%1'; ", date) };
  sql.append(Glib::ustring::compose("DELETE from ORDINI where DATA_ACC='%1';", date));
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::delete_ricevuta() exec failed!" } };
  }
}

bool DataBase::is_ricevuta_present(time_t date) {
  std::vector<Ricevuta> ricevute;
  Glib::ustring sql { Glib::ustring::compose("SELECT DATA_ACC NUMERO from RICEVUTE where DATA_ACC='%1';", date) };
  if (DataBase::exec(sqlt_db, sql, Ricevuta::retrieve_ricevuta, &ricevute) == SQLITE_OK) {
    return ricevute.size() > 0;
  } else {
    throw DBError { std::string { "Database::is_ricevuta_present() exec failed!" } };
  }
}

Ricevuta DataBase::get_ricevuta(time_t date) {
  Glib::ustring sql { Glib::ustring::compose("where RICEVUTE.DATA_ACC='%1'", date) };
  std::vector<Ricevuta> ricevute { select_ricevute(sql) };
  if (ricevute.size() > 0) {
    return ricevute.front();
  } else {
    throw DBEmptyResultException { std::string { "Database::get_ricevuta() empty result!" } };
  }
}

CustomDate DataBase::update_last_number_ricevuta(void) {
  CustomDate cdate;
  std::vector<Ricevuta> ricevute;
  Glib::ustring sql { Glib::ustring("SELECT DATA_ACC,NUMERO,NOME from RICEVUTE ORDER BY DATA_ACC DESC LIMIT 1;") };
  if (DataBase::exec(sqlt_db, sql, Ricevuta::retrieve_ricevuta, &ricevute) == SQLITE_OK) {
    std::vector<Ricevuta>::const_iterator iter_r { ricevute.cbegin() };
    if (iter_r != ricevute.cend()) {
      Ricevuta::set_numero_ricevute_emesse(iter_r->get_numero());
      cdate = iter_r->get_data_accettazione();
    }
  } else {
    throw DBError { std::string { "Database::update_last_number_ricevuta() exec failed!" } };
  }
  return cdate;
}

// entry-value string settings

Glib::ustring DataBase::get_setting(const Glib::ustring& entry) {
  std::vector<Glib::ustring> strings;
  Glib::ustring sql { Glib::ustring::compose("SELECT VALUE from SETTINGS where ENTRY='%1';",get_sqlite_safe_string(entry)) };
  if (DataBase::exec(sqlt_db, sql, retrieve_str_setting, &strings) == SQLITE_OK) {
    return strings.size() > 0 ? strings.front() : Glib::ustring("");
  } else {
    throw DBError { std::string { "Database::get_setting() exec failed!" } };
  }
}

void DataBase::insert(const Glib::ustring& entry, const Glib::ustring& value) {
  Glib::ustring sql { Glib::ustring::compose("INSERT INTO SETTINGS (ENTRY,VALUE) VALUES ('%1','%2');",get_sqlite_safe_string(entry),get_sqlite_safe_string(value)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::insert() exec failed!" } };
  }
}

void DataBase::update(const Glib::ustring& entry, const Glib::ustring& value) {
  Glib::ustring sql { Glib::ustring::compose("UPDATE SETTINGS set VALUE='%1' where ENTRY='%2';",get_sqlite_safe_string(value),get_sqlite_safe_string(entry)) };
  if (DataBase::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
    throw DBError { std::string { "Database::update() exec failed!" } };
  }
}

std::vector<Ricevuta> DataBase::select_ricevute(const Glib::ustring& where, SELECT details) {
  std::vector<Ricevuta> ricevute;
  Glib::ustring sql;
  if (details == SELECT::full) {
    sql = Glib::ustring::compose("SELECT " \
				 "RICEVUTE.DATA_ACC, " \
				 "RICEVUTE.NUMERO, " \
				 "RICEVUTE.NOME, " \
				 "RICEVUTE.ACCONTO, " \
				 "RICEVUTE.DATA_RIT, " \
				 "RICEVUTE.URGENTE, " \
				 "RICEVUTE.CONSEGNATO, " \
				 "ORDINI.CAPO, " \
				 "ORDINI.INDICE, " \
				 "ORDINI.DESCRIZIONE, " \
				 "ORDINI.PREZZO, " \
				 "ORDINI.COMPLETATA " \
				 "from RICEVUTE " \
				 "inner join ORDINI on RICEVUTE.DATA_ACC = ORDINI.DATA_ACC " \
				 "%1 ORDER BY RICEVUTE.DATA_ACC DESC;", where);
  } else {
    sql = Glib::ustring::compose("SELECT * from RICEVUTE %1 ORDER BY DATA_ACC DESC;", where);
  }
  if (DataBase::exec(sqlt_db, sql, Ricevuta::retrieve_ricevuta, &ricevute) == SQLITE_OK) {
    return ricevute;
  } else {
    throw DBError { std::string { "Database::select_ricevute() exec failed!" } };
  }
}

/*
 * static functions
 */

int DataBase::retrieve_string(void *data, int argc, char **argv, char **azColName) {
  Glib::ustring str;
  std::vector<Glib::ustring>* vec { static_cast<std::vector<Glib::ustring>*>(data) };
  if (argc > 0 && argv[0]) {
    Glib::ustring str { argv[0] };
    vec->push_back(str);
  }
  return 0;
}

int DataBase::retrieve_str_setting(void *data, int argc, char **argv, char **azColName) {
  Glib::ustring val;
  std::vector<std::string>* vec { static_cast<std::vector<std::string>*>(data) };
  for(int i=0; i<argc; i++) {
    if (Glib::ustring(azColName[i]) == Glib::ustring("VALUE") && argv[i]) {
      val = argv[i];
    }
  }
  vec->push_back(val);
  return 0;
}
