/*
 * controller.hh
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
LiberAcus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LiberAcus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#define APP_FILE_NAME "liberacus"
#define APP_NAME "Liberacus"
#define VERSION "3.5"
#define BUILD_NUMBER "344"
#define COPYRIGHT "Copyright © 2012 - 2022 Massimiliano Maniscalco"
#define APP_WEBSITE "https://gitlab.com/Maximilianum/liberacus"

#include <gtkmm.h>
#include <giomm/file.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <set>
#include <map>
#include <unordered_map>
#include "ctrl_app_engine.hh"
#include "database.hh"
#include "cliente.hh"
#include "prestazione.hh"
#include "ricevuta.hh"
#include "combobox_tree_model_column_record.hh"
#include "summary_tree_model_column_record.hh"
#include "customer_tree_model_column_record.hh"
#include "task_tree_model_column_record.hh"
#include "receipt_tree_model_column_record.hh"
#include "receipt_task_tree_model_column_record.hh"
#include "report_receipt_tree_model_column_record.hh"
#include "preferenze.hh"
#include "custom_date.hh"

#define PAGE_SUMMARY			0
#define PAGE_RECEIPT			1
#define PAGE_TASKS		       	2
#define PAGE_CUSTOMERS			3
#define PAGINA_RESOCONTO		4

enum class EXPORT_DATA { ricevute, prestazioni, clienti };

class Controller : public CtrlAppEngine {
public:
  static void calculate_pickup_date(CustomDate* tempo);
  Controller(Glib::RefPtr<Gtk::Builder> bldr, Glib::RefPtr<Gtk::Application> app, const Glib::ustring& main_path);
  ~Controller();
  // debugging functions
  void open_log(void);
  void close_log(void);
  inline bool is_log_active(void) const { return preferenze.get_enable_debug_log(); }
  void write_log(const Glib::ustring& str);
protected:
  void connect_signals(void);
  // main win
  bool on_main_window_focus_in(GdkEventFocus *event);
  bool on_main_window_focus_out(GdkEventFocus *event);
  void on_main_notebook_page_switch(Gtk::Widget *page, guint page_num);
  void on_items_treeselection_changed(void);
  void load_customers_in_treeview(Glib::RefPtr<Gtk::ListStore> liststore);
  // main toolbar
  void on_new_ricevuta_action(void);
  void on_new_prestazione_action(void);
  void on_new_cliente_action(void);
  void on_edit_item_action(void);
  void edit_selected_item(void);
  void on_remove_item_action(void);
  void remove_selected_items(void);
  void on_stampa_chiusura_cassa_action(void);
  // main menu
  void on_page_setup_action(void);
  void on_print_ricevuta_action(void);
  void on_print_etichette_action(void);
  void on_preferenze_action(void);
  inline void on_visualizza_action(const unsigned short val) { main_notebook->set_current_page(val); }
  void on_dialog_response(int response_id, Gtk::Dialog *dialog);
  /**
   * main notebook
   * */
  void on_treeview_clicked(GdkEventButton *event);
  // page summary
  void load_tasks_in_summary_treeview(void);
  void on_task_completed_toggled(const Glib::ustring &path);
  float totale_ricevute_del_giorno(const CustomDate& tempo);
  // page receipts
  void load_ricevute_in_treeview(void);
  void ricevute_treeview_remove_selected_rows(const std::vector<Gtk::TreePath> &sel);
  void ricevute_treeview_edit_selected_rows(const std::vector<Gtk::TreePath> &sel);
  void on_ricevuta_urgente_toggled(const Glib::ustring &path);
  void on_ricevuta_consegnato_toggled(const Glib::ustring &path);
  // page tasks
  void load_prestazioni_in_treeview(void);
  void prestazioni_treeview_remove_selected_rows(const std::vector<Gtk::TreePath> &sel);
  void prestazioni_treeview_edit_selected_rows(const std::vector<Gtk::TreePath> &sel);
  // page customers
  void load_clienti_in_treeview(void);
  void clienti_treeview_remove_selected_rows(const std::vector<Gtk::TreePath> &sel);
  void clienti_treeview_edit_selected_rows(const std::vector<Gtk::TreePath> &sel);
  // page resoconto
  void aggiorna_pagina_resoconto(void);
  // nuova ricevuta window
  inline void connect_capo_popup(void) { capo_popup_connection = selezione_capo_combobox->signal_changed().connect(sigc::mem_fun(this, &Controller::load_prestazione_popup_button)); }
  inline void connect_prestazione_popup(void) { prestazione_popup_connection = selezione_prestazione_combobox->signal_changed().connect(sigc::mem_fun(this, &Controller::on_selezione_prestazione_popup_changed)); }
  inline void connect_prestazioni_ricevuta(void) { prestazioni_ricevuta_connection = prestazioni_ricevuta_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_prestazioni_ricevuta_selection_changed)); }
  void update_mappa_capi(void);
  void load_capo_popup_button(void);
  void load_prestazione_popup_button(void);
  void on_trova_clienti_action(void);
  void on_cambia_data_ricevuta_action(Gtk::Button *sender);
  void load_prestazioni_ricevuta_in_treeview(std::vector<Capo>::const_iterator iter, std::vector<Capo>::const_iterator end);
  Gtk::TreeModel::Path get_prestazione_ricevuta_selezionata(void);
  void on_prestazioni_ricevuta_selection_changed(void);
  bool update_capo_popup_button(const Glib::ustring capo);
  void update_prestazioni_popup_button(const Glib::ustring descrizione);
  void on_prestazioni_ricevuta_row_expanded(const Gtk::TreeModel::iterator& iter, const Gtk::TreeModel::Path& path);
  void on_selezione_prestazione_popup_changed(void);
  void on_add_prestazione_ricevuta_action(void);
  void riordina_numeri_capi(void);
  void on_remove_prestazione_ricevuta_action(void);
  void on_descrizione_modificata(const Glib::ustring& path, const Glib::ustring& new_text);
  void on_prezzo_modificato(const Glib::ustring& path, const Glib::ustring& new_text);
  void update_totale_nuova_ricevuta(float delta);
  float ricalcola_totale_nuova_ricevuta(void);
  void on_pagamento_anticipato_toggled(void);
  void on_urgente_toggled(void);
  void on_conferma_ricevuta_button_action(void);
  // nuova prestazione window
  void load_capi_in_prestazione_completion(void);
  void on_new_prestazione_confirm_button_clicked(void);
  // nuovo cliente window
  void on_new_cliente_confirm_button_clicked(void);
  // calendario window
  void on_calendario_confirm_button_clicked(void);
  // preferenze window
  void show_preferenze_panel(void);
  void on_visualizza_numero_toggled(void);
  void update_visualizza_numero(bool visualizza);
  void on_salva_preferenze_action(void);
  void eliminazione_automatica(void);
  // suggerimento clienti window
  void on_suggerimento_clienti_double_clicked(GdkEventButton *event);
  // printing
  Glib::ustring get_stampa_prestazioni(const Ricevuta& rcpt, size_t line );
  void on_printoperation_status_changed(const Glib::RefPtr<Gtk::PrintOperation>& operation);
  void on_printoperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr<Gtk::PrintOperation>& operation);
  void print_or_preview_ricevuta(Gtk::PrintOperationAction print_action, const Glib::ustring &str1);
  void print_or_preview_etichetta(Gtk::PrintOperationAction print_action, const Glib::ustring &str1);
  void print_or_preview_chiusura(Gtk::PrintOperationAction print_action, const Glib::ustring &str1);
  // security
  int request_password(void);
  void on_request_password_ok(void);
  void on_request_password_cancel(void);
  inline bool check_password(void) { return check_password_dialog_entry->get_text() == preferenze.get_password(); }
  void on_password_error(void);
private:
  DataBase* database;
  unsigned int ricevute_caricate;
  unsigned int prestazioni_caricate;
  unsigned int clienti_caricati;
  Glib::ustring pref_path;
  Preferenze preferenze;
  std::map<Glib::ustring, int> mappa_capi;
  // debug
  Glib::ustring debug_log_path;
  std::ofstream ofs_log;
  // main window
  Gtk::Notebook* main_notebook;
  Gtk::Statusbar* main_statusbar;
  unsigned short main_notebook_page;
  int stbrItemSelId;
  int stbrItemDiskId;
  int stbrPrintId;
  // main toolbar
  Gtk::Toolbar* main_toolbar;
  Gtk::Button* nuova_ricevuta_button;
  Gtk::Button* nuova_prestazione_button;
  Gtk::Button* nuovo_cliente_button;
  Gtk::ToolButton* modifica_selezione_toolbutton;
  Gtk::ToolButton* elimina_selezione_toolbutton;
  Gtk::ToolButton* stampa_chiusura_toolbutton;
  // main menu
  Gtk::MenuBar* main_menubar;
  Gtk::MenuItem* nuova_ricevuta_menuitem;
  Gtk::MenuItem* nuova_prestazione_menuitem;
  Gtk::MenuItem* nuovo_cliente_menuitem;
  Gtk::MenuItem* imposta_pagina_menuitem;
  Gtk::MenuItem* stampa_ricevuta_menuitem;
  Gtk::MenuItem* stampa_etichette_menuitem;
  Gtk::MenuItem* stampa_chiusura_menuitem;
  Gtk::MenuItem* quit_menuitem;
  Gtk::MenuItem* modifica_selezione_menuitem;
  Gtk::MenuItem* elimina_selezione_menuitem;
  Gtk::MenuItem* preferenze_menuitem;
  Gtk::MenuItem* settimana_menuitem;
  Gtk::MenuItem* ricevute_menuitem;
  Gtk::MenuItem* prestazioni_menuitem;
  Gtk::MenuItem* clienti_menuitem;
  Gtk::MenuItem* resoconto_menuitem;
  Gtk::MenuItem* about_menuitem;
  /**
   * main notebook
   * */
  // page summary
  SummaryTreeModelColumnRecord summary_tree_mod_col_record;
  Glib::RefPtr<Gtk::TreeStore> summary_treestore;
  Gtk::TreeView* summary_treeview;
  Glib::RefPtr<Gtk::TreeSelection> summary_treeselection;
  Gtk::TreeViewColumn* summary_price_treeviewcolumn;
  Gtk::CellRendererText* summary_price_cellrenderertext;
  Gtk::TreeViewColumn* summary_completed_treeviewcolumn;
  Gtk::CellRendererToggle* summary_completed_cellrenderertoggle;
  // page receipts
  ReceiptTreeModelColumnRecord receipt_tree_mod_col_record;
  Glib::RefPtr<Gtk::ListStore> receipts_liststore;
  Gtk::TreeView* receipts_treeview;
  Glib::RefPtr<Gtk::TreeSelection> receipts_treeselection;
  Gtk::TreeViewColumn* receipt_number_treeviewcolumn;
  Gtk::TreeViewColumn* receipt_deliver_treeviewcolumn;
  Gtk::TreeViewColumn* receipt_deposit_treeviewcolumn;
  Gtk::TreeViewColumn* receipt_total_treeviewcolumn;
  Gtk::TreeViewColumn* receipt_urgent_treeviewcolumn;
  Gtk::CellRendererToggle* receipt_urgent_cellrenderertoggle;
  Gtk::TreeViewColumn* receipt_delivered_treeviewcolumn;
  Gtk::CellRendererToggle* receipt_delivered_cellrenderertoggle;
  // receipts contextual menu
  Gtk::Menu* ricevute_context_menu;
  Gtk::MenuItem* modifica_ricevute_context_menuitem;
  Gtk::MenuItem* elimina_ricevute_context_menuitem;
  Gtk::MenuItem* stampa_ricevute_context_menuitem;
  Gtk::MenuItem* stampa_etichette_context_menuitem;
  // page tasks
  TaskTreeModelColumnRecord task_tree_mod_col_record;
  Glib::RefPtr<Gtk::ListStore> tasks_liststore;
  Gtk::TreeView* tasks_treeview;
  Gtk::TreeViewColumn* tasks_price_treeviewcolumn;
  Glib::RefPtr<Gtk::TreeSelection> prestazioni_treeselection;
  // page clienti
  CustomerTreeModelColumnRecord customer_tree_mod_col_record;
  Glib::RefPtr<Gtk::ListStore> clienti_liststore;
  Gtk::TreeView* clienti_treeview;
  Glib::RefPtr<Gtk::TreeSelection> clienti_treeselection;
  // menu contestuale generico
  Gtk::Menu* generic_context_menu;
  Gtk::MenuItem* edit_generic_context_menuitem;
  Gtk::MenuItem* remove_generic_context_menuitem;
  // page resoconto
  Gtk::ComboBox* selezione_periodo_combobox;
  Glib::RefPtr<Gtk::ListStore> selezione_periodo_liststore;
  Gtk::Label* resoconto_ricevute_emesse_label;
  Gtk::Label* resoconto_lavori_consegnati_label;
  Gtk::Label* resoconto_clienti_serviti_label;
  Gtk::Label* resoconto_incasso_totale_label;
  ReportReceiptTreeModelColumnRecord report_receipt_tree_mod_col_record;
  Glib::RefPtr<Gtk::ListStore> ricevute_resoconto_liststore;
  Gtk::TreeView* ricevute_resoconto_treeview;
  Glib::RefPtr<Gtk::ListStore> prestazioni_resoconto_liststore;
  Gtk::TreeViewColumn* numero_ricevuta_resoconto_treeviewcolumn;
  Gtk::TreeView* prestazioni_resoconto_treeview;
  Glib::RefPtr<Gtk::ListStore> clienti_resoconto_liststore;
  Gtk::TreeView* clienti_resoconto_treeview;
  // nuova ricevuta window
  Gtk::Window* new_ricevuta_window;
  Gtk::Entry* cliente_ricevuta_entry;
  Glib::RefPtr<Gtk::EntryCompletion> nome_cliente_entrycompletion;
  Glib::RefPtr<Gtk::ListStore> nome_cliente_liststore;
  Gtk::Button* trova_clienti_button;
  Gtk::Label* data_accettazione_ricevuta_label;
  Gtk::Button* cambia_data_ricevuta_button1;
  ReceiptTaskTreeModelColumnRecord receipt_task_tree_mod_col_record;
  Glib::RefPtr<Gtk::TreeStore> prestazioni_ricevuta_treestore;
  Gtk::TreeView* prestazioni_ricevuta_treeview;
  Glib::RefPtr<Gtk::TreeSelection> prestazioni_ricevuta_treeselection;
  sigc::connection prestazioni_ricevuta_connection;
  Gtk::TreeViewColumn* descrizione_prestazione_treeviewcolumn;
  Gtk::CellRendererText* descrizione_prestazione_cellrenderertext;
  Gtk::TreeViewColumn* prezzo_prestazione_treeviewcolumn;
  Gtk::CellRendererText* prezzo_prestazione_cellrenderertext;
  Gtk::ComboBox* selezione_capo_combobox;
  Glib::RefPtr<Gtk::ListStore> selezione_capo_liststore;
  sigc::connection capo_popup_connection;
  Gtk::ComboBox* selezione_prestazione_combobox;
  Glib::RefPtr<Gtk::ListStore> selezione_prestazione_liststore;
  sigc::connection prestazione_popup_connection;
  Gtk::Button* add_prestazione_ricevuta_button;
  Gtk::Button* rem_prestazione_ricevuta_button;
  Gtk::Label* data_ritiro_ricevuta_label;
  Gtk::Button* cambia_data_ricevuta_button2;
  Gtk::Label* totale_ricevuta_label;
  Gtk::RadioButton* nonpagato_radiobutton;
  Gtk::RadioButton* pagato_radiobutton;
  Gtk::Entry* acconto_ricevuta_entry;
  Gtk::CheckButton* stampa_ricevuta_checkbutton;
  Gtk::CheckButton* urgente_ricevuta_checkbutton;
  Gtk::Button* new_ricevuta_cancel_button;
  Gtk::Button* new_ricevuta_confirm_button;
  // nuova prestazione window
  Gtk::Window* new_prestazione_window;
  Gtk::Entry* capo_prestazione_entry;
  Glib::RefPtr<Gtk::EntryCompletion> capo_entrycompletion;
  Glib::RefPtr<Gtk::ListStore> capo_liststore;
  Gtk::Entry* descrizione_prestazione_entry;
  Gtk::Entry* prezzo_prestazione_entry;
  Gtk::Button* new_prestazione_confirm_button;
  Gtk::Button* new_prestazione_cancel_button;
  // nuovo cliente window
  Gtk::Window* new_cliente_window;
  Gtk::Entry* nome_cliente_entry;
  Gtk::Entry* telefono_cliente_entry;
  Gtk::Button* new_cliente_confirm_button;
  Gtk::Button* new_cliente_cancel_button;
  // calendario window
  Gtk::Window* calendario_window;
  Gtk::Calendar* main_calendar;
  Gtk::ComboBox* calendar_hour_combobox;
  Glib::RefPtr<Gtk::ListStore> calendar_hour_liststore;
  Gtk::ComboBox* calendar_minute_combobox;
  Glib::RefPtr<Gtk::ListStore> calendar_minute_liststore;
  Gtk::Button* calendario_cancel_button;
  Gtk::Button* calendario_confirm_button;
  // settings window
  Gtk::Window* settings_window;
  Gtk::Entry* settings_password_entry;
  Gtk::CheckButton* settings_password_checkbutton;
  Gtk::TextView* settings_header_textview;
  Glib::RefPtr<Gtk::TextBuffer> settings_header_textbuffer;
  Gtk::TextView* settings_greeting_textview;
  Glib::RefPtr<Gtk::TextBuffer> settings_greeting_textbuffer;
  Gtk::Button* settings_save_button;
  Gtk::Button* settings_cancel_button;
  Gtk::ComboBox* settings_delete_combobox;
  Glib::RefPtr<Gtk::ListStore> settings_delete_liststore;
  Gtk::ComboBox* settings_number_reset_combobox;
  Glib::RefPtr<Gtk::ListStore> settings_number_reset_liststore;
  Gtk::CheckButton* settings_number_print_checkbutton;
  Gtk::CheckButton* settings_number_view_checkbutton;
  Gtk::Entry* settings_maxperday_entry;
  Gtk::CheckButton* settings_maxperday_checkbutton;
  Gtk::Entry* settings_extracharge_entry;
  Gtk::CheckButton* settings_debug_checkbutton;
  // suggerimento clienti window
  Gtk::Window* suggerimento_clienti_window;
  Glib::RefPtr<Gtk::ListStore> clienti_suggerimento_liststore;
  Gtk::TreeView* clienti_suggerimento_treeview;
  Glib::RefPtr<Gtk::TreeSelection> clienti_suggerimento_treeselection;
  // security request password window
  Gtk::Dialog* request_password_dialog;
  Gtk::Entry* check_password_dialog_entry;
  Gtk::Button* password_ok_button;
  Gtk::Button* password_cancel_button;
};

#endif // _CONTROLLER_H_
