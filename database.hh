/*
 * database.hh
 * Copyright (C) 2017 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DATA_BASE_H_
#define _DATA_BASE_H_

#include <vector>
#include "db_engine.hh"
#include "cliente.hh"
#include "ricevuta.hh"
#include "preferenze.hh"

enum class SELECT { full, half };

typedef struct info_struct {
  Glib::ustring data;
  Glib::ustring indice;
} info_ordine;

class DataBase : public DBEngine {
public:
  DataBase(const Glib::ustring& db_path);
  // clienti
  std::vector<Cliente> get_clienti(void);
  std::vector<Glib::ustring> get_nomi_clienti(void);
  Cliente get_cliente(const Glib::ustring& name);
  void insert(const Cliente& cliente);
  void update(const Glib::ustring& name, const Cliente& cliente);
  void delete_cliente(const Glib::ustring& name);
  bool is_name_present(const Glib::ustring& name);
  // prestazioni
  std::vector<Prestazione> get_prestazioni(void);
  std::vector<Glib::ustring> get_descrizione_prestazioni_per_capo(const Glib::ustring& capo);
  std::vector<Glib::ustring> get_capo_prestazioni(void);
  void insert(const Prestazione& prestazione);
  void update(const Glib::ustring& capo, const Glib::ustring& descr, const Prestazione& prestazione);
  void delete_prestazione(const Glib::ustring& capo, const Glib::ustring& descr);
  bool is_prestazione_present(const Glib::ustring& capo, const Glib::ustring& descr);
  Glib::ustring get_prezzo_prestazione(const Glib::ustring& capo, const Glib::ustring& descr);
  // ricevute
  std::vector<Ricevuta> get_ricevute(const Glib::ustring& where = Glib::ustring(""), SELECT details = SELECT::full);
  inline std::vector<Ricevuta> get_ricevute_in_consegna_il(const CustomDate& date) { return get_ricevute(Glib::ustring::compose("where date(RICEVUTE.DATA_RIT, 'unixepoch') == date('%1','unixepoch') AND RICEVUTE.CONSEGNATO == 0", date.get_time_t())); }
  inline std::vector<Ricevuta> get_ricevute_per_resoconto(const CustomDate& date_from, const CustomDate& date_to) { return get_ricevute(Glib::ustring::compose("where (CONSEGNATO == 0 AND RICEVUTE.DATA_ACC >= '%1' AND RICEVUTE.DATA_ACC <= '%2') || (CONSEGNATO == 1 AND RICEVUTE.DATA_RIT >= '%1' AND RICEVUTE.DATA_RIT <= '%2')", date_from.get_time_t(), date_to.get_time_t())); }
  inline std::vector<Ricevuta> get_ricevute_per_eliminazione(const CustomDate& date_from) { return get_ricevute(Glib::ustring::compose("where CONSEGNATO == 1 AND RICEVUTE.DATA_RIT < '%1'", date_from.get_time_t()), SELECT::half); }
  std::vector<std::pair<task_info, Prestazione>> get_tasks_from(const CustomDate& cd);
  void insert(const Ricevuta& rcv);
  void update(time_t date, const Ricevuta& rcv);
  void update(time_t date, const Ricevuta& rcv, MEMB_FIELD field);
  void update_nome(const Glib::ustring& old_name, const Glib::ustring& new_name);
  void update_task_completed(time_t date, int index, const Glib::ustring& descr, bool flag);
  void update_receipt_completed(time_t date, bool flag);
  void delete_ricevuta(time_t date);
  bool is_ricevuta_present(time_t date);
  Ricevuta get_ricevuta(time_t date);
  CustomDate update_last_number_ricevuta(void);
  // entry-value string settings
  Glib::ustring get_setting(const Glib::ustring& entry);
  void insert(const Glib::ustring& entry, const Glib::ustring& value);
  void update(const Glib::ustring& entry, const Glib::ustring& value);
protected:
  std::vector<Ricevuta> select_ricevute(const Glib::ustring& where = Glib::ustring(""), SELECT details = SELECT::full);
  static int retrieve_string(void *data, int argc, char **argv, char **azColName);
  static int retrieve_str_setting(void *data, int argc, char **argv, char **azColName);
};

#endif // _DATA_BASE_H_

