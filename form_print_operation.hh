/*
 * form_print_operation.hh
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FORM_PRINT_OPERATION_HH_
#define _FORM_PRINT_OPERATION_HH_

#include <glibmm/ustring.h>
#include <gtkmm/printoperation.h>

class FormPrintOperation: public Gtk::PrintOperation {
public:
  static Glib::RefPtr<FormPrintOperation> create();
  inline virtual ~FormPrintOperation() {}
  inline void set_content(const Glib::ustring &str) { content.assign(str); }
protected:
  inline FormPrintOperation() {}
  virtual void on_begin_print(const Glib::RefPtr<Gtk::PrintContext>& context);
  virtual void on_draw_page(const Glib::RefPtr<Gtk::PrintContext>& context, int page_nr);
  Glib::RefPtr<Pango::Layout> m_refLayout;
  std::vector<int> m_PageBreaks; // line numbers where a page break occurs
private:
  Glib::ustring content;
};

#endif // _FORM_PRINT_OPERATION_HH_

