/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * custom_date.hh
 * Copyright (C) 2018 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CUSTOM_DATE_H_
#define _CUSTOM_DATE_H_

#include <iostream>
#include <iomanip>
#include <string>

#define ONE_DAY 86400

static const short int MONTH_DAYS[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
static const int HOLIDAYS_SIZE = 10;
static std::pair<short, short> HOLIDAYS[] = {{0, 1}, {0, 6}, {3, 25}, {4, 1}, {5, 2}, {7, 15}, {10, 1}, {11, 8}, {11, 25}, {11, 26}};

enum class TIME_PERIOD { null, daily, weekly, monthly, annually };
enum class DATE_FORMAT { xml, sql, human, label, print, date };

class CustomDate {
	friend std::ostream& operator<<(std::ostream& os, const CustomDate& cd);
 public:
	static void get_time_period_boundary(CustomDate* start, CustomDate* end, TIME_PERIOD period);
	static CustomDate parse_string(const char* cs, DATE_FORMAT format = DATE_FORMAT::date);
	static bool is_leap_year(int year);
	static CustomDate start_of_day(const CustomDate& cd);
	static CustomDate end_of_day(const CustomDate& cd);

	inline CustomDate(void) { unixtime = time(nullptr); }
	inline CustomDate(const time_t* time) { unixtime = *time; }
	inline CustomDate(const CustomDate& cd) { unixtime = cd.unixtime; }
	inline CustomDate(const char* str) { unixtime = atoll(str); }
	CustomDate(const int& dy, const int& mo, const int& yr, const int& hr, const int& mi, const int& se);
	CustomDate& operator=(const CustomDate &cd);
	inline bool operator<(const CustomDate &cd) const { return unixtime < cd.unixtime; }
	inline bool operator>(const CustomDate &cd) const { return unixtime > cd.unixtime; }
	inline bool operator==(const CustomDate &cd) const { return unixtime == cd.unixtime; }
	inline bool operator!=(const CustomDate &cd) const { return unixtime != cd.unixtime; }
	inline bool operator>=(const CustomDate &cd) const { return unixtime >= cd.unixtime; }
	inline bool operator<=(const CustomDate &cd) const { return unixtime <= cd.unixtime; }
	inline struct tm* get_struct_time(void) { return localtime(&unixtime); }
	void set_date_time(short int y, short int mn, short int d, short int h, short int m, short int s);
	void set_year(short int val);
	int get_year(void) const;
	void set_month(short int val);
	int get_month(void) const;
	int get_days_of_month(void) const;
	void set_day(int val);
	inline void add_days(short int val) { unixtime += (ONE_DAY * val); }
	int get_day(void) const;
	void set_hour(short int val);
	int get_hour(void) const;
	void set_minute(short int val);
	int get_minute(void) const;
	void set_second(short int val);
	int get_second(void) const;
	inline time_t get_time_t(void) const { return unixtime; }
	int get_weekday(void) const;
	bool is_holiday(void) const;
	std::string get_formatted_string(DATE_FORMAT format) const;
	bool is_same_day(const CustomDate& cd) const;
 protected:
	std::pair<short, short> get_easter_day(short int yr) const;
 private:
	time_t unixtime;
};

#endif // _CUSTOM_DATE_H_

