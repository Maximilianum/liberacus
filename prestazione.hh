/*
 * prestazione.hh
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
LiberAcus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LiberAcus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PRESTAZIONE_HH_
#define _PRESTAZIONE_HH_

#include <glibmm/ustring.h>

class Prestazione {
 public:
  static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
  static int retrieve_prestazione(void *data, int argc, char **argv, char **azColName);
  inline Prestazione() : capo (""), descrizione (""), prezzo (0.0), completata (false) {}
  inline Prestazione (const Glib::ustring &str1, const Glib::ustring &str2, float val) : capo (str1), descrizione (str2), prezzo (val), completata (false) {}
  Prestazione& operator=(const Prestazione& prst);
  bool operator<(const Prestazione& prst) const;
  bool operator==(const Prestazione& prst) const;
  inline void set_capo(const Glib::ustring& str) { capo.assign(str); }
  inline void set_capo(const char* ptr) { capo.assign(Glib::ustring(ptr)); }
  inline Glib::ustring get_capo(void) const { return capo; }
  inline void set_descrizione(const Glib::ustring& str) { descrizione.assign(str); }
  inline void set_descrizione(const char* ptr) { descrizione.assign(Glib::ustring(ptr)); }
  inline Glib::ustring get_descrizione(void) const { return descrizione; }
  inline void set_prezzo(float val) { prezzo = val; }
  inline void set_prezzo(const Glib::ustring& str) { prezzo = atof(str.c_str()); }
  inline void set_prezzo(const char* ptr) { prezzo = atof(ptr); }
  inline float get_prezzo(void) const { return prezzo; }
  inline void set_completata(bool flag) { completata = flag; }
  void set_completata(const Glib::ustring& str);
  inline void set_completata(const char* ptr) { set_completata(Glib::ustring(ptr)); }
  inline bool get_completata(void) const { return completata; }
private:
  static Glib::ustring sqlite_table;	 
  Glib::ustring capo;
  Glib::ustring descrizione;
  float prezzo;
  bool completata;
};

#endif // _PRESTAZIONE_HH_

