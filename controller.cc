/*
 * controller.cc
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
LiberAcus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LiberAcus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "controller.hh"
#include <vector>
#include "form_print_operation.hh"

/*
 * STATIC FUNCTIONS
 */

void Controller::calculate_pickup_date(CustomDate* tempo) {
  tempo->add_days(3);
  while (tempo->is_holiday()) {
    tempo->add_days(1);
  }
  if (tempo->get_weekday() == 6) { // saturday
    tempo->set_hour(12);
    tempo->set_minute(0);
    tempo->set_second(0);
  } else {
    tempo->set_hour(17);
    tempo->set_minute(30);
    tempo->set_second(0);
  }
}

Controller::Controller(Glib::RefPtr<Gtk::Builder> bldr, Glib::RefPtr<Gtk::Application> app, const Glib::ustring& main_path) : CtrlAppEngine(app){
  CtrlAppEngine::app_filename = APP_FILE_NAME;
  CtrlAppEngine::app_name = APP_NAME;
  CtrlAppEngine::version = VERSION;
  CtrlAppEngine::build_number = BUILD_NUMBER;
  CtrlAppEngine::copyright = COPYRIGHT;
  CtrlAppEngine::project_website = APP_WEBSITE;
  // working directory
  debug_log_path = main_path + Glib::ustring("/liberacus_debug.log");
  pref_path = main_path + Glib::ustring ( "/preferences.xml" );
  page_setup_path = main_path + Glib::ustring ( "/printer.page_setup" );
  database = new DataBase(main_path + Glib::ustring("/liberacus.db"));
  preferenze.set_database(database);
  // main window
  bldr->get_widget("main_window", main_win);
  bldr->get_widget ("main_notebook", main_notebook);
  bldr->get_widget ("main_statusbar", main_statusbar);
  stbrItemDiskId = main_statusbar->get_context_id(Glib::ustring("ITEM_DISK"));
  stbrItemSelId = main_statusbar->get_context_id(Glib::ustring("ITEM_SELECT"));
  main_notebook_page = 0;
  // main toolbar
  bldr->get_widget("main_toolbar", main_toolbar);
  bldr->get_widget("nuova_ricevuta_button", nuova_ricevuta_button);
  bldr->get_widget("nuova_prestazione_button", nuova_prestazione_button);
  bldr->get_widget("nuovo_cliente_button", nuovo_cliente_button);
  bldr->get_widget("modifica_selezione_toolbutton", modifica_selezione_toolbutton);
  bldr->get_widget("elimina_selezione_toolbutton", elimina_selezione_toolbutton);
  bldr->get_widget("chiusura_cassa_toolbutton", stampa_chiusura_toolbutton);
  // main menu
  bldr->get_widget ("main_menubar", main_menubar);
  bldr->get_widget ("nuova_ricevuta_menuitem", nuova_ricevuta_menuitem);
  bldr->get_widget ("nuova_prestazione_menuitem", nuova_prestazione_menuitem);
  bldr->get_widget ("nuovo_cliente_menuitem", nuovo_cliente_menuitem);
  bldr->get_widget ("imposta_pagina_menuitem", imposta_pagina_menuitem);
  bldr->get_widget ("print_ricevuta_menuitem", stampa_ricevuta_menuitem);
  bldr->get_widget ("print_etichette_menuitem", stampa_etichette_menuitem);
  bldr->get_widget ("print_chiusura_menuitem", stampa_chiusura_menuitem);
  bldr->get_widget ("quit_menuitem", quit_menuitem);
  bldr->get_widget ("modifica_selezione_menuitem", modifica_selezione_menuitem);
  bldr->get_widget ("elimina_selezione_menuitem", elimina_selezione_menuitem);
  bldr->get_widget ("preferenze_menuitem", preferenze_menuitem);
  bldr->get_widget ("calendario_menuitem", settimana_menuitem);
  bldr->get_widget ("ricevute_menuitem", ricevute_menuitem);
  bldr->get_widget ("prestazioni_menuitem", prestazioni_menuitem);
  bldr->get_widget ("clienti_menuitem", clienti_menuitem);
  bldr->get_widget ("resoconto_menuitem", resoconto_menuitem);
  bldr->get_widget ("about_menuitem", about_menuitem);
  /**
   * main notebook
   * */
  // page summary
  summary_treestore = Gtk::TreeStore::create(summary_tree_mod_col_record);
  bldr->get_widget ("summary_treeview", summary_treeview);
  summary_treeview->set_model(summary_treestore);
  summary_treeview->append_column_numeric(Glib::ustring("Totale"), summary_tree_mod_col_record.price, "%.2f €");
  summary_price_treeviewcolumn = summary_treeview->get_column(4);
  summary_price_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  summary_price_cellrenderertext = (Gtk::CellRendererText*)summary_price_treeviewcolumn->get_first_cell();
  summary_treeview->append_column_editable(Glib::ustring("Completata"), summary_tree_mod_col_record.completed);
  summary_completed_treeviewcolumn = summary_treeview->get_column(5);
  summary_completed_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  summary_completed_cellrenderertoggle = (Gtk::CellRendererToggle*)summary_completed_treeviewcolumn->get_first_cell();
  summary_treeview->append_column(Glib::ustring(), summary_tree_mod_col_record.empty);
  summary_treeselection = summary_treeview->get_selection();
  summary_treeselection->set_mode(Gtk::SELECTION_SINGLE);
  // page receipts
  receipts_liststore = Gtk::ListStore::create(receipt_tree_mod_col_record);
  bldr->get_widget("receipts_treeview", receipts_treeview);
  receipts_treeview->set_model(receipts_liststore);
  receipt_number_treeviewcolumn = receipts_treeview->get_column(0);
  receipts_treeview->append_column_numeric(Glib::ustring("Totale"), receipt_tree_mod_col_record.total,"%.2f €");
  receipt_total_treeviewcolumn = receipts_treeview->get_column(3);
  receipt_total_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  receipts_treeview->append_column_numeric(Glib::ustring("Acconto"), receipt_tree_mod_col_record.deposit,"%.2f €");
  receipt_deposit_treeviewcolumn = receipts_treeview->get_column(4);
  receipt_deposit_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  receipts_treeview->append_column(Glib::ustring("Consegna"), receipt_tree_mod_col_record.date_deliver);
  receipt_deliver_treeviewcolumn = receipts_treeview->get_column(5);
  receipt_deliver_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  receipts_treeview->append_column_editable(Glib::ustring("Urgente"), receipt_tree_mod_col_record.urgent);
  receipt_urgent_treeviewcolumn = receipts_treeview->get_column(6);
  receipt_urgent_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  receipt_urgent_cellrenderertoggle = (Gtk::CellRendererToggle*)receipt_urgent_treeviewcolumn->get_first_cell();
  receipts_treeview->append_column_editable(Glib::ustring("Consegnato"), receipt_tree_mod_col_record.delivered);
  receipt_delivered_treeviewcolumn = receipts_treeview->get_column(7);
  receipt_delivered_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  receipt_delivered_cellrenderertoggle = (Gtk::CellRendererToggle*)receipt_delivered_treeviewcolumn->get_first_cell();
  receipts_treeview->append_column(Glib::ustring(), receipt_tree_mod_col_record.empty);
  receipts_treeselection = receipts_treeview->get_selection();
  receipts_treeselection->set_mode(Gtk::SELECTION_SINGLE);	
  // receipts contextual menu
  bldr->get_widget("ricevute_context_menu", ricevute_context_menu);
  bldr->get_widget("modifica_ricevuta_context_menuitem", modifica_ricevute_context_menuitem);
  bldr->get_widget("elimina_ricevuta_context_menuitem", elimina_ricevute_context_menuitem);
  bldr->get_widget("stampa_ricevuta_context_menuitem", stampa_ricevute_context_menuitem);
  bldr->get_widget("stampa_etichetta_context_menuitem", stampa_etichette_context_menuitem);
  // page tasks
  tasks_liststore = Gtk::ListStore::create(task_tree_mod_col_record);
  bldr->get_widget("tasks_treeview", tasks_treeview);
  tasks_treeview->set_model(tasks_liststore);
  tasks_treeview->append_column_numeric(Glib::ustring("Prezzo"), task_tree_mod_col_record.price,"%.2f €");
  tasks_price_treeviewcolumn = tasks_treeview->get_column(2);
  tasks_price_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  prestazioni_treeselection = tasks_treeview->get_selection();
  prestazioni_treeselection->set_mode(Gtk::SELECTION_SINGLE);
  // page clienti
  clienti_liststore = Gtk::ListStore::create(customer_tree_mod_col_record);
  bldr->get_widget("clienti_treeview", clienti_treeview);
  clienti_treeview->set_model(clienti_liststore);
  clienti_treeselection = clienti_treeview->get_selection();
  clienti_treeselection->set_mode(Gtk::SELECTION_SINGLE);
  // menu contestuale generico
  bldr->get_widget("generic_context_menu", generic_context_menu);
  bldr->get_widget("edit_generic_context_menuitem", edit_generic_context_menuitem);
  bldr->get_widget("remove_generic_context_menuitem", remove_generic_context_menuitem);
  // page resoconto
  selezione_periodo_liststore = Gtk::ListStore::create(combobox_tmcr);
  bldr->get_widget("periodo_resoconto_combobox", selezione_periodo_combobox);
  selezione_periodo_combobox->set_model(selezione_periodo_liststore);
  std::vector<Glib::ustring> periodi_vec { "oggi", "questa settimana", "questo mese" };
  load_data_combobox(periodi_vec.begin(), periodi_vec.end(), selezione_periodo_liststore);
  selezione_periodo_combobox->set_active(0);
  bldr->get_widget("ricevute_emesse_label2", resoconto_ricevute_emesse_label);
  bldr->get_widget("lavori_consegnati_label2", resoconto_lavori_consegnati_label);
  bldr->get_widget("clienti_serviti_label2", resoconto_clienti_serviti_label);
  bldr->get_widget("incasso_totale_label2", resoconto_incasso_totale_label);
  ricevute_resoconto_liststore = Gtk::ListStore::create(report_receipt_tree_mod_col_record);
  bldr->get_widget("ricevute_resoconto_treeview", ricevute_resoconto_treeview);
  ricevute_resoconto_treeview->set_model(ricevute_resoconto_liststore);
  numero_ricevuta_resoconto_treeviewcolumn = ricevute_resoconto_treeview->get_column(0);
  prestazioni_resoconto_liststore = Gtk::ListStore::create(receipt_task_tree_mod_col_record);
  bldr->get_widget("prestazioni_resoconto_treeview", prestazioni_resoconto_treeview);
  prestazioni_resoconto_treeview->set_model(prestazioni_resoconto_liststore);
  clienti_resoconto_liststore = Gtk::ListStore::create(customer_tree_mod_col_record);
  bldr->get_widget("clienti_resoconto_treeview", clienti_resoconto_treeview);
  clienti_resoconto_treeview->set_model(clienti_resoconto_liststore);
  // nuova ricevuta window
  bldr->get_widget("new_ricevuta_window", new_ricevuta_window);
  bldr->get_widget("cliente_ricevuta_entry", cliente_ricevuta_entry);
  nome_cliente_entrycompletion = cliente_ricevuta_entry->get_completion();
  nome_cliente_liststore = Gtk::ListStore::create(combobox_tmcr);
  nome_cliente_entrycompletion->set_model(nome_cliente_liststore);
  bldr->get_widget("suggerimento_cliente_button", trova_clienti_button);
  bldr->get_widget("data_accettazione_ricevuta_label", data_accettazione_ricevuta_label);
  bldr->get_widget("cambia_data_ricevuta_button1", cambia_data_ricevuta_button1);
  prestazioni_ricevuta_treestore = Gtk::TreeStore::create(receipt_task_tree_mod_col_record);
  bldr->get_widget("capi_prestazioni_ricevuta_treeview", prestazioni_ricevuta_treeview);
  prestazioni_ricevuta_treeview->set_model(prestazioni_ricevuta_treestore);
  prestazioni_ricevuta_treeview->append_column_editable(Glib::ustring("Descrizione"), receipt_task_tree_mod_col_record.description);
  descrizione_prestazione_treeviewcolumn = prestazioni_ricevuta_treeview->get_column(2);
  descrizione_prestazione_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  descrizione_prestazione_cellrenderertext = (Gtk::CellRendererText*)descrizione_prestazione_treeviewcolumn->get_first_cell();
  prestazioni_ricevuta_treeview->append_column_numeric_editable(Glib::ustring("Prezzo"), receipt_task_tree_mod_col_record.price, "%.2f €");
  prezzo_prestazione_treeviewcolumn = prestazioni_ricevuta_treeview->get_column(3);
  prezzo_prestazione_treeviewcolumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  prezzo_prestazione_cellrenderertext = (Gtk::CellRendererText*)prezzo_prestazione_treeviewcolumn->get_first_cell();
  prestazioni_ricevuta_treeselection = prestazioni_ricevuta_treeview->get_selection();
  prestazioni_ricevuta_treeselection->set_mode(Gtk::SELECTION_SINGLE);	
  bldr->get_widget("selezione_capo_combobox", selezione_capo_combobox);
  selezione_capo_liststore = Gtk::ListStore::create(combobox_tmcr);
  selezione_capo_combobox->set_model(selezione_capo_liststore);
  bldr->get_widget("selezione_prestazione_combobox", selezione_prestazione_combobox);
  selezione_prestazione_liststore = Gtk::ListStore::create(combobox_tmcr);
  selezione_prestazione_combobox->set_model(selezione_prestazione_liststore);
  bldr->get_widget("add_prestazione_ricevuta_button", add_prestazione_ricevuta_button);
  bldr->get_widget("rem_prestazione_ricevuta_button", rem_prestazione_ricevuta_button);
  bldr->get_widget("data_ritiro_ricevuta_label", data_ritiro_ricevuta_label);
  bldr->get_widget("cambia_data_ricevuta_button2", cambia_data_ricevuta_button2);
  bldr->get_widget("totale_ricevuta_label2", totale_ricevuta_label);
  bldr->get_widget("new_ricevuta_nonpagato_radiobutton", nonpagato_radiobutton);
  bldr->get_widget("new_ricevuta_pagato_radiobutton", pagato_radiobutton);
  bldr->get_widget("acconto_ricevuta_entry", acconto_ricevuta_entry);
  bldr->get_widget("stampa_ricevuta_checkbutton", stampa_ricevuta_checkbutton);
  bldr->get_widget("urgente_ricevuta_checkbutton", urgente_ricevuta_checkbutton);
  bldr->get_widget("annulla_ricevuta_button", new_ricevuta_cancel_button);
  bldr->get_widget("conferma_ricevuta_button", new_ricevuta_confirm_button);
  // nuova prestazione window
  bldr->get_widget("new_prestazione_window", new_prestazione_window);
  bldr->get_widget("capo_prestazione_entry", capo_prestazione_entry);
  capo_entrycompletion = capo_prestazione_entry->get_completion ();
  capo_liststore = Gtk::ListStore::create(combobox_tmcr);
  capo_entrycompletion->set_model (capo_liststore);
  bldr->get_widget("descrizione_prestazione_entry", descrizione_prestazione_entry);
  bldr->get_widget("prezzo_prestazione_entry", prezzo_prestazione_entry);
  bldr->get_widget("new_prestazione_confirm_button", new_prestazione_confirm_button);
  bldr->get_widget("new_prestazione_cancel_button", new_prestazione_cancel_button);
  // nuovo cliente window
  bldr->get_widget("new_cliente_window", new_cliente_window);
  bldr->get_widget("nome_cliente_entry", nome_cliente_entry);
  bldr->get_widget("telefono_cliente_entry", telefono_cliente_entry);
  bldr->get_widget("new_cliente_confirm_button", new_cliente_confirm_button);
  bldr->get_widget("new_cliente_cancel_button", new_cliente_cancel_button);
  // calendario window
  bldr->get_widget("calendario_window", calendario_window);
  bldr->get_widget("calendario", main_calendar);
  bldr->get_widget("calendario_ora_combobox", calendar_hour_combobox);
  calendar_hour_liststore = Gtk::ListStore::create(combobox_tmcr);
  calendar_hour_combobox->set_model(calendar_hour_liststore);
  std::vector<Glib::ustring> hour {};
  for (short i = 7; i < 21; i++) {
    hour.push_back(Glib::ustring::format(std::setfill(L'0'), std::setw(2), i));
  }
  load_data_combobox(hour.begin(), hour.end(), calendar_hour_liststore);
  calendar_hour_combobox->set_active(0);
  bldr->get_widget("calendario_minuti_combobox", calendar_minute_combobox);
  calendar_minute_liststore = Gtk::ListStore::create(combobox_tmcr);
  calendar_minute_combobox->set_model(calendar_minute_liststore);
  std::vector<Glib::ustring> minute {};
  for (short i = 0; i < 60; i += 5) {
    minute.push_back(Glib::ustring::format(std::setfill(L'0'), std::setw(2), i));
  }
  load_data_combobox(minute.begin(), minute.end(), calendar_minute_liststore);
  calendar_minute_combobox->set_active(0);
  bldr->get_widget("calendario_cancel_button", calendario_cancel_button);
  bldr->get_widget("calendario_confirm_button", calendario_confirm_button);
  // preferenze window
  bldr->get_widget("settings_window", settings_window);
  bldr->get_widget("settings_password_entry", settings_password_entry);
  bldr->get_widget("settings_password_checkbutton", settings_password_checkbutton);
  bldr->get_widget("settings_header_textview", settings_header_textview);
  settings_header_textbuffer = settings_header_textview->get_buffer ();
  bldr->get_widget("settings_greeting_textview", settings_greeting_textview);
  settings_greeting_textbuffer = settings_greeting_textview->get_buffer ();
  bldr->get_widget("settings_save_button", settings_save_button);
  bldr->get_widget("settings_cancel_button", settings_cancel_button);
  bldr->get_widget("settings_delete_combobox", settings_delete_combobox);
  settings_delete_liststore = Gtk::ListStore::create(combobox_tmcr);
  settings_delete_combobox->set_model (settings_delete_liststore);
  std::vector<Glib::ustring> frequenza_vec { "mai", "giornaliero", "settimanale", "mensile", "annuale" };
  load_data_combobox(frequenza_vec.begin(), frequenza_vec.end(), settings_delete_liststore);
  settings_delete_combobox->set_active(0);
  bldr->get_widget("settings_number_reset_combobox", settings_number_reset_combobox);
  settings_number_reset_liststore = Gtk::ListStore::create(combobox_tmcr);
  settings_number_reset_combobox->set_model(settings_number_reset_liststore);
  load_data_combobox(frequenza_vec.begin() + 1, frequenza_vec.end(), settings_number_reset_liststore);
  settings_number_reset_combobox->set_active(0);
  bldr->get_widget("settings_number_print_checkbutton", settings_number_print_checkbutton);
  bldr->get_widget("settings_number_view_checkbutton", settings_number_view_checkbutton);
  bldr->get_widget("settings_maxperday_entry", settings_maxperday_entry);
  bldr->get_widget("settings_maxperday_checkbutton", settings_maxperday_checkbutton);
  bldr->get_widget("settings_extracharge_entry", settings_extracharge_entry);
  bldr->get_widget("settings_debug_checkbutton", settings_debug_checkbutton);
  // suggerimento clienti window
  bldr->get_widget("suggerimento_clienti_window", suggerimento_clienti_window);
  clienti_suggerimento_liststore = Gtk::ListStore::create(customer_tree_mod_col_record);
  bldr->get_widget("suggerimento_clienti_treeview", clienti_suggerimento_treeview);
  clienti_suggerimento_treeview->set_model(clienti_suggerimento_liststore);
  clienti_suggerimento_treeselection = clienti_suggerimento_treeview->get_selection();
  // printing
  m_ref_pagesetup = Gtk::PageSetup::create();
  m_ref_printsettings = Gtk::PrintSettings::create();
  // security
  bldr->get_widget("request_password_dialog", request_password_dialog);
  bldr->get_widget("check_password_dialog_entry", check_password_dialog_entry);
  bldr->get_widget("request_password_dialog_ok_button", password_ok_button);
  bldr->get_widget("request_password_dialog_cancel_button", password_cancel_button);
  connect_signals();
}

Controller::~Controller() {
  delete database;
}

/*
 * MEMBER FUNCTIONS
 */

void Controller::connect_signals(void)
{
  // main menu
  nuova_ricevuta_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_new_ricevuta_action));
  nuova_prestazione_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_new_prestazione_action));
  nuovo_cliente_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_new_cliente_action));
  imposta_pagina_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_page_setup_action));
  stampa_ricevuta_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_print_ricevuta_action));
  stampa_etichette_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_print_etichette_action));
  stampa_chiusura_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_stampa_chiusura_cassa_action));
  quit_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::quit));
  modifica_selezione_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_edit_item_action));
  elimina_selezione_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_remove_item_action));
  preferenze_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_preferenze_action));
  settimana_menuitem->signal_activate().connect(sigc::bind<const unsigned short>(sigc::mem_fun(this, &Controller::on_visualizza_action), PAGE_SUMMARY));
  ricevute_menuitem->signal_activate().connect(sigc::bind<const unsigned short>(sigc::mem_fun(this, &Controller::on_visualizza_action), PAGE_RECEIPT));
  prestazioni_menuitem->signal_activate().connect(sigc::bind<const unsigned short>(sigc::mem_fun(this, &Controller::on_visualizza_action), PAGE_TASKS));
  clienti_menuitem->signal_activate().connect(sigc::bind<const unsigned short>(sigc::mem_fun(this, &Controller::on_visualizza_action), PAGE_CUSTOMERS));
  resoconto_menuitem->signal_activate().connect(sigc::bind<const unsigned short>(sigc::mem_fun (this, &Controller::on_visualizza_action), PAGINA_RESOCONTO));
  about_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::about));
  // main toolbar
  nuova_ricevuta_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_new_ricevuta_action));
  nuova_prestazione_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_new_prestazione_action));
  nuovo_cliente_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_new_cliente_action));
  modifica_selezione_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_edit_item_action));
  elimina_selezione_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_remove_item_action));
  stampa_chiusura_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_stampa_chiusura_cassa_action));
  // main window
  main_win->signal_focus_in_event().connect(sigc::mem_fun(this, &Controller::on_main_window_focus_in));
  main_win->signal_focus_out_event().connect(sigc::mem_fun(this, &Controller::on_main_window_focus_out));
  main_notebook->signal_switch_page().connect(sigc::mem_fun(this, &Controller::on_main_notebook_page_switch));
  /**
   * main notebook
   * */
  // page summary
  summary_completed_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(this, &Controller::on_task_completed_toggled));
  // page ricevute
  receipts_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(this, &Controller::on_treeview_clicked));
  receipts_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_items_treeselection_changed));
  receipt_urgent_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(this, &Controller::on_ricevuta_urgente_toggled));
  receipt_delivered_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(this, &Controller::on_ricevuta_consegnato_toggled));
  // menu contestuale ricevute
  modifica_ricevute_context_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_edit_item_action));
  elimina_ricevute_context_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_remove_item_action));
  stampa_ricevute_context_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_print_ricevuta_action));
  stampa_etichette_context_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_print_etichette_action));
  // page prestazioni
  tasks_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(this, &Controller::on_treeview_clicked));
  prestazioni_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_items_treeselection_changed));
  // page clienti
  clienti_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(this, &Controller::on_treeview_clicked));
  clienti_treeselection->signal_changed().connect(sigc::mem_fun(this, &Controller::on_items_treeselection_changed));
  // menu contestuale generico
  edit_generic_context_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_edit_item_action));
  remove_generic_context_menuitem->signal_activate().connect(sigc::mem_fun(this, &Controller::on_remove_item_action));
  // page resoconto
  selezione_periodo_combobox->signal_changed().connect(sigc::mem_fun(this, &Controller::aggiorna_pagina_resoconto));
  // new ricevuta window
  trova_clienti_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_trova_clienti_action));
  cambia_data_ricevuta_button1->signal_clicked().connect(sigc::bind<Gtk::Button*>(sigc::mem_fun(this, &Controller::on_cambia_data_ricevuta_action), cambia_data_ricevuta_button1));
  cambia_data_ricevuta_button2->signal_clicked().connect(sigc::bind<Gtk::Button*>(sigc::mem_fun(this, &Controller::on_cambia_data_ricevuta_action), cambia_data_ricevuta_button2));
  connect_prestazioni_ricevuta();
  connect_capo_popup();
  connect_prestazione_popup();
  prestazioni_ricevuta_treeview->signal_row_expanded ().connect(sigc::mem_fun (this, &Controller::on_prestazioni_ricevuta_row_expanded));
  add_prestazione_ricevuta_button->signal_clicked ().connect ( sigc::mem_fun (this, &Controller::on_add_prestazione_ricevuta_action));
  rem_prestazione_ricevuta_button->signal_clicked ().connect ( sigc::mem_fun (this, &Controller::on_remove_prestazione_ricevuta_action));
  descrizione_prestazione_cellrenderertext->signal_edited().connect(sigc::mem_fun(this, &Controller::on_descrizione_modificata));
  prezzo_prestazione_cellrenderertext->signal_edited().connect(sigc::mem_fun(this, &Controller::on_prezzo_modificato));
  nonpagato_radiobutton->signal_toggled ().connect ( sigc::mem_fun (this, &Controller::on_pagamento_anticipato_toggled));
  pagato_radiobutton->signal_toggled ().connect ( sigc::mem_fun (this, &Controller::on_pagamento_anticipato_toggled));
  urgente_ricevuta_checkbutton->signal_toggled().connect(sigc::mem_fun(this, &Controller::on_urgente_toggled));
  new_ricevuta_cancel_button->signal_clicked ().connect ( sigc::bind<Gtk::Window*>( sigc::mem_fun (this, &Controller::on_cancel_button_clicked), new_ricevuta_window));
  new_ricevuta_confirm_button->signal_clicked ().connect ( sigc::mem_fun (this, &Controller::on_conferma_ricevuta_button_action));
  // new prestazione window
  new_prestazione_confirm_button->signal_clicked ().connect ( sigc::mem_fun (this, &Controller::on_new_prestazione_confirm_button_clicked) );
  new_prestazione_cancel_button->signal_clicked ().connect ( sigc::bind<Gtk::Window*>( sigc::mem_fun (this, &Controller::on_cancel_button_clicked), new_prestazione_window ) );
  // new cliente window
  new_cliente_confirm_button->signal_clicked ().connect ( sigc::mem_fun (this, &Controller::on_new_cliente_confirm_button_clicked) );
  new_cliente_cancel_button->signal_clicked ().connect ( sigc::bind<Gtk::Window*>( sigc::mem_fun (this, &Controller::on_cancel_button_clicked), new_cliente_window ) );	
  // calendario window
  calendario_confirm_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_calendario_confirm_button_clicked));
  calendario_cancel_button->signal_clicked().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &Controller::on_cancel_button_clicked), calendario_window));
  // preferenze window
  settings_number_view_checkbutton->signal_toggled().connect(sigc::mem_fun(this, &Controller::on_visualizza_numero_toggled));
  settings_cancel_button->signal_clicked().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &Controller::on_cancel_button_clicked), settings_window));
  settings_save_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_salva_preferenze_action));
  // suggerimento clienti window
  clienti_suggerimento_treeview->signal_button_press_event ().connect_notify (sigc::mem_fun (this, &Controller::on_suggerimento_clienti_double_clicked) );
  // security request password
  check_password_dialog_entry->signal_activate().connect(sigc::mem_fun(this, &Controller::on_request_password_ok));
  password_ok_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_request_password_ok));
  password_cancel_button->signal_clicked().connect(sigc::mem_fun(this, &Controller::on_request_password_cancel));
}

bool Controller::on_main_window_focus_in(GdkEventFocus *event) {
  if (app_status == APP_STATUS::starting) {
    set_up_printer();
    Ricevuta::set_sovraprezzo_urgenza(preferenze.get_extra_charge());
    load_clienti_in_treeview();
    load_customers_in_treeview(nome_cliente_liststore);
    load_prestazioni_in_treeview();
    update_mappa_capi();
    load_capo_popup_button();
    load_capi_in_prestazione_completion();
    eliminazione_automatica();
    load_ricevute_in_treeview();
    try {
      CustomDate tempo = database->update_last_number_ricevuta();
      CustomDate inizio, fine;
      CustomDate::get_time_period_boundary(&inizio, &fine, preferenze.get_reset_number_period());
      if (tempo < inizio) {
	Ricevuta::set_numero_ricevute_emesse(0);
      }
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::on_main_window_focus_in() " << dbe.get_message() << std::endl;
    }
    app_status = APP_STATUS::running;
    load_tasks_in_summary_treeview();
  }	
  main_menubar->set_sensitive(true);
  main_toolbar->set_sensitive(true);
  receipts_treeview->set_sensitive(true);
  tasks_treeview->set_sensitive(true);
  clienti_treeview->set_sensitive(true);
  update_visualizza_numero(preferenze.get_view_number());
  return false;
}

bool Controller::on_main_window_focus_out(GdkEventFocus *event) {
  main_menubar->set_sensitive(false);
  main_toolbar->set_sensitive(false);
  switch(main_notebook_page) {
  case PAGE_RECEIPT:
    receipts_treeview->set_sensitive(false);
    break;
  case PAGE_TASKS:
    tasks_treeview->set_sensitive(false);
    break;
  case PAGE_CUSTOMERS:
    clienti_treeview->set_sensitive(false);
    break;
  }
  return false;
}

void Controller::on_main_notebook_page_switch(Gtk::Widget *page, guint page_num) {
  main_notebook_page = page_num;
  switch (page_num) {
  case PAGE_SUMMARY:
    load_tasks_in_summary_treeview();
    break;
  }
  on_items_treeselection_changed();
}

void Controller::on_items_treeselection_changed(void) {
  Glib::ustring messaggio;
  int nr = 0;
  switch (main_notebook_page)	{
  case PAGE_SUMMARY:
    settimana_menuitem->set_sensitive(false);
    ricevute_menuitem->set_sensitive(true);
    prestazioni_menuitem->set_sensitive(true);
    clienti_menuitem->set_sensitive(true);
    resoconto_menuitem->set_sensitive(true);
    stampa_ricevuta_menuitem->set_sensitive(false);
    stampa_etichette_menuitem->set_sensitive(false);
    break;
  case PAGE_RECEIPT:
    settimana_menuitem->set_sensitive(true);
    ricevute_menuitem->set_sensitive(false);
    prestazioni_menuitem->set_sensitive(true);
    clienti_menuitem->set_sensitive(true);
    resoconto_menuitem->set_sensitive(true);
    nr = receipts_treeselection->count_selected_rows();
    if (nr == 0) {
      messaggio.append(Glib::ustring("Nessuna ricevuta selezionata."));
    } else if (nr == 1) {
      messaggio.append(Glib::ustring("Una ricevuta selezionata."));
    } else if (nr > 1) {
      messaggio.append(Glib::ustring::compose("%1 ricevute selezionate.", Glib::ustring::format(nr)));
    }
    if (nr > 0) {
      stampa_ricevuta_menuitem->set_sensitive(true);
      stampa_etichette_menuitem->set_sensitive(true);
    } else {
      stampa_ricevuta_menuitem->set_sensitive(false);
      stampa_etichette_menuitem->set_sensitive(false);
    }
    break;
  case PAGE_TASKS:
    settimana_menuitem->set_sensitive(true);
    ricevute_menuitem->set_sensitive(true);
    prestazioni_menuitem->set_sensitive(false);
    clienti_menuitem->set_sensitive(true);
    resoconto_menuitem->set_sensitive(true);
    stampa_ricevuta_menuitem->set_sensitive(false);
    stampa_etichette_menuitem->set_sensitive(false);
    nr = prestazioni_treeselection->count_selected_rows();
    if (nr == 0) {
      messaggio.append(Glib::ustring("Nessuna prestazione selezionata."));
    } else if (nr == 1) {
      messaggio.append(Glib::ustring("Una prestazione selezionata."));
    } else if (nr > 1) {
      messaggio.append(Glib::ustring::compose("%1 prestazioni selezionate.", Glib::ustring::format(nr)));
    }
    break;
  case PAGE_CUSTOMERS:
    settimana_menuitem->set_sensitive(true);
    ricevute_menuitem->set_sensitive(true);
    prestazioni_menuitem->set_sensitive(true);
    clienti_menuitem->set_sensitive(false);
    resoconto_menuitem->set_sensitive(true);
    stampa_ricevuta_menuitem->set_sensitive(false);
    stampa_etichette_menuitem->set_sensitive(false);
    nr = clienti_treeselection->count_selected_rows();
    if (nr == 0) {
      messaggio.append(Glib::ustring("Nessun cliente selezionato."));
    } else if (nr == 1) {
      messaggio.append(Glib::ustring("Un cliente selezionato."));
    } else if (nr > 1) {
      messaggio.append(Glib::ustring::compose("%1 clienti selezionati.", Glib::ustring::format(nr)));
    }
    break;
  case PAGINA_RESOCONTO:
    settimana_menuitem->set_sensitive(true);
    ricevute_menuitem->set_sensitive(true);
    prestazioni_menuitem->set_sensitive(true);
    clienti_menuitem->set_sensitive(true);
    resoconto_menuitem->set_sensitive(false);
    stampa_ricevuta_menuitem->set_sensitive(false);
    stampa_etichette_menuitem->set_sensitive(false);
    aggiorna_pagina_resoconto();
    break;
  }
  main_statusbar->pop(stbrItemSelId);
  main_statusbar->push(messaggio, stbrItemSelId);
  if (nr > 0)	{
    elimina_selezione_toolbutton->set_sensitive(true);
    elimina_selezione_menuitem->set_sensitive(true);
    modifica_selezione_toolbutton->set_sensitive(true);
    modifica_selezione_menuitem->set_sensitive(true);
  } else {
    elimina_selezione_toolbutton->set_sensitive(false);
    elimina_selezione_menuitem->set_sensitive(false);
    modifica_selezione_toolbutton->set_sensitive(false);
    modifica_selezione_menuitem->set_sensitive(false);
  }
}

void Controller::load_customers_in_treeview(Glib::RefPtr<Gtk::ListStore> liststore) {
  liststore->clear();
  try {
    std::vector<Glib::ustring> nomi { database->get_nomi_clienti() };
    std::vector<Glib::ustring>::iterator iter = nomi.begin();
    while (iter != nomi.end())	{
      Gtk::TreeModel::iterator iterTree { liststore->append() };
      Gtk::TreeModel::Row row { *iterTree };
      row[combobox_tmcr.column] = *iter;
      iter++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::load_customers_in_treeview() " << dbe.get_message() << std::endl;
  }
}

void Controller::on_new_ricevuta_action(void) {
  CustomDate cd {};
  main_notebook->set_current_page(PAGE_RECEIPT);
  receipts_treeselection->unselect_all();
  new_ricevuta_window->set_title("Nuova Ricevuta");
  cliente_ricevuta_entry->set_text("");
  urgente_ricevuta_checkbutton->set_active(false);
  data_accettazione_ricevuta_label->set_text(Glib::ustring(cd.get_formatted_string(DATE_FORMAT::human)));
  calculate_pickup_date(&cd);
  data_ritiro_ricevuta_label->set_text(Glib::ustring(cd.get_formatted_string(DATE_FORMAT::human)));
  prestazioni_ricevuta_treestore->clear();
  capo_popup_connection.disconnect();
  prestazione_popup_connection.disconnect();
  selezione_capo_combobox->set_active(-1);
  selezione_prestazione_combobox->set_active(-1);
  capo_popup_connection = selezione_capo_combobox->signal_changed().connect(sigc::mem_fun(this, &Controller::load_prestazione_popup_button));
  prestazione_popup_connection = selezione_prestazione_combobox->signal_changed().connect(sigc::mem_fun(this, &Controller::on_selezione_prestazione_popup_changed));
  totale_ricevuta_label->set_text("0,0");
  acconto_ricevuta_entry->set_text("0,0");
  acconto_ricevuta_entry->set_sensitive(true);
  nonpagato_radiobutton->set_active(true);
  stampa_ricevuta_checkbutton->set_active(true);
  new_ricevuta_window->present();
  cliente_ricevuta_entry->grab_focus();
}

void Controller::on_new_prestazione_action(void) {
  main_notebook->set_current_page (PAGE_TASKS);
  prestazioni_treeselection->unselect_all();
  capo_prestazione_entry->set_text("");
  descrizione_prestazione_entry->set_text("");
  prezzo_prestazione_entry->set_text("");
  new_prestazione_window->present();
  capo_prestazione_entry->grab_focus();
}

void Controller::on_new_cliente_action(void) {
  main_notebook->set_current_page(PAGE_CUSTOMERS);
  clienti_treeselection->unselect_all();
  nome_cliente_entry->set_text("");
  telefono_cliente_entry->set_text("");
  new_cliente_window->present();
  nome_cliente_entry->grab_focus();
}

void Controller::on_edit_item_action(void) {
  if (preferenze.get_enable_password()) {
    if (request_password() == Gtk::RESPONSE_OK) {
      if (check_password()) {
	edit_selected_item();
      } else {
	on_password_error();
      }
    }
  } else {
    edit_selected_item();
  }
}

void Controller::edit_selected_item(void) {
  std::vector<Gtk::TreePath> selected;
  switch(main_notebook_page){
  case PAGE_RECEIPT:
    selected = receipts_treeselection->get_selected_rows();
    ricevute_treeview_edit_selected_rows(selected);
    break;
  case PAGE_TASKS:
    selected = prestazioni_treeselection->get_selected_rows();
    prestazioni_treeview_edit_selected_rows(selected);
    break;
  case PAGE_CUSTOMERS:
    selected = clienti_treeselection->get_selected_rows();
    clienti_treeview_edit_selected_rows(selected);
    break;
  }
}

void Controller::on_remove_item_action ( void ) {
  Gtk::MessageDialog alert_dialog { *main_win, "Vuoi eliminare definitivamente gli elementi selezionati?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
  int res_id = alert_dialog.run();
  if (res_id == Gtk::RESPONSE_YES) {
    if (preferenze.get_enable_password()) {
      if (request_password() == Gtk::RESPONSE_OK) {
	if (check_password()) {
	  remove_selected_items();
	} else {
	  on_password_error();
	}
      }
    } else {
      remove_selected_items();
    }
  }
}

void Controller::remove_selected_items(void) {
  std::vector<Gtk::TreePath> selected;
  switch (main_notebook_page)	{
  case PAGE_RECEIPT:
    selected = receipts_treeselection->get_selected_rows();
    ricevute_treeview_remove_selected_rows(selected);
    break;
  case PAGE_TASKS:
    selected = prestazioni_treeselection->get_selected_rows();
    prestazioni_treeview_remove_selected_rows(selected);
    break;
  case PAGE_CUSTOMERS:
    selected = clienti_treeselection->get_selected_rows();
    clienti_treeview_remove_selected_rows(selected);
    break;
  }
}

void Controller::on_stampa_chiusura_cassa_action(void) {
  Glib::ustring str = "<tt>";
  Glib::ustring str2;
  CustomDate inizio, fine;
  float totale_lordo = 0.0;
  float totale_sospeso = 0.0;
  float totale_netto = 0.0;
  size_t line = 32;
  CustomDate::get_time_period_boundary(&inizio, &fine, TIME_PERIOD::daily);
  try {
    std::vector<Ricevuta> ricevute { database->get_ricevute_per_resoconto(inizio, fine) };
    std::vector<Ricevuta>::const_iterator iter_r = ricevute.cbegin();
    CustomDate now {};
    str += now.get_formatted_string(DATE_FORMAT::human) + "\n";
    while (iter_r != ricevute.cend())	{
      if (iter_r->is_consegnato()) {
	// lavoro consegnato il giorno corrente
	str2 += get_stampa_prestazioni(*iter_r, line);
	if (iter_r->get_data_accettazione() >= inizio && iter_r->get_data_accettazione() <= fine) {
	  totale_lordo += iter_r->get_totale_netto();
	  totale_netto += iter_r->get_totale_netto();
	} else {
	  totale_netto += iter_r->get_totale_da_pagare();
	}
      } else {
	// lavoro accettato il giorno corrente
	str2 += get_stampa_prestazioni(*iter_r, line);
	totale_lordo += iter_r->get_totale_netto();
	totale_sospeso += iter_r->get_totale_da_pagare();
	totale_netto += iter_r->get_acconto();
      }
      iter_r++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::on_stampa_chiusura_cassa_action() " << dbe.get_message() << std::endl;
  }
  str += "________________________________\n\n";
  str += Ricevuta::get_formatted_line (Glib::ustring("Incasso lordo"), "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), totale_lordo), line);
  str += Ricevuta::get_formatted_line (Glib::ustring("Incasso in sospeso"), "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), totale_sospeso), line);
  str += Ricevuta::get_formatted_line (Glib::ustring("Incasso netto"), "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), totale_netto), line);
  str += str2;
  str += "________________________________\n\n";
  str += Ricevuta::get_formatted_line (Glib::ustring("Incasso netto"), "€ " + Glib::ustring::format(std::fixed, std::setprecision(2), totale_netto), line);
  str += "</tt>";
  print_or_preview_chiusura(Gtk::PRINT_OPERATION_ACTION_PRINT_DIALOG, str);
}

void Controller::on_page_setup_action(void) {
  m_ref_pagesetup = Gtk::run_page_setup_dialog(*main_win, m_ref_pagesetup, m_ref_printsettings);
  m_ref_pagesetup->save_to_key_file(printer_keyfile);
  std::ofstream outfile(page_setup_path.c_str(), std::ios_base::out);
  if (outfile) {
    outfile << printer_keyfile.to_data();
    outfile.close();
  } else {
    write_log(Glib::ustring("ERROR -> on_page_setup_action() unable to open file \"") + page_setup_path + Glib::ustring("\""));
  }
}

void Controller::on_print_ricevuta_action(void) {
  std::vector<Gtk::TreePath> selected = receipts_treeselection->get_selected_rows();
  Glib::RefPtr<Gtk::TreeModel> ricevute_treemodel = receipts_treeview->get_model();
  std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(selected, ricevute_treemodel) };
  std::vector<Gtk::TreeRowReference>::iterator sr_iter = sel_ref.begin();
  std::vector<Gtk::TreeRowReference>::iterator sr_end = sel_ref.end();
  while (sr_iter != sr_end) {
    Gtk::TreeModel::iterator it_rem = ricevute_treemodel->get_iter(sr_iter->get_path());
    try {
      Ricevuta rcv { database->get_ricevuta((*it_rem)[receipt_tree_mod_col_record.time_acceptance]) };
      print_or_preview_ricevuta(Gtk::PRINT_OPERATION_ACTION_PRINT_DIALOG, rcv.get_ricevuta_string(preferenze.get_print_number()));
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::on_print_ricevuta_action() " << dbe.get_message() << std::endl;
    } catch (const DataBase::DBEmptyResultException& dbere) {
      std::cerr << "Controller::on_print_ricevuta_action() " << dbere.get_message() << std::endl;
    }
    sr_iter++;
  }
}

void Controller::on_print_etichette_action(void) {
  std::vector<Gtk::TreePath> selected = receipts_treeselection->get_selected_rows();
  Glib::RefPtr<Gtk::TreeModel> ricevute_treemodel = receipts_treeview->get_model();
  std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(selected, ricevute_treemodel) };
  std::vector<Gtk::TreeRowReference>::iterator sr_iter = sel_ref.begin();
  std::vector<Gtk::TreeRowReference>::iterator sr_end = sel_ref.end();
  while (sr_iter != sr_end) {
    Gtk::TreeModel::iterator it_rem = ricevute_treemodel->get_iter(sr_iter->get_path());
    try {
      Ricevuta rcv { database->get_ricevuta((*it_rem)[receipt_tree_mod_col_record.time_acceptance]) };
      const std::vector<Capo>* capi { rcv.get_const_capi() };
      std::vector<Capo>::const_iterator iter_cp { capi->begin() };
      while (iter_cp != capi->end()) {
	print_or_preview_etichetta(Gtk::PRINT_OPERATION_ACTION_PRINT_DIALOG, rcv.get_etichetta_string(iter_cp, preferenze.get_print_number()));
	iter_cp++;
      }
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::on_print_etichette_action() " << dbe.get_message() << std::endl;
    } catch (const DataBase::DBEmptyResultException& dbere) {
      std::cerr << "Controller::on_print_etichette_action() " << dbere.get_message() << std::endl;
    }
    sr_iter++;
  }
}

void Controller::on_preferenze_action(void) {
  if (preferenze.get_enable_password()) {
    if (request_password() == Gtk::RESPONSE_OK)	{
      if (check_password()) {
	show_preferenze_panel();
      } else {
	on_password_error();
      }
    }
  } else {
    show_preferenze_panel();
  }
}

void Controller::on_dialog_response(int response_id, Gtk::Dialog *dialog) {
  dialog->hide();
}

void Controller::on_treeview_clicked(GdkEventButton *event) {
  if (event->button == 1 && event->type == GDK_2BUTTON_PRESS) {
    on_edit_item_action();
  }
  if (event->button == 3 && event->type == GDK_BUTTON_PRESS) {
    switch (main_notebook_page)	{
    case PAGE_RECEIPT:
      ricevute_context_menu->popup(event->button, event->time);
      break;
    case PAGE_TASKS:
    case PAGE_CUSTOMERS:
      generic_context_menu->popup(event->button, event->time);
      break;
    }
  }
}

void Controller::load_tasks_in_summary_treeview(void) {
  summary_treestore->clear();
  try {
    std::vector<std::pair<task_info, Prestazione>> tasks { database->get_tasks_from(CustomDate()) };
    std::vector<std::pair<task_info, Prestazione>>::iterator iter { tasks.begin() };
    Gtk::TreeModel::iterator tm_iter1, tm_iter2, tm_iter3;
    Gtk::TreeModel::Row row1, row2, row3;
    float tot1 = 0.0;
    float tot2 = 0.0;
    bool first1 = false;
    bool first2 = false;
    CustomDate prev_pickup;
    prev_pickup.add_days(-1);
    CustomDate prev_date { prev_pickup };
    enum class BSTATE { null, inconsistent, on, off };
    BSTATE bstate = BSTATE::null;
    while (iter != tasks.end())	{
      if (!iter->first.pickup.is_same_day(prev_pickup)) {
	if (first1) {
	  row1[summary_tree_mod_col_record.price] = tot1;
	} else {
	  first1 = true;
	}
	tot1 = 0.0;
	prev_pickup = iter->first.pickup;
	tm_iter1 = summary_treestore->append();
	row1 = *tm_iter1;
	row1[summary_tree_mod_col_record.pickup] = iter->first.pickup.get_formatted_string(DATE_FORMAT::date);
      }
      if (iter->first.date != prev_date) {
	if (first2)	{
	  row2[summary_tree_mod_col_record.price] = tot2;
	  bstate = BSTATE::null;
	} else {
	  first2 = true;
	}
	tot2 = 0.0;
	prev_date = iter->first.date;
	tm_iter2 = summary_treestore->append(row1.children());
	row2 = *tm_iter2;
	row2[summary_tree_mod_col_record.customer] = iter->first.customer;
	row2[summary_tree_mod_col_record.time_acp] = iter->first.date.get_time_t();
      }
      tot2 += iter->second.get_prezzo();
      tot1 += iter->second.get_prezzo();
      tm_iter3 = summary_treestore->append(row2.children());
      row3 = *tm_iter3;
      row3[summary_tree_mod_col_record.garment] = iter->second.get_capo();
      row3[summary_tree_mod_col_record.description] = iter->second.get_descrizione();
      row3[summary_tree_mod_col_record.index] = iter->first.index;
      row3[summary_tree_mod_col_record.price] = iter->second.get_prezzo();
      row3[summary_tree_mod_col_record.completed] = iter->second.get_completata();
      if (bstate == BSTATE::null)	{
	bstate = iter->second.get_completata() ? BSTATE::on : BSTATE::off;
      } else if (bstate != BSTATE::inconsistent) {
	BSTATE row_state = iter->second.get_completata() ? BSTATE::on : BSTATE::off;
	if (bstate != row_state) {
	  bstate = BSTATE::inconsistent;
	}
      }
      row2[summary_tree_mod_col_record.completed] = bstate == BSTATE::on ? true : false;
      iter++;
    }
    if (first2)	{
      row1[summary_tree_mod_col_record.price] = tot1;
      row2[summary_tree_mod_col_record.price] = tot2;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::load_tasks_in_summary_treeview() " << dbe.get_message() << std::endl;
  }
  summary_treeview->expand_all();
}

void Controller::on_task_completed_toggled(const Glib::ustring &path) {
  /*
   * path is a Glib::ustring path which is needed to get the Gtk::TreeModel::iterator
   * with the iterator we can get the Gtk::TreePath which will be usefull to navigate the tree hierarchy
   */
  Glib::RefPtr<Gtk::TreeModel> summary_treemodel = summary_treeview->get_model();
  Gtk::TreeModel::iterator iterTree = summary_treemodel->get_iter(path);
  Gtk::TreePath tpath = summary_treemodel->get_path(iterTree);
  Gtk::TreeModel::Row row = *iterTree;
  // try to expand row or check if it's already expanded
  if (summary_treeview->expand_row(tpath, false) || summary_treeview->row_expanded(tpath)) {
    if (Glib::ustring().compare(row[summary_tree_mod_col_record.pickup]) != 0) { // this is the row with pickup date
      row[summary_tree_mod_col_record.completed] = false;
    }
    if (Glib::ustring().compare(row[summary_tree_mod_col_record.customer]) != 0) { // customer name row, update all receipt
      try {
	database->update_receipt_completed(row[summary_tree_mod_col_record.time_acp], row[summary_tree_mod_col_record.completed]);
      } catch (const DataBase::DBError& dbe) {
	std::cerr << "Controller::on_task_completed_toggled() " << dbe.get_message() << std::endl;
      }
      load_tasks_in_summary_treeview();
    }
  } else {
    bool flag { row[summary_tree_mod_col_record.completed] };
    int index { row[summary_tree_mod_col_record.index] };
    Glib::ustring description { row[summary_tree_mod_col_record.description] };
    tpath.up(); // go up to the parent row to in order to get the date
    Gtk::TreeModel::iterator parent_iter = summary_treemodel->get_iter(tpath);
    Gtk::TreeModel::Row parent_row = *parent_iter;
    time_t unixtime { parent_row[summary_tree_mod_col_record.time_acp] };
    try {
      database->update_task_completed(unixtime, index, description, flag);
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::on_task_completed_toggled() " << dbe.get_message() << std::endl;
    }
    load_tasks_in_summary_treeview();
  }
}

float Controller::totale_ricevute_del_giorno(const CustomDate& tempo) {
  float totale = 0.0;
  try {
    std::vector<Ricevuta> ricevute { database->get_ricevute_in_consegna_il(tempo) };
    std::vector<Ricevuta>::iterator iter { ricevute.begin() };
    while (iter != ricevute.end())	{
      totale += iter->get_totale_netto();
      iter++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::totale_ricevute_del_giorno() " << dbe.get_message() << std::endl;
  }
  return totale;
}

void Controller::load_ricevute_in_treeview(void) {
  receipts_liststore->clear();
  try {
    std::vector<Ricevuta> ricevute { database->get_ricevute() };
    std::vector<Ricevuta>::iterator iter { ricevute.begin() };
    while (iter != ricevute.end())	{
      Gtk::TreeModel::iterator iterTree { receipts_liststore->append() };
      Gtk::TreeModel::Row row { *iterTree };
      row[receipt_tree_mod_col_record.number] = iter->get_numero_string();
      row[receipt_tree_mod_col_record.customer] = iter->get_nome_cliente();
      row[receipt_tree_mod_col_record.date_acceptance] = iter->get_data_acc_string(DATE_FORMAT::human);
      row[receipt_tree_mod_col_record.total] = iter->get_totale_netto();
      row[receipt_tree_mod_col_record.deposit] = iter->get_acconto();
      row[receipt_tree_mod_col_record.date_deliver] = iter->get_data_rit_string(DATE_FORMAT::label);
      row[receipt_tree_mod_col_record.urgent] = iter->is_urgente();
      row[receipt_tree_mod_col_record.delivered] = iter->is_consegnato();
      row[receipt_tree_mod_col_record.time_acceptance] = iter->get_data_accettazione().get_time_t();
      row[receipt_tree_mod_col_record.time_deliver] = iter->get_data_ritiro().get_time_t();
      iter++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::load_ricevute_in_treeview() " << dbe.get_message() << std::endl;
  }
}

void Controller::ricevute_treeview_remove_selected_rows(const std::vector<Gtk::TreePath> &sel) {
  Glib::ustring messaggio;
  Glib::RefPtr<Gtk::TreeModel> ricevute_treemodel = receipts_treeview->get_model();
  std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, ricevute_treemodel) };
  std::vector<Gtk::TreeRowReference>::iterator sr_iter { sel_ref.begin() };
  while (sr_iter != sel_ref.end()) {
    Gtk::TreeModel::iterator it_rem = ricevute_treemodel->get_iter(sr_iter->get_path());
    try {
      if (database->is_ricevuta_present((*it_rem)[receipt_tree_mod_col_record.time_acceptance])) {
	database->delete_ricevuta((*it_rem)[receipt_tree_mod_col_record.time_acceptance]);
	receipts_liststore->erase(it_rem);
      } else {
	write_log("ERROR -> ricevute_treeview_remove_selected_rows() couldn't find ricevuta");
      }
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::ricevute_treeview_remove_selected_rows() " << dbe.get_message() << std::endl;
    }
    sr_iter++;
  }
  main_statusbar->pop(stbrItemSelId);
  if (sel.size() == 1) {
    messaggio.append(Glib::ustring("Una ricevuta eliminata."));
  } else {
    messaggio.append(Glib::ustring::compose("%1 ricevute eliminate.", Glib::ustring::format(sel.size())));
  }
  main_statusbar->push(messaggio, stbrItemSelId);
}

void Controller::ricevute_treeview_edit_selected_rows(const std::vector<Gtk::TreePath> &sel) {
  int nr_sel = receipts_treeselection->count_selected_rows();
  Glib::RefPtr<Gtk::TreeModel> ricevute_treemodel { receipts_treeview->get_model() };
  std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, ricevute_treemodel) };
  if (nr_sel > 0)	{
    std::vector<Gtk::TreePath>::const_iterator iter = sel.begin();
    Gtk::TreeModel::iterator iter_view = ricevute_treemodel->get_iter(*iter);
    Gtk::TreeModel::Row row = *iter_view;
    if (preferenze.get_view_number()) {
      Glib::ustring numero = row[receipt_tree_mod_col_record.number];
      new_ricevuta_window->set_title(Glib::ustring("Ricevuta numero: ") + numero);
    } else {
      new_ricevuta_window->set_title(Glib::ustring("Ricevuta"));
    }
    cliente_ricevuta_entry->set_text(row[receipt_tree_mod_col_record.customer]);
    selezione_capo_combobox->set_active(-1);
    selezione_prestazione_combobox->set_active(-1);
    acconto_ricevuta_entry->set_text(Glib::ustring::format(row[receipt_tree_mod_col_record.deposit]));
    stampa_ricevuta_checkbutton->set_active(false);
    urgente_ricevuta_checkbutton->set_active(row[receipt_tree_mod_col_record.urgent]);
    if (nr_sel > 1)	{
      data_accettazione_ricevuta_label->set_text("selezione multipla");
      cambia_data_ricevuta_button1->set_sensitive(false);
      data_ritiro_ricevuta_label->set_text("selezione multipla");
      cambia_data_ricevuta_button2->set_sensitive(false);
      selezione_prestazione_combobox->set_sensitive(false);
      totale_ricevuta_label->set_text("selezione multipla");
    } else {
      data_accettazione_ricevuta_label->set_text(row[receipt_tree_mod_col_record.date_acceptance]);
      cambia_data_ricevuta_button1->set_sensitive(true);
      time_t timedeliv = row[receipt_tree_mod_col_record.time_deliver];
      CustomDate cddeliv { &timedeliv };
      data_ritiro_ricevuta_label->set_text(cddeliv.get_formatted_string(DATE_FORMAT::human));
      cambia_data_ricevuta_button2->set_sensitive(true);
      selezione_prestazione_combobox->set_sensitive(true);
      std::vector<Gtk::TreeRowReference>::iterator sr_iter = sel_ref.begin();
      Gtk::TreeModel::iterator it_r = ricevute_treemodel->get_iter(sr_iter->get_path());
      try {
	Ricevuta rcv { database->get_ricevuta((*it_r)[receipt_tree_mod_col_record.time_acceptance]) };
	const std::vector<Capo>* capi { rcv.get_const_capi() };
	load_prestazioni_ricevuta_in_treeview(capi->begin(), capi->end());
	prestazioni_ricevuta_treeview->expand_all();
	totale_ricevuta_label->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), rcv.get_totale_netto()));
	if (rcv.is_pagato()) {
	  acconto_ricevuta_entry->set_sensitive(false);
	  pagato_radiobutton->set_active(true);
	} else {
	  acconto_ricevuta_entry->set_sensitive(true);
	  nonpagato_radiobutton->set_active(true);
	}
      } catch (const DataBase::DBError& dbe) {
	std::cerr << "Controller::ricevute_treeview_edit_selected_rows() " << dbe.get_message() << std::endl;
      } catch (const DataBase::DBEmptyResultException& dbere) {
	std::cerr << "Controller::ricevute_treeview_edit_selected_rows() " << dbere.get_message() << std::endl;
      }
    }
    new_ricevuta_window->present();
  }
}

void Controller::on_ricevuta_urgente_toggled(const Glib::ustring &path) {
  Glib::RefPtr<Gtk::TreeModel> ricevute_treemodel { receipts_treeview->get_model() };
  Gtk::TreeModel::iterator iterTree { ricevute_treemodel->get_iter(path) };
  Gtk::TreeModel::Row row { *iterTree };
  try {
    Ricevuta rcv { database->get_ricevuta(row[receipt_tree_mod_col_record.time_acceptance]) };
    rcv.set_urgente(row[receipt_tree_mod_col_record.urgent]);
    row[receipt_tree_mod_col_record.total] = rcv.get_totale_netto();
    database->update(rcv.get_date_acc_unix(), rcv, MEMB_FIELD::urg);
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::on_ricevuta_urgente_toggled() " << dbe.get_message() << std::endl;
  } catch (const DataBase::DBEmptyResultException& dbere) {
    std::cerr << "Controller::on_ricevuta_urgente_toggled() " << dbere.get_message() << std::endl;
  }
}

void Controller::on_ricevuta_consegnato_toggled(const Glib::ustring &path) {
  Glib::RefPtr<Gtk::TreeModel> ricevute_treemodel { receipts_treeview->get_model() };
  Gtk::TreeModel::iterator iterTree { ricevute_treemodel->get_iter(path) };
  Gtk::TreeModel::Row row { *iterTree };
  try {
    Ricevuta rcv { database->get_ricevuta(row[receipt_tree_mod_col_record.time_acceptance]) };
    if (row[receipt_tree_mod_col_record.delivered])	{
      float pagare = rcv.get_totale_da_pagare();
      Gtk::MessageDialog *alert_dialog = new Gtk::MessageDialog(*main_win, Glib::ustring::compose("Totale da pagare %1 euro. Vuoi contrassegnare la ricevuta selezionata come lavoro consegnato?", Glib::ustring::format(std::fixed, std::setprecision(2), pagare)), false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true);
      int res_id = alert_dialog->run();
      delete alert_dialog;
      if (res_id == Gtk::RESPONSE_YES) {
	rcv.set_consegnato(row[receipt_tree_mod_col_record.delivered]);
	rcv.set_lavoro_completato();
	time_t today = time(nullptr);
	rcv.set_data_ritiro(CustomDate {&today});
	row[receipt_tree_mod_col_record.date_deliver] = rcv.get_data_rit_string(DATE_FORMAT::human);
	database->update(rcv.get_date_acc_unix(), rcv);
	on_items_treeselection_changed();
	main_statusbar->pop(stbrItemSelId);
	main_statusbar->push(Glib::ustring("Una ricevuta contrassegnata come lavoro consegnato."), stbrItemSelId);
      } else {
	row[receipt_tree_mod_col_record.delivered] = false;
      }
    } else {
      Gtk::MessageDialog *alert_dialog = new Gtk::MessageDialog(*main_win, Glib::ustring("Questa ricevuta risulta come lavoro già consegnato, vuoi contrassegnarla come lavoro non ancora consegnato?"), false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true);
      int res_id = alert_dialog->run();
      delete alert_dialog;
      if (res_id == Gtk::RESPONSE_YES) {
	rcv.set_consegnato(row[receipt_tree_mod_col_record.delivered]);
	database->update(rcv.get_date_acc_unix(), rcv);
	on_items_treeselection_changed();
	main_statusbar->pop(stbrItemSelId);
	main_statusbar->push(Glib::ustring("Una ricevuta contrassegnata come lavoro non consegnato"), stbrItemSelId);
      } else {
	row[receipt_tree_mod_col_record.delivered] = true;
      }
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::on_ricevuta_consegnato_toggled() " << dbe.get_message() << std::endl;
  } catch (const DataBase::DBEmptyResultException& dbere) {
    std::cerr << "Controller::on_ricevuta_consegnato_toggled() " << dbere.get_message() << std::endl;
  }
}

void Controller::load_prestazioni_in_treeview(void) {
  tasks_liststore->clear();
  try {
    std::vector<Prestazione> db_prest { database->get_prestazioni() };
    std::vector<Prestazione>::iterator iter = db_prest.begin();
    while (iter != db_prest.end())	{
      Gtk::TreeModel::iterator iterTree = tasks_liststore->append();
      Gtk::TreeModel::Row row = *iterTree;
      row[task_tree_mod_col_record.garment] = iter->get_capo();
      row[task_tree_mod_col_record.description] = iter->get_descrizione();
      row[task_tree_mod_col_record.price] = iter->get_prezzo();
      iter++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::load_prestazioni_in_treeview() " << dbe.get_message() << std::endl;
  }
}

void Controller::prestazioni_treeview_remove_selected_rows(const std::vector<Gtk::TreePath> &sel) {
  Glib::ustring messaggio;
  Glib::RefPtr<Gtk::TreeModel> prestazioni_treemodel { tasks_treeview->get_model() };
  std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, prestazioni_treemodel) };
  std::vector<Gtk::TreeRowReference>::iterator sr_iter = sel_ref.begin();
  while (sr_iter != sel_ref.end()) {
    Gtk::TreeModel::iterator it_rem = prestazioni_treemodel->get_iter(sr_iter->get_path());
    Glib::ustring capo = (*it_rem)[task_tree_mod_col_record.garment];
    Glib::ustring descrizione = (*it_rem)[task_tree_mod_col_record.description];
    try {
      if (database->is_prestazione_present(capo, descrizione)) {
	if (mappa_capi.count(capo) > 1) {
	  mappa_capi.at(capo) -= 1;
	} else {
	  mappa_capi.erase(capo);
	  load_capo_popup_button();
	  load_capi_in_prestazione_completion();
	}
	database->delete_prestazione(capo, descrizione);
	tasks_liststore->erase(it_rem);
      } else {
	write_log("ERROR -> prestazioni_treeview_remove_selected_rows() couldn't find prestazione");
      }
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::prestazioni_treeview_remove_selected_rows() " << dbe.get_message() << std::endl;
    }
    sr_iter++;
  }
  main_statusbar->pop(stbrItemSelId);
  if (sel.size() == 1) {
    messaggio.append(Glib::ustring("Una prestazione eliminata."));
  } else {
    messaggio.append(Glib::ustring::compose("%1 prestazioni eliminate.", Glib::ustring::format(sel.size())));
  }
  main_statusbar->push(messaggio, stbrItemSelId);
}

void Controller::prestazioni_treeview_edit_selected_rows(const std::vector<Gtk::TreePath> &sel) {
  int nr_sel = prestazioni_treeselection->count_selected_rows();
  Glib::RefPtr<Gtk::TreeModel> prestazioni_treemodel { tasks_treeview->get_model() };
  if (nr_sel > 0)	{
    std::vector<Gtk::TreePath>::const_iterator iter { sel.begin() };
    Gtk::TreeModel::iterator iter_view { prestazioni_treemodel->get_iter(*iter) };
    Gtk::TreeModel::Row row { *iter_view };
    prezzo_prestazione_entry->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), row[task_tree_mod_col_record.price]));
    if (nr_sel > 1)	{
      capo_prestazione_entry->set_text("selezione multipla");
      capo_prestazione_entry->set_sensitive(false);
      descrizione_prestazione_entry->set_text("selezione multipla");
      descrizione_prestazione_entry->set_sensitive(false);
    } else {
      capo_prestazione_entry->set_text(row[task_tree_mod_col_record.garment]);
      capo_prestazione_entry->set_sensitive(true);
      descrizione_prestazione_entry->set_text(row[task_tree_mod_col_record.description]);
      descrizione_prestazione_entry->set_sensitive(true);
    }
    new_prestazione_window->present();
  }
}

void Controller::load_clienti_in_treeview(void) {
  try {
    std::vector<Cliente> db_clienti { database->get_clienti() };
    clienti_liststore->clear();
    std::vector<Cliente>::iterator iter = db_clienti.begin();
    while (iter != db_clienti.end()) {
      Gtk::TreeModel::iterator iterTree = clienti_liststore->append();
      Gtk::TreeModel::Row row = *iterTree;
      row[customer_tree_mod_col_record.name] = iter->get_nome();
      row[customer_tree_mod_col_record.telephone] = iter->get_telefono();
      iter++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::load_clienti_in_treeview() " << dbe.get_message() << std::endl;
  }
}

void Controller::clienti_treeview_remove_selected_rows(const std::vector<Gtk::TreePath> &sel) {
  Glib::ustring messaggio;
  Glib::RefPtr<Gtk::TreeModel> clienti_treemodel { clienti_treeview->get_model() };
  std::vector<Gtk::TreeRowReference> sel_ref { get_treeview_row_ref(sel, clienti_treemodel) };
  std::vector<Gtk::TreeRowReference>::iterator sr_iter { sel_ref.begin() };
  while (sr_iter != sel_ref.end()) {
    Gtk::TreeModel::iterator it_rem { clienti_treemodel->get_iter(sr_iter->get_path()) };
    try {
      database->delete_cliente((*it_rem)[customer_tree_mod_col_record.name]);
      clienti_liststore->erase(it_rem);
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::clienti_treeview_remove_selected_rows() " << dbe.get_message() << std::endl;
    }
    sr_iter++;
  }
  main_statusbar->pop(stbrItemSelId);
  if (sel.size() == 1) {
    messaggio.append(Glib::ustring("Un cliente eliminato."));
  } else {
    messaggio.append(Glib::ustring::compose("%1 clienti eliminati.", Glib::ustring::format(sel.size())));
  }
  main_statusbar->push(messaggio, stbrItemSelId);
}

void Controller::clienti_treeview_edit_selected_rows(const std::vector<Gtk::TreePath> &sel) {
  int nr_sel = clienti_treeselection->count_selected_rows();
  Glib::RefPtr<Gtk::TreeModel> clienti_treemodel { clienti_treeview->get_model() };
  if (nr_sel > 0)	{
    std::vector<Gtk::TreePath>::const_iterator iter = sel.begin();
    Gtk::TreeModel::iterator iter_view = clienti_treemodel->get_iter(*iter);
    Gtk::TreeModel::Row row = *iter_view;
    telefono_cliente_entry->set_text(row[customer_tree_mod_col_record.telephone]);
    if (nr_sel > 1) {
      nome_cliente_entry->set_text("selezione multipla");
      nome_cliente_entry->set_sensitive(false);
    } else {
      nome_cliente_entry->set_text(row[customer_tree_mod_col_record.name]);
      nome_cliente_entry->set_sensitive(true);
    }
    new_cliente_window->present();
  }
}

void Controller::aggiorna_pagina_resoconto(void) {
  CustomDate inizio, fine;
  TIME_PERIOD period;
  switch (selezione_periodo_combobox->get_active_row_number()) {
  case 0:
    period = TIME_PERIOD::daily;
    break;
  case 1:
    period = TIME_PERIOD::weekly;
    break;
  case 2:
    period = TIME_PERIOD::monthly;
    break;
  case 3:
    period = TIME_PERIOD::annually;
    break;
  }
  CustomDate::get_time_period_boundary(&inizio, &fine, period);
  int numero_ricevute = 0;
  int lavori_consegnati = 0;
  float incasso_totale = 0.0;
  std::list<Glib::ustring> customers;
  std::list<Prestazione> works;
  ricevute_resoconto_liststore->clear();
  try {
    std::vector<Ricevuta> ricevute { database->get_ricevute_per_resoconto(inizio, fine) };
    std::vector<Ricevuta>::iterator iter { ricevute.begin() };
    while (iter != ricevute.end()) {
      if (iter->is_consegnato()) {
	lavori_consegnati++;
	if (iter->get_data_accettazione() >= inizio && iter->get_data_accettazione() <= fine) {
	  numero_ricevute++;
	  incasso_totale += iter->get_totale_netto();
	} else {
	  incasso_totale += iter->get_totale_da_pagare();
	}
	Glib::ustring nome = iter->get_nome_cliente();
	std::list<Glib::ustring>::iterator it_cl = find(customers.begin(), customers.end(), nome);
	if (it_cl == customers.end()) {
	  customers.push_back(nome);
	}
	const std::vector<Capo>* capi_r = iter->get_const_capi();
	std::vector<Capo>::const_iterator iter_c = capi_r->begin();
	std::vector<Capo>::const_iterator end_c = capi_r->end();
	while (iter_c != end_c)	{
	  const std::vector<Prestazione>* prestazioni_r = iter_c->get_const_prestazioni();
	  std::vector<Prestazione>::const_iterator iter_p = prestazioni_r->begin();
	  std::vector<Prestazione>::const_iterator end_p = prestazioni_r->end();
	  while (iter_p != end_p)	{
	    works.push_back(*iter_p);
	    iter_p++;
	  }
	  iter_c++;
	}
      } else {
	numero_ricevute++;
	incasso_totale += iter->get_acconto();
	Glib::ustring nome = iter->get_nome_cliente();
	std::list<Glib::ustring>::iterator it_cl = find(customers.begin(), customers.end(), nome);
	if (it_cl == customers.end()) {
	  customers.push_back(nome);
	}
	Gtk::TreeModel::iterator iterTree { ricevute_resoconto_liststore->append() };
	Gtk::TreeModel::Row row { *iterTree };
	row[report_receipt_tree_mod_col_record.number] = iter->get_numero_string();
	row[report_receipt_tree_mod_col_record.customer] = iter->get_nome_cliente();
	row[report_receipt_tree_mod_col_record.date_acceptance] = iter->get_data_acc_string(DATE_FORMAT::human);
	row[report_receipt_tree_mod_col_record.total] = Glib::ustring::format(std::fixed, std::setprecision(2), iter->get_totale_netto());
	row[report_receipt_tree_mod_col_record.time_acceptance] = iter->get_data_accettazione();
      }
		
      iter++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::aggiorna_pagina_resoconto() " << dbe.get_message() << std::endl;
  }
  std::list<Prestazione>::iterator iterP = works.begin();
  prestazioni_resoconto_liststore->clear();
  while (iterP != works.end()) {
    Gtk::TreeModel::iterator iterTree { prestazioni_resoconto_liststore->append() };
    Gtk::TreeModel::Row row { *iterTree };
    row[receipt_task_tree_mod_col_record.number] = Glib::ustring();
    row[receipt_task_tree_mod_col_record.garment] = iterP->get_capo();
    row[receipt_task_tree_mod_col_record.description] = iterP->get_descrizione();
    row[receipt_task_tree_mod_col_record.price] = iterP->get_prezzo();
    iterP++;
  }
  std::list<Glib::ustring>::iterator iterC = customers.begin();
  clienti_resoconto_liststore->clear();
  while (iterC != customers.end()) {
    Gtk::TreeModel::iterator iterTree { clienti_resoconto_liststore->append() };
    Gtk::TreeModel::Row row { *iterTree };
    row[customer_tree_mod_col_record.name] = *iterC;
    iterC++;
  }
  resoconto_ricevute_emesse_label->set_text(Glib::ustring::format(numero_ricevute));
  resoconto_lavori_consegnati_label->set_text(Glib::ustring::format(lavori_consegnati));
  resoconto_clienti_serviti_label->set_text(Glib::ustring::format(customers.size()));
  resoconto_incasso_totale_label->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), incasso_totale));
}

void Controller::update_mappa_capi(void) {
  try {
    std::vector<Glib::ustring> capi { database->get_capo_prestazioni() };
    std::vector<Glib::ustring>::iterator iter { capi.begin() };
    while (iter != capi.end()) {
      if(mappa_capi.count(*iter)) {
	mappa_capi.at(*iter) += 1;
      } else {
	mappa_capi.insert(std::pair<Glib::ustring,int>(*iter,1));
      }
      iter++;
    }
  } catch (const DataBase::DBError& dbe) {
    std::cerr << "Controller::update_mappa_capi() " << dbe.get_message() << std::endl;
  }
}

void Controller::load_capo_popup_button(void) {
  capo_popup_connection.disconnect();
  selezione_capo_liststore->clear();
  std::map<Glib::ustring,int>::iterator iter_capi { mappa_capi.begin() };
  while (iter_capi != mappa_capi.end()) {
    Gtk::TreeModel::iterator iter { selezione_capo_liststore->append() };
    Gtk::TreeModel::Row row { *iter };
    row[combobox_tmcr.column] = iter_capi->first;
    iter_capi++;
  }
  selezione_capo_liststore->set_sort_column(0, Gtk::SORT_ASCENDING);
  selezione_capo_combobox->set_active(-1);
  connect_capo_popup();
}

void Controller::load_prestazione_popup_button(void) {
  prestazione_popup_connection.disconnect();
  selezione_prestazione_liststore->clear();
  Glib::ustring capo { Glib::ustring() };
  int indice = selezione_capo_combobox->get_active_row_number();
  if (indice >= 0) {
    Gtk::TreeModel::iterator iter { selezione_capo_combobox->get_active() };
    Gtk::TreeModel::Row row { *iter };
    capo.assign(row[combobox_tmcr.column]);
    try {
      std::vector<Glib::ustring> strings { database->get_descrizione_prestazioni_per_capo(capo) };
      std::vector<Glib::ustring>::iterator iter_prestazioni { strings.begin() };
      while (iter_prestazioni != strings.end()) {
	Gtk::TreeModel::iterator iter { selezione_prestazione_liststore->append() };
	Gtk::TreeModel::Row row { *iter };
	row[combobox_tmcr.column] = *iter_prestazioni;
	iter_prestazioni++;
      }
      selezione_prestazione_liststore->set_sort_column(0, Gtk::SORT_ASCENDING);
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::load_prestazione_popup_button() " << dbe.get_message() << std::endl;
    }
  }
  connect_prestazione_popup();
}

void Controller::on_trova_clienti_action(void) {
  load_customers_in_treeview(clienti_suggerimento_liststore);
  suggerimento_clienti_window->present();
}

void Controller::on_cambia_data_ricevuta_action(Gtk::Button *sender) {
  Glib::ustring data_ora_string;
  if (sender == cambia_data_ricevuta_button1)	{
    calendario_window->set_title("Accettazione");
    data_ora_string = data_accettazione_ricevuta_label->get_text();
  } else if (sender == cambia_data_ricevuta_button2) {
    calendario_window->set_title("Ritiro");
    data_ora_string = data_ritiro_ricevuta_label->get_text();
  }
  CustomDate data_ora { CustomDate::parse_string(data_ora_string.c_str(), DATE_FORMAT::human) };
  main_calendar->select_month(data_ora.get_month(), data_ora.get_year() + 1900);
  main_calendar->select_day(data_ora.get_day());
  int index = data_ora.get_hour() - 7;
  if (index < 0) {
    index = 0;
  } else if (index > 13) {
    index = 13;
  }
  calendar_hour_combobox->set_active(index);
  index = data_ora.get_minute() / 5;
  calendar_minute_combobox->set_active(index);
  calendario_window->present();
}

void Controller::load_prestazioni_ricevuta_in_treeview(std::vector<Capo>::const_iterator iter, std::vector<Capo>::const_iterator end) {
  prestazioni_ricevuta_connection.disconnect();
  prestazioni_ricevuta_treestore->clear();
  while (iter != end)	{
    Gtk::TreeModel::iterator iterTree { prestazioni_ricevuta_treestore->append() };
    Gtk::TreeModel::Row row { *iterTree };
    row[receipt_task_tree_mod_col_record.number] = Glib::ustring::format(iter->get_numero() + 1);
    row[receipt_task_tree_mod_col_record.garment] = iter->get_capo();
    const std::vector<Prestazione>* prestazioni_capo { iter->get_const_prestazioni() };
    std::vector<Prestazione>::const_iterator prst_iter { prestazioni_capo->begin() };
    while (prst_iter != prestazioni_capo->end()) {
      Gtk::TreeModel::iterator iterRowChild { prestazioni_ricevuta_treestore->append(row.children()) };
      Gtk::TreeModel::Row childRow { *iterRowChild };
      childRow[receipt_task_tree_mod_col_record.description] = prst_iter->get_descrizione();
      childRow[receipt_task_tree_mod_col_record.price] = prst_iter->get_prezzo();
      prst_iter++;
    }
    iter++;
  }
  connect_prestazioni_ricevuta();
}

Gtk::TreeModel::Path Controller::get_prestazione_ricevuta_selezionata(void) {
  std::vector<Gtk::TreePath> selected { prestazioni_ricevuta_treeselection->get_selected_rows() };
  Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_treemodel { prestazioni_ricevuta_treeview->get_model() };
  std::vector<Gtk::TreeRowReference> selRef { get_treeview_row_ref(selected, prestazioni_ricevuta_treemodel) };
  std::vector<Gtk::TreeRowReference>::iterator srIter { selRef.begin() };
  return srIter->get_path();
}

/**
 * funziona chiamata quando cambia la selezione delle prestazioni nella finestra della ricevuta
 * aggiorna la selezione dei tasti popup dei capi e della prestazione
 * */
void Controller::on_prestazioni_ricevuta_selection_changed(void) {
  // controlla se ci sono righe selezionate
  if (prestazioni_ricevuta_treeselection->count_selected_rows() > 0) {
    // attiva il tasto di rimozione dei capi o prestazioni
    rem_prestazione_ricevuta_button->set_sensitive(true);
    // trova l'iterator della riga selezionata
    Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
    Gtk::TreeModel::Path path { get_prestazione_ricevuta_selezionata() };
    Gtk::TreeModel::iterator iter_tree { prestazioni_ricevuta_model->get_iter(path) };
    Gtk::TreeModel::Row row_tree { *iter_tree };
    // tenta di espandere la riga selezionata e verifica se è già espansa
    if (prestazioni_ricevuta_treeview->expand_row(path, false) || prestazioni_ricevuta_treeview->row_expanded(path)) {
      // è stato selezionato un capo della ricevuta
      // aggiorna la selezione del tasto popup dei capi
      update_capo_popup_button(Glib::ustring(row_tree[receipt_task_tree_mod_col_record.garment]));
      // aggiorna la selezione del tasto popup delle prestazioni
      update_prestazioni_popup_button(Glib::ustring());
      // disattiva il tasto di addizione di una nuova prestazione
      add_prestazione_ricevuta_button->set_sensitive(false);
    } else {
      // è stata selezionata una prestazione
      // risale alla riga genitore per trovare il capo di appartenenza
      path.up();
      Gtk::TreeModel::iterator parent_iter { prestazioni_ricevuta_model->get_iter(path) };
      Gtk::TreeModel::Row parent_row { *parent_iter };
      // aggiorna la selezione del tasto popup dei capi
      update_capo_popup_button (Glib::ustring(parent_row[receipt_task_tree_mod_col_record.garment]));
      // aggiorna la selezione del tasto popup delle prestazioni
      update_prestazioni_popup_button (Glib::ustring(row_tree[receipt_task_tree_mod_col_record.description]));
      // attiva il tasto di addizione di una nuova prestazione
      add_prestazione_ricevuta_button->set_sensitive(true);
    }
  } else {
    rem_prestazione_ricevuta_button->set_sensitive(false);
    add_prestazione_ricevuta_button->set_sensitive(false);
    update_capo_popup_button (Glib::ustring());
  }
}

/**
 * aggiorna la selezione del tasto popup dei capi con il capo passato come parametro
 * */
bool Controller::update_capo_popup_button ( const Glib::ustring capo ) {
  Glib::ustring capo_button;
  bool result = false;
  if ( selezione_capo_combobox->get_active_row_number () >= 0 ) {
    Gtk::TreeModel::iterator it_popup { selezione_capo_combobox->get_active () };
    Gtk::TreeModel::Row row_popup { *it_popup };
    capo_button = row_popup[combobox_tmcr.column];
  }
  if ( capo != capo_button ) {
    capo_popup_connection.disconnect();
    selezione_capo_combobox->set_active(-1);
    if (capo.compare(Glib::ustring()) != 0) {
      int n = 0;
      Gtk::TreeModel::iterator iter { selezione_capo_liststore->children().begin() };
      while (iter != selezione_capo_liststore->children().end()) {
	Gtk::TreeModel::Row row { *iter };
	if (row[combobox_tmcr.column] == capo) {
	  selezione_capo_combobox->set_active(n);
	  result = true;
	  break;
	}
	n++;
	iter++;
      }
    } else {
      result = true;
    }
    load_prestazione_popup_button();
    connect_capo_popup();
    return result;
  }
  return result;
}

/**
 * aggiorna la selezione del tasto popup delle prestazioni con la descrizione passata come parametro
 * */
void Controller::update_prestazioni_popup_button(const Glib::ustring descrizione) {
  Glib::ustring desc_button;
  if (selezione_prestazione_combobox->get_active_row_number() > -1) {
    Gtk::TreeModel::iterator it_popup { selezione_prestazione_combobox->get_active() };
    Gtk::TreeModel::Row row_popup { *it_popup };
    desc_button = row_popup[combobox_tmcr.column];
  }
  if (descrizione != desc_button) {
    prestazione_popup_connection.disconnect();
    selezione_prestazione_combobox->set_active(-1);
    if (descrizione.compare(Glib::ustring()) != 0) {
      int n = 0;
      Gtk::TreeModel::iterator iter { selezione_prestazione_liststore->children().begin() };
      while (iter != selezione_prestazione_liststore->children().end()) {
	Gtk::TreeModel::Row row { *iter };
	if (row[combobox_tmcr.column] == descrizione) {
	  selezione_prestazione_combobox->set_active(n);
	  break;
	}
	n++;
	iter++;
      }
    }
    connect_prestazione_popup();
  }
}

/**
 * funzione chiamata quando la riga viene espansa
 * seleziona la riga soggetta ad espansione
 * */
void Controller::on_prestazioni_ricevuta_row_expanded(const Gtk::TreeModel::iterator& iter, const Gtk::TreeModel::Path& path) {
  prestazioni_ricevuta_connection.disconnect();
  Gtk::TreeModel::Row row { *iter };
  prestazioni_ricevuta_treeselection->select(row);
  connect_prestazioni_ricevuta ();
}

/**
 * funzione chiamata quando si cambia la selezione del tasto popup delle prestazioni
 * */
void Controller::on_selezione_prestazione_popup_changed(void) {
  // verifica che sia selezionata una prestazione
  if (selezione_prestazione_combobox->get_active_row_number() >= 0) {
    add_prestazione_ricevuta_button->set_sensitive (true);
    Gtk::TreeModel::iterator iter_popup = selezione_prestazione_combobox->get_active();
    Gtk::TreeModel::Row row_popup = *iter_popup;
    Glib::ustring prest_popup_sel = row_popup[combobox_tmcr.column];
    iter_popup = selezione_capo_combobox->get_active();
    row_popup = *iter_popup;
    Glib::ustring capo_popup_sel = row_popup[combobox_tmcr.column];
    Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model = prestazioni_ricevuta_treeview->get_model ();
    // verifica se ci sono capi o prestazioni selezionate nel TreeView
    if (prestazioni_ricevuta_treeselection->count_selected_rows() > 0)	{
      Gtk::TreeModel::Path path = get_prestazione_ricevuta_selezionata ();
      Gtk::TreeModel::iterator iter = prestazioni_ricevuta_model->get_iter (path);
      Gtk::TreeModel::Row row = *iter;
      Glib::ustring descrizione = row[receipt_task_tree_mod_col_record.description];
      // se la descrizione è una stringa vuota allora è stato selezionato un capo
      if (descrizione.compare(Glib::ustring()) != 0) {
	path.up();
	iter = prestazioni_ricevuta_model->get_iter (path);
	row = *iter;
      }
      // verifica che il capo selezionato sulla lista e quello del tasto popup corrispondono
      if (capo_popup_sel == row[receipt_task_tree_mod_col_record.garment]) {
	// itera tra le prestazioni associate al capo per vedere se quella selezionata sul tasto popup è già esistente
	Gtk::TreeModel::Children children = row.children();
	Gtk::TreeModel::Children::iterator iter_child = children.begin();
	Gtk::TreeModel::Children::iterator iter_end = children.end();
	bool found = false;
	while (iter_child != iter_end) {
	  Gtk::TreeModel::Row row_child = *iter_child;
	  if (prest_popup_sel == Glib::ustring(row_child[receipt_task_tree_mod_col_record.description])) {
	    found = true;
	    break;
	  }
	  iter_child++;
	}
	if (!found)	{
	  // aggiunge la prestazione selezionata al capo
	  try {
	    float prezzo;
	    std::istringstream iss(database->get_prezzo_prestazione(capo_popup_sel, prest_popup_sel).raw());
	    iss >> prezzo;
	    Gtk::TreeModel::iterator new_child_iter = prestazioni_ricevuta_treestore->append(children);
	    Gtk::TreeModel::Row new_child_row = *new_child_iter;
	    new_child_row[receipt_task_tree_mod_col_record.description] = prest_popup_sel;
	    new_child_row[receipt_task_tree_mod_col_record.price] = prezzo;
	    update_totale_nuova_ricevuta(prezzo);
	  } catch (const DataBase::DBError& dbe) {
	    std::cerr << "Controller::on_selezione_prestazione_popup_changed() " << dbe.get_message() << std::endl;
	  }
	  return;
	}
      }
    }
    // aggiunge un nuovo capo con la prestazione selezionata
    Gtk::TreeModel::Children main_children { prestazioni_ricevuta_model->children() };
    Gtk::TreeModel::Children::iterator iter_main { main_children.begin() };
    // conta il numero dei capi per determinare il numero del nuovo capo
    unsigned int n = 0;
    while (iter_main != main_children.end()) {
      n++;
      iter_main++;
    }
    Gtk::TreeModel::iterator iterTree { prestazioni_ricevuta_treestore->append() };
    Gtk::TreeModel::Row row { *iterTree };
    row[receipt_task_tree_mod_col_record.number] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), n + 1);
    row[receipt_task_tree_mod_col_record.garment] = capo_popup_sel;
    Gtk::TreeModel::iterator iterRowChild { prestazioni_ricevuta_treestore->append(row.children()) };
    Gtk::TreeModel::Row childRow { *iterRowChild };
    childRow[receipt_task_tree_mod_col_record.description] = prest_popup_sel;
    float prezzo;
    try {
      std::istringstream iss(database->get_prezzo_prestazione(capo_popup_sel, prest_popup_sel).raw());
      iss >> prezzo;
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::on_selezione_prestazione_popup_changed() " << dbe.get_message() << std::endl;
    }
    childRow[receipt_task_tree_mod_col_record.price] = prezzo;
    update_totale_nuova_ricevuta(prezzo);
    // seleziona il nuovo capo creato
    prestazioni_ricevuta_treeselection->select(iterTree);
  } else {
    add_prestazione_ricevuta_button->set_sensitive(false);
  }
}

/**
 * funzione chiamata quando si cliccla sul tasto di addizione delle prestazioni
 * */
void Controller::on_add_prestazione_ricevuta_action(void) {
  if (selezione_capo_combobox->get_active_row_number() >= 0 && selezione_prestazione_combobox->get_active_row_number() >= 0) {
    Gtk::TreeModel::iterator iter_popup { selezione_prestazione_combobox->get_active() };
    Gtk::TreeModel::Row row_popup { *iter_popup };
    Glib::ustring prest_popup_sel { row_popup[combobox_tmcr.column] };
    iter_popup = selezione_capo_combobox->get_active();
    row_popup = *iter_popup;
    Glib::ustring capo_popup_sel { row_popup[combobox_tmcr.column] };
    Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
    Gtk::TreeModel::Children main_children { prestazioni_ricevuta_model->children() };
    Gtk::TreeModel::iterator iterTree { prestazioni_ricevuta_treestore->append() };
    Gtk::TreeModel::Row row { *iterTree };
    row[receipt_task_tree_mod_col_record.number] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), main_children.size() + 1);
    row[receipt_task_tree_mod_col_record.garment] = capo_popup_sel;
    Gtk::TreeModel::iterator iterRowChild { prestazioni_ricevuta_treestore->append(row.children()) };
    Gtk::TreeModel::Row childRow { *iterRowChild };
    childRow[receipt_task_tree_mod_col_record.description] = prest_popup_sel;
    float prezzo;
    try {
      std::istringstream iss(database->get_prezzo_prestazione(capo_popup_sel, prest_popup_sel).raw());
      iss >> prezzo;
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::on_add_prestazione_ricevuta_action() " << dbe.get_message() << std::endl;
    }
    childRow[receipt_task_tree_mod_col_record.price] = prezzo;
    update_totale_nuova_ricevuta(prezzo);
  }
}

/**
 * riassegna i numeri ai capi in ordine crescente
 * */
void Controller::riordina_numeri_capi(void) {
  Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
  Gtk::TreeModel::Children main_children { prestazioni_ricevuta_model->children() };
  Gtk::TreeModel::Children::iterator iter_main { main_children.begin() };
  // itera tra i capi e riassegna i numeri
  unsigned int n = 1;
  while (iter_main != main_children.end()) {
    Gtk::TreeModel::Row row { *iter_main };
    row[receipt_task_tree_mod_col_record.number] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), n);
    n++;
    iter_main++;
  }
}

/**
 * funzione chiamata quando si cliccla sul tasto di rimozione delle prestazioni
 * */
void Controller::on_remove_prestazione_ricevuta_action(void) {
  // verifica se ci sono capi o prestazioni selezionate nel TreeView
  if (prestazioni_ricevuta_treeselection->count_selected_rows() > 0) {
    prestazioni_ricevuta_connection.disconnect();
    Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
    Gtk::TreeModel::Path path { get_prestazione_ricevuta_selezionata() };
    Gtk::TreeModel::iterator iter { prestazioni_ricevuta_model->get_iter(path) };
    Gtk::TreeModel::Row row { *iter };
    Glib::ustring descrizione { row[receipt_task_tree_mod_col_record.description] };
    // se la descrizione è una stringa vuota allora è stato selezionato un capo
    if (descrizione.compare(Glib::ustring()) != 0) {
      float prezzo = row[receipt_task_tree_mod_col_record.price];
      path.up();
      Gtk::TreeModel::iterator main_iter { prestazioni_ricevuta_model->get_iter(path) };
      Gtk::TreeModel::Row main_row { *main_iter };
      Gtk::TreeModel::Children children { main_row.children() };
      prestazioni_ricevuta_treestore->erase(iter);
      // se non ci sono altre prestazioni allora il capo viene eliminato
      if (children.empty()) {
	prestazioni_ricevuta_treestore->erase(main_iter);
      }
      update_totale_nuova_ricevuta(-prezzo);
    } else {
      // è stato selezionato un capo
      float totale = 0.0;
      // itera tra le prestazioni del capo selezionato per calcolare il valore totale dei prezzi
      Gtk::TreeModel::Children children { row.children() };
      Gtk::TreeModel::Children::iterator iter_child { children.begin() };
      while (iter_child != children.end()) {
	Gtk::TreeModel::Row child_row { *iter_child };
	float prezzo = child_row[receipt_task_tree_mod_col_record.price];
	totale += prezzo;
	iter_child++;
      }
      prestazioni_ricevuta_treestore->erase(iter);
      update_totale_nuova_ricevuta (-totale);
    }
    riordina_numeri_capi();
    selezione_capo_combobox->set_active(-1);
    connect_prestazioni_ricevuta();
    prestazioni_ricevuta_treeselection->unselect_all();
  }
}

void Controller::on_descrizione_modificata(const Glib::ustring& path, const Glib::ustring& new_text) {
  Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
  Gtk::TreeModel::iterator iter { prestazioni_ricevuta_model->get_iter(path) };
  Gtk::TreeModel::Row row { *iter };
  Glib::ustring capo { row[receipt_task_tree_mod_col_record.garment] };
  // se il capo non è una stringa vuota allora è stato selezionata la riga del capo
  if (capo.compare(Glib::ustring()) != 0) {
    // è stata modificata la descrizione di un capo
    row[receipt_task_tree_mod_col_record.description] = Glib::ustring();
  }
}

void Controller::on_prezzo_modificato(const Glib::ustring& path, const Glib::ustring& new_text) {
  Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
  Gtk::TreeModel::iterator iter { prestazioni_ricevuta_model->get_iter(path) };
  Gtk::TreeModel::Row row { *iter };
  Glib::ustring capo { row[receipt_task_tree_mod_col_record.garment] };
  // se il capo non è una stringa vuota allora è stato selezionata la riga del capo
  if (capo.compare(Glib::ustring()) != 0) {
    // è stato modificato il prezzo di un capo
    row[receipt_task_tree_mod_col_record.price] = 0.0;
  } else {
    // è stato modificato il prezzo di una prestazione
    float totale = ricalcola_totale_nuova_ricevuta();
    totale_ricevuta_label->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), totale));
    if ( pagato_radiobutton->get_active()) {
      acconto_ricevuta_entry->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), totale));
    }
  }
}

/**
 * aggiorna il totale della ricevuta sommando il valore delta
 * */
void Controller::update_totale_nuova_ricevuta(float delta) {
  float sub_totale = atof(totale_ricevuta_label->get_text().c_str());
  float totale = urgente_ricevuta_checkbutton->get_active() ? sub_totale + delta + ((delta / 100.0) * preferenze.get_extra_charge()) : sub_totale + delta;
  totale_ricevuta_label->set_text (Glib::ustring::format(std::fixed, std::setprecision(2), totale));
}

/**
 * itera tra i capi e le relative prestazioni sommando tutti i prezzi per calcolare il totale della ricevuta
 * */
float Controller::ricalcola_totale_nuova_ricevuta(void) {
  float totale = 0.0;
  Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
  Gtk::TreeModel::Children main_children { prestazioni_ricevuta_model->children() };
  Gtk::TreeModel::Children::iterator iter_main { main_children.begin() };
  while (iter_main != main_children.end()) {
    Gtk::TreeModel::Row main_row { *iter_main };
    Gtk::TreeModel::Children row_children { main_row.children() };
    Gtk::TreeModel::Children::iterator iter_child { row_children.begin() };
    while (iter_child != row_children.end()) {
      Gtk::TreeModel::Row child_row { *iter_child };
      float sub_totale = child_row[receipt_task_tree_mod_col_record.price];
      if (urgente_ricevuta_checkbutton->get_active()) {
	totale += sub_totale + ((sub_totale / 100.0) * preferenze.get_extra_charge());
      } else {
	totale += sub_totale;
      }
      iter_child++;
    }
    iter_main++;
  }
  return totale;
}

void Controller::on_pagamento_anticipato_toggled(void) {
  if (pagato_radiobutton->get_active()) {
    float acconto = atof(totale_ricevuta_label->get_text().c_str());
    acconto_ricevuta_entry->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), acconto));
    acconto_ricevuta_entry->set_sensitive(false);
  } else {
    acconto_ricevuta_entry->set_text("0,0");
    acconto_ricevuta_entry->set_sensitive(true);
    acconto_ricevuta_entry->grab_focus();
  }
}

void Controller::on_urgente_toggled(void) {
  float totale = ricalcola_totale_nuova_ricevuta();
  totale_ricevuta_label->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), totale));
}

void Controller::on_conferma_ricevuta_button_action(void) {
  Ricevuta rcvt;
  int nrSel = receipts_treeselection->count_selected_rows();
  Glib::ustring cliente { cliente_ricevuta_entry->get_text() };
  bool urgente = urgente_ricevuta_checkbutton->get_active();
  float totale = atof(totale_ricevuta_label->get_text().c_str());
  CustomDate data_accettazione { CustomDate::parse_string(data_accettazione_ricevuta_label->get_text().c_str(), DATE_FORMAT::human) };
  CustomDate data_ritiro { CustomDate::parse_string(data_ritiro_ricevuta_label->get_text().c_str(), DATE_FORMAT::human) };
  if (cliente.compare(Glib::ustring()) != 0) {
    if (totale > 0.0) {
      if (data_ritiro > data_accettazione) {
	bool totale_max_check = true;
	if (preferenze.get_enable_max_per_day() && (totale_ricevute_del_giorno(data_ritiro) + totale) > preferenze.get_max_per_day()) {
	  Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*main_win, "Il totale delle ricevute da consegnare per il giorno scelto ha superato la soglia massima! Vuoi continuare con la stessa data?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true) };
	  int res_id = alert_dialog->run();
	  delete alert_dialog;
	  if (res_id == Gtk::RESPONSE_NO) {
	    totale_max_check = false;
	  }
	}
	if (totale_max_check) {
	  if (nrSel == 0) {  // nuova ricevuta
	    rcvt.set_nome_cliente(cliente);
	    rcvt.set_numero(Ricevuta::generate_next_numero_ricevuta());
	    rcvt.set_urgente(urgente);
	    rcvt.set_data_accettazione(data_accettazione);
	    rcvt.set_data_ritiro(data_ritiro);
	    rcvt.set_acconto(acconto_ricevuta_entry->get_text());
	    Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
	    Gtk::TreeModel::Children main_children { prestazioni_ricevuta_model->children() };
	    Gtk::TreeModel::Children::iterator iter_main { main_children.begin() };
	    while (iter_main != main_children.end()) {
	      Gtk::TreeModel::Row main_row { *iter_main };
	      Glib::ustring capo { main_row[receipt_task_tree_mod_col_record.garment] };
	      unsigned int numero = atoi(Glib::ustring(main_row[receipt_task_tree_mod_col_record.number]).c_str());
	      Capo cp { Capo (capo, numero) };
	      Gtk::TreeModel::Children row_children { main_row.children() };
	      Gtk::TreeModel::Children::iterator iter_child { row_children.begin() };
	      while (iter_child != row_children.end()) {
		Gtk::TreeModel::Row child_row { *iter_child };
		float prezzo = child_row[receipt_task_tree_mod_col_record.price];
		Prestazione temp { Prestazione(capo, Glib::ustring(child_row[receipt_task_tree_mod_col_record.description]), prezzo) };
		if (!cp.aggiungi_prestazione(temp)) {
		  write_log("ERROR -> on_conferma_ricevuta_button_action() couldn't add prestazione to capo");
		}
		iter_child++;
	      }
	      rcvt.aggiungi_capo(cp);
	      iter_main++;
	    }
	    if (rcvt.get_acconto() > rcvt.get_totale_netto()) {
	      rcvt.set_acconto (rcvt.get_totale_netto());
	    }
	    try {
	      database->insert(rcvt);
	    } catch (const DataBase::DBError& dbe) {
	      std::cerr << "Controller::on_conferma_ricevuta_button_action() " << dbe.get_message() << std::endl;
	    }
	    on_items_treeselection_changed();
	  } else {   // modifica ricevuta
	    std::vector<Gtk::TreePath> selected { receipts_treeselection->get_selected_rows() };
	    Glib::RefPtr<Gtk::TreeModel> ricevute_treemodel { receipts_treeview->get_model() };
	    std::vector<Gtk::TreePath>::const_iterator iter { selected.cbegin() };
	    while (iter != selected.cend())	{
	      Gtk::TreeModel::iterator iter_view { ricevute_treemodel->get_iter(*iter) };
	      try {
		rcvt = database->get_ricevuta((*iter_view)[receipt_tree_mod_col_record.time_acceptance]);
		if (nrSel == 1)	{
		  rcvt.set_numero((*iter_view)[receipt_tree_mod_col_record.number]);
		  rcvt.set_nome_cliente(cliente);
		  rcvt.set_urgente(urgente);
		  rcvt.set_data_accettazione(data_accettazione);
		  rcvt.set_data_ritiro(data_ritiro);
		  rcvt.set_acconto(acconto_ricevuta_entry->get_text());
		  rcvt.clear_capi();
		  Glib::RefPtr<Gtk::TreeModel> prestazioni_ricevuta_model { prestazioni_ricevuta_treeview->get_model() };
		  Gtk::TreeModel::Children main_children { prestazioni_ricevuta_model->children() };
		  Gtk::TreeModel::Children::iterator iter_main { main_children.begin() };
		  while (iter_main != main_children.end()) {
		    Gtk::TreeModel::Row main_row { *iter_main };
		    Glib::ustring capo { main_row[receipt_task_tree_mod_col_record.garment] };
		    unsigned int numero = atoi(Glib::ustring(main_row[receipt_task_tree_mod_col_record.number]).c_str());
		    Capo cp { Capo(capo, numero) };
		    Gtk::TreeModel::Children row_children { main_row.children() };
		    Gtk::TreeModel::Children::iterator iter_child { row_children.begin() };
		    while (iter_child != row_children.end()) {
		      Gtk::TreeModel::Row child_row { *iter_child };
		      float prezzo = child_row[receipt_task_tree_mod_col_record.price];
		      Prestazione temp { Prestazione(capo, Glib::ustring(child_row[receipt_task_tree_mod_col_record.description]), prezzo) };
		      if (!cp.aggiungi_prestazione(temp)) {
			write_log("ERROR -> on_conferma_ricevuta_button_action() couldn't add prestazione to capo");
		      }
		      iter_child++;
		    }
		    rcvt.aggiungi_capo(cp);
		    iter_main++;
		  }
		  database->update((*iter_view)[receipt_tree_mod_col_record.time_acceptance], rcvt);
		}
	      } catch (const DataBase::DBError& dbe) {
		std::cerr << "on_conferma_ricevuta_button_action() " << dbe.get_message() << std::endl;
	      } catch (const DataBase::DBEmptyResultException& dbere) {
		std::cerr << "on_conferma_ricevuta_button_action() " << dbere.get_message() << std::endl;
	      }
	      iter++;
	    }
	  }
	  Cliente clnt;
	  load_ricevute_in_treeview();
	  new_ricevuta_window->hide();
	  clnt.set_nome(cliente);
	  try {
	    if (!database->is_name_present(cliente)) {
	      Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*main_win, "Questo cliente non è presente nel database, vuoi aggiungerlo?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true) };
	      int res_id = alert_dialog->run();
	      delete alert_dialog;
	      if (res_id == Gtk::RESPONSE_YES) {
		on_new_cliente_action();
		nome_cliente_entry->set_text(cliente);
	      }
	    }
	  } catch (const DataBase::DBError& dbe) {
	    std::cerr << "Controller::on_conferma_ricevuta_button_action() " << dbe.get_message() << std::endl;
	  }
	  if (stampa_ricevuta_checkbutton->get_active()) {
	    print_or_preview_ricevuta(Gtk::PRINT_OPERATION_ACTION_PRINT, rcvt.get_ricevuta_string(preferenze.get_print_number()));
	    const std::vector<Capo>* capi { rcvt.get_const_capi() };
	    std::vector<Capo>::const_iterator iter_cp { capi->begin() };
	    while (iter_cp != capi->end()) {
	      print_or_preview_etichetta(Gtk::PRINT_OPERATION_ACTION_PRINT, rcvt.get_etichetta_string(iter_cp, preferenze.get_print_number()));
	      iter_cp++;
	    }
	    print_or_preview_ricevuta(Gtk::PRINT_OPERATION_ACTION_PRINT, rcvt.get_ricevuta_string(preferenze.get_print_number()));
	  }
	}
      } else {
	Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_ricevuta_window, "La <b>data di ritiro</b> non può essere antecedente a quella di <b>accettazione</b>.", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
	int res_id = alert_dialog->run();
	delete alert_dialog;
      }
    } else {
      Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_ricevuta_window, "Il <b>totale</b> è uguale a zero, aggiungere almeno una prestazione.", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
      int res_id = alert_dialog->run();
      delete alert_dialog;
    }
  } else {
    Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_ricevuta_window, "Non è stato inserito il <b>nome del cliente</b>.", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
    int res_id = alert_dialog->run();
    delete alert_dialog;
  }
}

void Controller::load_capi_in_prestazione_completion(void) {
  capo_liststore->clear();
  std::map<Glib::ustring,int>::iterator iter { mappa_capi.begin() };
  while (iter != mappa_capi.end()) {
    Gtk::TreeModel::iterator iterTree { capo_liststore->append() };
    Gtk::TreeModel::Row row { *iterTree };
    row[combobox_tmcr.column] = iter->first;
    iter++;
  }
}

void Controller::on_new_prestazione_confirm_button_clicked(void) {
  Glib::ustring capo { capo_prestazione_entry->get_text() };
  Glib::ustring descrizione { descrizione_prestazione_entry->get_text() };
  Glib::ustring prezzo { prezzo_prestazione_entry->get_text() };
  int nrSel = prestazioni_treeselection->count_selected_rows();
  if (capo.compare(Glib::ustring()) != 0) {
    if (descrizione.compare(Glib::ustring()) != 0) {
      if (nrSel == 0)	{	
	Prestazione prst { Prestazione(capo, descrizione, atof(prezzo.c_str())) };
	try {
	  if (database->is_prestazione_present(capo, descrizione)) {
	    Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_prestazione_window, "La stessa prestazione è già presente nel preziario, vuoi sostituirla con questa?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true) };
	    int res_id = alert_dialog->run();
	    delete alert_dialog;
	    if (res_id == Gtk::RESPONSE_YES) {
	      database->update(capo, descrizione, prst);
	    }
	  } else {
	    database->insert(prst);
	    on_items_treeselection_changed();
	    if(mappa_capi.count(prst.get_capo())) {
	      mappa_capi.at(prst.get_capo()) += 1;
	    } else {
	      mappa_capi.insert(std::pair<Glib::ustring,int>(prst.get_capo(),1));
	      load_capo_popup_button();
	      load_capi_in_prestazione_completion();
	    }
	  }
	} catch (DataBase::DBError& dbe) {
	  std::cerr << "Controller::on_new_prestazione_confirm_button_clicked() " << dbe.get_message() << std::endl;
	}
      } else {
	std::vector<Gtk::TreePath> selected { prestazioni_treeselection->get_selected_rows() };
	Glib::RefPtr<Gtk::TreeModel> prestazioni_treemodel { tasks_treeview->get_model() };
	std::vector<Gtk::TreePath>::const_iterator iter { selected.begin() };
	while (iter != selected.end()) {
	  Gtk::TreeModel::iterator iter_view { prestazioni_treemodel->get_iter(*iter) };
	  try {
	    if (database->is_prestazione_present((*iter_view)[task_tree_mod_col_record.garment], (*iter_view)[task_tree_mod_col_record.description])) {
	      if (nrSel == 1)	{
		Prestazione prst { Prestazione(capo, descrizione, atof(prezzo.c_str())) };
		if(mappa_capi.count(capo)) {
		  mappa_capi.at(capo) += 1;
		} else {
		  mappa_capi.insert(std::pair<Glib::ustring,int>(capo,1));
		  load_capo_popup_button();
		  load_capi_in_prestazione_completion();
		}
		database->update((*iter_view)[task_tree_mod_col_record.garment], (*iter_view)[task_tree_mod_col_record.description], prst);
	      }
	    } else {
	      write_log("ERROR -> on_new_prestazione_confirm_button_clicked() couldn't find selected prestazione");
	    }
	  } catch (DataBase::DBError& dbe) {
	    std::cerr << "Controller::on_new_prestazione_confirm_button_clicked() " << dbe.get_message() << std::endl;
	  }
	  iter++;
	}
      }
      load_prestazioni_in_treeview();
      new_prestazione_window->hide();
    } else {
      Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_prestazione_window, "La <b>descrizione</b> della prestazione non può essere vuota!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
      int res_id = alert_dialog->run();
      delete alert_dialog;
    }
  } else {
    Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_prestazione_window, "Il <b>capo</b> della prestazione non può essere vuoto!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
    int res_id = alert_dialog->run();
    delete alert_dialog;
  }
}

void Controller::on_new_cliente_confirm_button_clicked(void) {
  bool result = true;
  Glib::ustring nome { nome_cliente_entry->get_text() };
  Glib::ustring telefono { telefono_cliente_entry->get_text() };
  int nrSel = clienti_treeselection->count_selected_rows();
  if (nome.compare(Glib::ustring()) != 0) {
    if (nrSel == 0) {
      try {
	if (database->is_name_present(nome)) {
	  std::vector<Cliente> clienti { database->get_cliente(nome) };
	  Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_cliente_window, "Questo nome è già presente, vuoi aggiornare i suoi dati con questi?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true) };
	  int res_id = alert_dialog->run();
	  delete alert_dialog;
	  if (res_id == Gtk::RESPONSE_YES) {
	    Cliente clnt { clienti.front() };
	    clnt.set_telefono(telefono);
	    database->update(clnt.get_nome(), clnt);
	  }
	} else {
	  Cliente clnt { nome, telefono };
	  database->insert(clnt);
	  on_items_treeselection_changed();
	}
      } catch (const DataBase::DBEmptyResultException& dbere) {
	std::cerr << "Controller::on_new_cliente_confirm_button_clicked() " << dbere.get_message() << std::endl;
	result = false;
      } catch (const DataBase::DBError& dbe) {
	std::cerr << "Controller::on_new_cliente_confirm_button_clicked() " << dbe.get_message() << std::endl;
	result = false;
      }
    } else {
      std::vector<Gtk::TreePath> selected { clienti_treeselection->get_selected_rows() };
      Glib::RefPtr<Gtk::TreeModel> clienti_treemodel { clienti_treeview->get_model() };
      std::vector<Gtk::TreePath>::const_iterator iter { selected.begin() };
      while (iter != selected.end ())	{
	Gtk::TreeModel::iterator iter_view { clienti_treemodel->get_iter(*iter) };
	Cliente clnt = Cliente(nome, telefono);
	try {
	  database->update((*iter_view)[customer_tree_mod_col_record.name], clnt);
	  database->update_nome((*iter_view)[customer_tree_mod_col_record.name], nome);
	  load_ricevute_in_treeview();
	} catch (const DataBase::DBError& dbe) {
	  std::cerr << "Controller::on_new_cliente_confirm_button_clicked() " << dbe.get_message() << std::endl;
	  result = false;
	}
	iter++;
      }
    }
    if (result) {
      load_clienti_in_treeview();
      load_customers_in_treeview(nome_cliente_liststore);
      new_cliente_window->hide();
    }
  } else {
    Gtk::MessageDialog *alert_dialog { new Gtk::MessageDialog(*new_cliente_window, "Il <b>nome</b> del cliente non può essere vuoto!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true) };
    int res_id = alert_dialog->run();
    delete alert_dialog;
  }
}

void Controller::on_calendario_confirm_button_clicked(void) {
  Glib::ustring data_ora_string;
  Gtk::Label *working_label;
  if (calendario_window->get_title() == Glib::ustring("Accettazione")) {
    working_label = data_accettazione_ricevuta_label;
  } else if (calendario_window->get_title() == Glib::ustring("Ritiro")) {
    working_label = data_ritiro_ricevuta_label;
  }
  guint year, month, day;
  int hour, minute;
  main_calendar->get_date(year, month, day);
  hour = calendar_hour_combobox->get_active_row_number() + 7;
  minute = calendar_minute_combobox->get_active_row_number() * 5;
  CustomDate new_date { (int)day, (int)month + 1, (int)year, hour, minute, 0 };
  working_label->set_text(Glib::ustring(new_date.get_formatted_string(DATE_FORMAT::human)));
  calendario_window->hide();
}

void Controller::show_preferenze_panel(void) {
  settings_password_entry->set_text(preferenze.get_password());
  settings_password_checkbutton->set_active(preferenze.get_enable_password());
  settings_header_textbuffer->set_text(preferenze.get_receipt_header());
  settings_greeting_textbuffer->set_text(preferenze.get_receipt_greeting());
  settings_delete_combobox->set_active(preferenze.get_receipt_autodelete());
  settings_number_reset_combobox->set_active(preferenze.get_reset_number());
  settings_number_print_checkbutton->set_active(preferenze.get_print_number());
  settings_number_view_checkbutton->set_active(preferenze.get_view_number());
  settings_maxperday_entry->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), preferenze.get_max_per_day()));
  settings_maxperday_checkbutton->set_active(preferenze.get_enable_max_per_day());
  settings_extracharge_entry->set_text(Glib::ustring::format(std::fixed, std::setprecision(2), preferenze.get_extra_charge()));
  settings_debug_checkbutton->set_active(preferenze.get_enable_debug_log());
  settings_window->show();
}

void Controller::on_visualizza_numero_toggled(void) {
  preferenze.set_view_number(settings_number_view_checkbutton->get_active());
  update_visualizza_numero(preferenze.get_view_number());
}

void Controller::update_visualizza_numero(bool visualizza) {
  receipt_number_treeviewcolumn->set_visible(visualizza);
  numero_ricevuta_resoconto_treeviewcolumn->set_visible(visualizza);
}

void Controller::on_salva_preferenze_action(void) {
  preferenze.set_password(settings_password_entry->get_text());
  preferenze.set_enable_password(settings_password_checkbutton->get_active());
  preferenze.set_receipt_header(settings_header_textbuffer->get_text());
  preferenze.set_receipt_greeting(settings_greeting_textbuffer->get_text());
  preferenze.set_receipt_autodelete(settings_delete_combobox->get_active_row_number());
  preferenze.set_reset_number(settings_number_reset_combobox->get_active_row_number());
  preferenze.set_print_number(settings_number_print_checkbutton->get_active());
  preferenze.set_view_number(settings_number_view_checkbutton->get_active());
  preferenze.set_max_per_day(settings_maxperday_entry->get_text());
  preferenze.set_enable_max_per_day(settings_maxperday_checkbutton->get_active());
  preferenze.set_extra_charge(settings_extracharge_entry->get_text());
  Ricevuta::set_sovraprezzo_urgenza(preferenze.get_extra_charge());
  preferenze.set_enable_debug_log(settings_debug_checkbutton->get_active());
  if (preferenze.get_enable_debug_log()) {
    open_log();
  } else {
    close_log();
  }
  settings_window->hide();
}

void Controller::eliminazione_automatica(void) {
  if (preferenze.get_receipt_autodelete() > 0) {
    CustomDate inizio, fine;
    TIME_PERIOD period;
    switch (preferenze.get_receipt_autodelete() - 1) {
    case 0:
      period = TIME_PERIOD::daily;
      break;
    case 1:
      period = TIME_PERIOD::weekly;
      break;
    case 2:
      period = TIME_PERIOD::monthly;
      break;
    case 3:
      period = TIME_PERIOD::annually;
      break;
    }
    CustomDate::get_time_period_boundary(&inizio, &fine, period);
    try {
      std::vector<Ricevuta> ricevute { database->get_ricevute_per_eliminazione(inizio) };
      std::vector<Ricevuta>::iterator iter { ricevute.begin() };
      while (iter != ricevute.end()) {
	database->delete_ricevuta(iter->get_date_acc_unix());
	iter++;
      }
    } catch (const DataBase::DBError& dbe) {
      std::cerr << "Controller::eliminazione_automatica() " << dbe.get_message() << std::endl;
    }
    load_ricevute_in_treeview();
  }
}

void Controller::on_suggerimento_clienti_double_clicked(GdkEventButton *event) {
  if (event->type == GDK_2BUTTON_PRESS) {
    std::vector<Gtk::TreePath> selected { clienti_suggerimento_treeselection->get_selected_rows() };
    int nr_sel = clienti_suggerimento_treeselection->count_selected_rows();
    Glib::RefPtr<Gtk::TreeModel> clienti_treemodel { clienti_suggerimento_treeview->get_model() };
    if (nr_sel > 0)	{
      std::vector<Gtk::TreePath>::const_iterator iter { selected.cbegin() };
      Gtk::TreeModel::iterator iter_view { clienti_treemodel->get_iter(*iter) };
      Gtk::TreeModel::Row row { *iter_view };
      cliente_ricevuta_entry->set_text (row[customer_tree_mod_col_record.name]);
    }
    suggerimento_clienti_window->hide();
  }
}

Glib::ustring Controller::get_stampa_prestazioni(const Ricevuta& rcpt, size_t line) {
  Glib::ustring str { "- - - - - - - - - - - - - - - - \n" };
  const std::vector<Capo>* capi_ricevuta { rcpt.get_const_capi() };
  std::vector<Capo>::const_iterator iter_c { capi_ricevuta->cbegin() };
  while (iter_c != capi_ricevuta->cend())	{
    const std::vector<Prestazione>* prestazioni_capo { iter_c->get_const_prestazioni() };
    std::vector<Prestazione>::const_iterator iter_p { prestazioni_capo->cbegin() };
    while (iter_p != prestazioni_capo->end()) {
      Glib::ustring capo { iter_p->get_capo() };
      if (capo.size() > 6) {
	capo.assign(capo.substr(0,6) + ".");
      }
      Glib::ustring descr { iter_p->get_descrizione() };
      if (descr.size() > 14) {
	descr.assign(descr.substr(0,14) + ".");
      }
      Glib::ustring str_end { " € " + Glib::ustring::format(std::fixed, std::setprecision(2), iter_p->get_prezzo()) };
      Glib::ustring str_start { "1 " + capo + " " + descr };
      str += Ricevuta::get_formatted_line (str_start, str_end, line);
      iter_p++;
    }
    iter_c++;
  }
  return str;
}

void Controller::on_printoperation_status_changed(const Glib::RefPtr<Gtk::PrintOperation>& operation) {
  Glib::ustring status_msg;
  if (operation->is_finished()) {
    status_msg = "Lavoro di stampa inviato alla stampante.";
  } else {
    status_msg = operation->get_status_string();
  }
  main_statusbar->push(status_msg, stbrPrintId);
}

void Controller::on_printoperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr<Gtk::PrintOperation>& operation) {
  //Printing is "done" when the print data is spooled.
  if (result == Gtk::PRINT_OPERATION_RESULT_ERROR) {
    Gtk::MessageDialog err_dialog(*main_win, "Error printing form", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
    err_dialog.run();
  } else if (result == Gtk::PRINT_OPERATION_RESULT_APPLY) {
    //Update PrintSettings with the ones used in this PrintOperation:
    m_ref_printsettings = operation->get_print_settings();
  }
  if (!operation->is_finished()) {
    //We will connect to the status-changed signal to track status
    //and update a status bar. In addition, you can, for example,
    //keep a list of active print operations, or provide a progress dialog.
    operation->signal_status_changed().connect(sigc::bind(sigc::mem_fun(*this,&Controller::on_printoperation_status_changed),operation));
  }
}

void Controller::print_or_preview_ricevuta(Gtk::PrintOperationAction print_action, const Glib::ustring &str1) {
  Glib::RefPtr<FormPrintOperation> print = FormPrintOperation::create();
  Glib::ustring content { Glib::ustring("<b>\n") };
  content.append(preferenze.get_receipt_header());
  content.append(Glib::ustring("\n_______________________________</b>\n\n"));
  content.append(str1);
  content.append(preferenze.get_receipt_greeting());
  print->set_content(content);
  print->set_track_print_status();
  print->set_default_page_setup(m_ref_pagesetup);
  print->set_print_settings(m_ref_printsettings);
  print->signal_done().connect(sigc::bind ( sigc::mem_fun(*this,&Controller::on_printoperation_done), print ) );
  try {
    print->run(print_action, *main_win);
  } catch (const Gtk::PrintError& ex) {
    write_log(Glib::ustring("ERROR -> print_or_preview_ricevuta() an error occurred while trying to run a print operation:") + Glib::ustring(ex.what()));
  }
}

void Controller::print_or_preview_etichetta(Gtk::PrintOperationAction print_action, const Glib::ustring &str1) {
  Glib::RefPtr<FormPrintOperation> print = FormPrintOperation::create();
  print->set_content(str1);
  print->set_track_print_status();
  print->set_default_page_setup(m_ref_pagesetup);
  print->set_print_settings(m_ref_printsettings);
  print->signal_done().connect(sigc::bind ( sigc::mem_fun(*this,&Controller::on_printoperation_done), print ) );
  try {
    print->run(print_action, *main_win);
  } catch (const Gtk::PrintError& ex) {
    write_log(Glib::ustring("ERROR -> print_or_preview_etichetta() an error occurred while trying to run a print operation:") + Glib::ustring(ex.what()));
  }
}

void Controller::print_or_preview_chiusura(Gtk::PrintOperationAction print_action, const Glib::ustring &str1) {
  Glib::RefPtr<FormPrintOperation> print = FormPrintOperation::create();
  Glib::ustring content { Glib::ustring("<b>\n") };
  content.append(preferenze.get_receipt_header());
  content.append(Glib::ustring("\n_______________________________</b>\n\n"));
  content.append(str1);
  print->set_content(content);
  print->set_track_print_status();
  print->set_default_page_setup(m_ref_pagesetup);
  print->set_print_settings(m_ref_printsettings);
  print->signal_done().connect(sigc::bind(sigc::mem_fun(*this,&Controller::on_printoperation_done), print));
  try {
    print->run(print_action, *main_win);
  } catch (const Gtk::PrintError& ex) {
    write_log(Glib::ustring("ERROR -> print_or_preview_chiusura() an error occurred while trying to run a print operation:") + Glib::ustring(ex.what()));
  }
}

int Controller::request_password(void) {
  check_password_dialog_entry->set_text("");
  check_password_dialog_entry->grab_focus();
  int response_id = request_password_dialog->run();
  return response_id;
}

void Controller::on_request_password_ok(void) {
  request_password_dialog->response(Gtk::RESPONSE_OK);
  request_password_dialog->hide();
}

void Controller::on_request_password_cancel(void) {
  request_password_dialog->response(Gtk::RESPONSE_CANCEL);
  request_password_dialog->hide();
}

void Controller::on_password_error(void) {
  Gtk::MessageDialog* error_dialog { new Gtk::MessageDialog (*main_win, Glib::ustring("Password errata, accesso negato."), false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE, true) };
  error_dialog->signal_response().connect(sigc::bind<Gtk::Dialog*>(sigc::mem_fun(this, &Controller::on_dialog_response), error_dialog));
  error_dialog->run();
}

void Controller::open_log(void) {
  if (!ofs_log.is_open())	{
    Glib::RefPtr<Gio::File> log_file { Gio::File::create_for_path(debug_log_path) };
    if (log_file->query_exists()) {
      ofs_log.open(debug_log_path.c_str(), std::ofstream::app);
    } else {
      ofs_log.open(debug_log_path.c_str(), std::ofstream::out);
    }
    write_log("--> log started <--");
  }
}

void Controller::close_log(void) {
  if (ofs_log.is_open()) {
    write_log("--> log stopped <--");
    ofs_log.close();
  }
}

void Controller::write_log(const Glib::ustring& str) {
  CustomDate now {};
  if (ofs_log.is_open()) {
    ofs_log << now.get_formatted_string(DATE_FORMAT::human) << " - " << str.c_str() << std::endl;
  } else {
    std::cerr << now.get_formatted_string(DATE_FORMAT::human) << " - " << str.c_str() << std::endl;
  }
}
