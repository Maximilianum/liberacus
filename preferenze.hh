/*
 * preferenze.hh
 * Copyright (C) 2014 - 2022 Massimiliano <maximilianum@protonmail.com>
 *
 * liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PREFERENZE_H_
#define _PREFERENZE_H_

#include <glibmm/ustring.h>
#include "custom_date.hh"

#define SETT_PASSWORD		"SETT_PSSWRD"
#define SETT_ENABLE_PSW		"SETT_EN_PSW"
#define SETT_REC_HEADER		"SETT_RC_HEAD"
#define SETT_REC_GREETING	"SETT_RC_GRT"
#define SETT_REC_AUT_DELETE   "SETT_AUT_DEL"
#define SETT_RESET_NUMBER     "SETT_RES_NMB"
#define SETT_PRINT_NUMBER	"SETT_PRT_NMB"
#define SETT_VIEW_NUMBER	"SETT_VW_NMB"
#define SETT_MAX_PER_DAY	"SETT_MAX_PD"
#define SETT_ENABLE_MAX_PD    "SETT_EN_MAX"
#define SETT_DEBUG_LOG	      "SETT_DEB_LOG"
#define SETT_EXTRA_CHARGE	"SETT_EXT_CRG"

class DataBase;

class Preferenze {
 public:
	inline static Glib::ustring get_sqlite_settings_table(void) { return sqlite_settings_table; }
	inline Preferenze() : database (nullptr) {}
	inline void set_database(DataBase* db) { database = db; }
	void set_password(const Glib::ustring &str);
	Glib::ustring get_password(void) const;
	inline void set_enable_password(bool flag) { Glib::ustring str = flag ? "true" : "false"; set_enable_password(str); }
	void set_enable_password(const Glib::ustring& str);
	bool get_enable_password(void) const;
	void set_receipt_header(const Glib::ustring &str);
	Glib::ustring get_receipt_header(void) const;
	void set_receipt_greeting(const Glib::ustring &str);
	Glib::ustring get_receipt_greeting(void) const;
	void set_receipt_autodelete(short val);
	void set_receipt_autodelete(const Glib::ustring& str);
	short get_receipt_autodelete(void) const;
	void set_reset_number(TIME_PERIOD period);
	void set_reset_number(const Glib::ustring& str);
	void set_reset_number(unsigned short val);
	TIME_PERIOD get_reset_number_period(void) const;
	short get_reset_number(void) const;
	inline void set_print_number(bool flag) { Glib::ustring str = flag ? "true" : "false"; set_print_number(str); }
	void set_print_number(const Glib::ustring& str);
	bool get_print_number(void) const;
	inline void set_view_number(bool flag) { Glib::ustring str = flag ? "true" : "false"; set_view_number(str); }
	void set_view_number(const Glib::ustring& str);
	bool get_view_number(void) const;
	inline void set_max_per_day(float val) { set_max_per_day(Glib::ustring::format(std::fixed, std::setprecision(2), val)); }
	void set_max_per_day(const Glib::ustring& str);
	float get_max_per_day(void) const;
	inline void set_enable_max_per_day(bool flag) { Glib::ustring str = flag ? "true" : "false"; set_enable_max_per_day(str); }
	void set_enable_max_per_day(const Glib::ustring& str);
	bool get_enable_max_per_day(void) const;
	inline void set_enable_debug_log(bool flag) { Glib::ustring str = flag ? "true" : "false"; set_enable_debug_log(str); }
	void set_enable_debug_log(const Glib::ustring& str);
	bool get_enable_debug_log(void) const;
	inline void set_extra_charge(float val) { set_extra_charge(Glib::ustring::format(std::fixed, std::setprecision(2), val)); }
	void set_extra_charge(const Glib::ustring& str);
	float get_extra_charge(void) const;
 private:
	static Glib::ustring sqlite_settings_table;
	DataBase* database;
};


#endif // _PREFERENZE_H_

