/*
 * cliente.hh
 * Copyright (C) 2012 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
liberacus is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * liberacus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CLIENTE_HH_
#define _CLIENTE_HH_

#include <glibmm/ustring.h>

class Cliente {
public:
  static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
  static int retrieve_cliente(void *data, int argc, char **argv, char **azColName);
  inline Cliente() {}
  inline Cliente (const Glib::ustring &str_n, const Glib::ustring &str_t) : nome (str_n), telefono (str_t) {}
  Cliente& operator=(const Cliente &clt);
  bool operator<(const Cliente &clt) const;
  bool operator==(const Cliente &clt) const;
  inline void set_nome(const Glib::ustring& str) { nome.assign(str); }
  inline void set_nome(const char* ptr) { nome.assign(Glib::ustring(ptr)); }
  inline Glib::ustring get_nome(void) const { return nome; }
  inline void set_telefono(const Glib::ustring& str) { telefono.assign(str); }
  inline void set_telefono(const char* ptr) { telefono.assign(Glib::ustring(ptr)); }
  inline Glib::ustring get_telefono(void) const { return telefono; }
private:
  static Glib::ustring sqlite_table;
  Glib::ustring nome;
  Glib::ustring telefono;
};

#endif // _CLIENTE_HH_

